package com.antares.cal.model.car;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.Drive;
import com.antares.cal.model.user.*;


/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class CarTest {

	private static Car testcar;
	private static LicenseNumber testln;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
		
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
	}


	@Test
	public void test_addDriveNull() {

		Car testcar = new Car();
		System.out.println("new Car created");
		
		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2016";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		testln.toString();
		testcar.setLicenseNumber(testln);
		
		// created OwnerUser for car 
		OwnerUser testowneruser = new OwnerUser();
		testowneruser.setBillingPreference(BillingPreference.EMAIL);
		
		//created test PersonalData for owneruser
		PersonalData testpd = new PersonalData();
		String firstName = "Max";
		String lastName = "Mustermann";
		testpd.setFirstName(firstName);
		testpd.setLastName(lastName);
		testpd.setGender(Gender.MALE);
		
		testowneruser.setPersonalData(testpd);
		
		// created testaddress for owneruser
		Address testaddress = new Address();
		
		String city = "Bautzen";
		String houseNumber = "1";
		String street = "Loebauer Stasse";
		String zipCode = "02625";
		testaddress.setCity(city);
		testaddress.setCountry(Country.GERMANY);
		testaddress.setHouseNumber(houseNumber);
		testaddress.setStreet(street);
		testaddress.setZipCode(zipCode);
		
		testowneruser.setAddress(testaddress);
		
		
		testcar.setStolen(false);
		testcar.setCarType(CarType.PRIVATE);
		
		//Owneruser wird testcar zugeordnet
		testcar.setOwnerUser(testowneruser);
		
		
	
		Drive testdrive = new Drive();
		/*
		String approachPoint = "Bautzen";
		String exitPoint = "Dresden";
		int distance = 63;
		testdrive.setApproachPoint(approachPoint);
		testdrive.setExitPoint(exitPoint);
		testdrive.setDistance(distance);
		*/
		testdrive = null;
		
		  try {
			    // call function
				testcar.addDrive(testdrive);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 1 successful");
			  }
	} 
		  
		  

	@Test
	public void test_addDriveNotNull() {

		Car testcar = new Car();
		System.out.println("new Car created");
		
		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2016";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		testln.toString();	
		testcar.setLicenseNumber(testln);

		
		// created OwnerUser for car 
		OwnerUser testowneruser = new OwnerUser();
		testowneruser.setBillingPreference(BillingPreference.EMAIL);
		
		//created test PersonalData for owneruser
		PersonalData testpd = new PersonalData();
		String firstName = "Max";
		String lastName = "Mustermann";
		testpd.setFirstName(firstName);
		testpd.setLastName(lastName);
		testpd.setGender(Gender.MALE);
		
		testowneruser.setPersonalData(testpd);
		
		// created testaddress for owneruser
		Address testaddress = new Address();
		
		String city = "Bautzen";
		String houseNumber = "1";
		String street = "Loebauer Stasse";
		String zipCode = "02625";
		testaddress.setCity(city);
		testaddress.setCountry(Country.GERMANY);
		testaddress.setHouseNumber(houseNumber);
		testaddress.setStreet(street);
		testaddress.setZipCode(zipCode);
		
		testowneruser.setAddress(testaddress);
		
		
		testcar.setStolen(false);
		testcar.setCarType(CarType.PRIVATE);
		
		//Owneruser wird testcar zugeordnet
		testcar.setOwnerUser(testowneruser);
		
		
	
		Drive testdrive = new Drive();
		
		String approachPoint = "Bautzen";
		String exitPoint = "Dresden";
		int distance = 63;
		testdrive.setApproachPoint(approachPoint);
		testdrive.setExitPoint(exitPoint);
		testdrive.setDistance(distance);
		
		testcar.addDrive(testdrive);
		assertNotNull(testdrive);
		System.out.println("test 2 successful");
	}
	
	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}


}
