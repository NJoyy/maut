package com.antares.cal.model.car;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

@RunWith(Suite.class)
@SuiteClasses({ CarLocalizationTest.class, CarTest.class, DriveTest.class, LicenseNumberTest.class })
public class carAllTests {

}
