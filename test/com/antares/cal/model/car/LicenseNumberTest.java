package com.antares.cal.model.car;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;

import com.antares.cal.model.car.LicenseNumber;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class LicenseNumberTest {

	private static LicenseNumber testln;
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
		
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
	}


	@Test
	public void test_toString() {

		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2016";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		testln.toString();
		
		String testString = "D-" + code;
		String testOutput = testln.toString();
		
		assertEquals(testString, testOutput);
		System.out.println("test 1 successful");
	}
	
	@Test
	public void test_parse() {
		
		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2017";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		String serial;
		serial = testln.toString();
		
		
		testln.parse(serial);
		boolean testbool;
		
		if( testln.getCode() == code & testln.getOrigin() == Country.GERMANY) {
			testbool = true;
			assertTrue(testbool);
			
		}else {
			testbool = false;
			assertTrue(testbool);
		}
		System.out.println("test 2 successful");
		
	}

	@Test
	public void test_parseNull() {
		
		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2018";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		String serial=null;
		
		  try {
			    // call function
			  	testln.parse(serial);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 3 successful");
			  }
		
	}
	
	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}
