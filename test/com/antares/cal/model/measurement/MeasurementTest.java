package com.antares.cal.model.measurement;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.measurement.Measurement;
import com.antares.cal.model.measurement.MeasurementCollection;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class MeasurementTest {

	private static Measurement testmeasurement;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
		testmeasurement = new Measurement();
		System.out.println("new Measurement created");
		
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
	}
	
	@Test
	public void test_getsetTime() {
		Long time = 1L;
		testmeasurement.setTime(time);

		assertEquals(time, testmeasurement.getTime());
		System.out.println("Test 1 successful");
	}

	@Test
	public void test_setMeasurementCollection() {
		MeasurementCollection tmc = new MeasurementCollection();
		tmc = null;
		  try {
			    // call function
			  testmeasurement.setMeasurementCollection(tmc);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 2 successful");
			  }
		}
	
	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}


}
