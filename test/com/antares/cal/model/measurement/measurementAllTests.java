package com.antares.cal.model.measurement;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

@RunWith(Suite.class)
@SuiteClasses({ MeasurementCollectionTest.class, MeasurementTest.class, MeasureStationTest.class })
public class measurementAllTests {

}
