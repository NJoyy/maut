package com.antares.cal.model.measurement;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.CarType;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.measurement.Measurement;
import com.antares.cal.model.user.OwnerUser;
/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class MeasurementCollectionTest {

	private static List<Measurement> testmeasurements;
	private static MeasurementCollection tmc; 
	private static Car testcar;
	private static Measurement testmeasurement1;
	private static Measurement testmeasurement2;
	private static Measurement testmeasurement3;

	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
		testcar = null;
		testmeasurements = null;
	}
	
	@Test
	public void test_setMeasurements() {		
		  try {
			    // call function
			  tmc.setMeasurements(testmeasurements);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 1 successful");
			  }
	}
	
	@Test
	public void test_setCar() {		
		  try {
			    // call function			  
			  tmc.setCar(testcar);
			     fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 2 successful");
			  }
	}
	
	@Test
	public void test_MeasurementCollection() {
		

		MeasureStation MS1 = new MeasureStation();
		MeasureStation MS2 = new MeasureStation();
		MeasureStation MS3 = new MeasureStation();

		  
		MS1.setType(MeasureStationType.APPROACH);
		MS2.setType(MeasureStationType.EXIT);
		MS3.setType(MeasureStationType.APPROACH);


		//testmeasurement sets
		testmeasurement1 = new Measurement();
		testmeasurement2 = new Measurement();
		testmeasurement3 = new Measurement();

		testmeasurement1.setMeasureStation(MS1);
		testmeasurement1.setId(1L);
		testmeasurement2.setMeasureStation(MS2);
		testmeasurement2.setId(2L);
		testmeasurement3.setMeasureStation(MS3);
		testmeasurement3.setId(3L);
		
		
		tmc = new MeasurementCollection();
		tmc.addMeasurement(testmeasurement1);
		tmc.addMeasurement(testmeasurement2);
		
		try {
			tmc.addMeasurement(testmeasurement3);
		    fail(); // ensure that this point is never reached
		  
		  } catch (Exception e) {
			  System.out.println("expected Exception: " + e);
			  System.out.println("test 3 successful");
		  }		
		
	}
	
	@After
	public void tearDown() {
		System.out.println("after test");
		testmeasurements = null;
		testcar = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}
