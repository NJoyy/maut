package com.antares.cal.model.measurement;

import static org.junit.Assert.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.measurement.Measurement;
import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.Drive;
import com.antares.cal.model.measurement.Driveway;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */
public class MeasureStationTest {

    private List<Measurement> measurements = new ArrayList<>();
    private List<Driveway> driveways = new ArrayList<>();

    private static MeasureStation testmeasurestation;
    private static Measurement testm1;
    private static Measurement testm2;
    private static Driveway drive1;
    private static Driveway drive2;

    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("JUnit-Test start");
        testmeasurestation = new MeasureStation();
        System.out.println("new MeasureStation created");

    }

    @Before
    public void setUp() {
        System.out.println("before test");
    }


    //get-set Name
    @Test
    public void test_getsetName() {
        String name = "MeasureStation";
        testmeasurestation.setName(name);
        assertEquals(name, testmeasurestation.getName());

        System.out.println("Test 1 successful");
    }

    //get-set ShortName
    @Test
    public void test_getsetShortName() {
        String shortname = "ShoreMeasureStation";
        testmeasurestation.setShortName(shortname);
        assertEquals(shortname, testmeasurestation.getShortName());

        System.out.println("Test 2 successful");
    }

    // get-set Measurements
    @Test
    public void test_getsetMeasurements() {
        testm1 = new Measurement();
        testm2 = new Measurement();

        measurements.add(testm1);
        measurements.add(testm2);
        testmeasurestation.setMeasurements(measurements);

        assertTrue(testmeasurestation.getMeasurements().contains(testm1) & testmeasurestation.getMeasurements().contains(testm2));

        System.out.println("Test 3 successful");
    }

    // get-set Driveways
    @Test
    public void test_getsetDrives() {
        drive1 = new Driveway();
        drive2 = new Driveway();

        driveways.add(drive1);
        driveways.add(drive2);

        testmeasurestation.setDriveways(driveways);

        assertTrue(testmeasurestation.getDriveways().contains(drive1) & testmeasurestation.getDriveways().contains(drive2));

        System.out.println("Test 4 successful");
    }

    // get-set InterfaceId
    @Test
    public void test_getsetInterfaceId() {
        Long interfaceId = 1L;
        testmeasurestation.setId(interfaceId);
        assertEquals(interfaceId, testmeasurestation.getId());

        System.out.println("Test 5 successful");
    }

    @After
    public void tearDown() {
        System.out.println("after test");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        System.out.println("JUnit-Test completed");
    }


}
