package com.antares.cal.model.rest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.*;
import com.antares.cal.model.rest.RawOwnerUser;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.model.user.User;


public class RawOwnerUserTest {

	private static Car testcar;
	private static OwnerUser testowneruser;
	private static RawOwnerUser testrou;
	private static LicenseNumber testln;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("start  new test");

	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("before test");
	}
	
	@Test
	public void testAddCarNotNull() {
		
		testcar = new Car();
		System.out.println("new Car created");
		
		testrou = new RawOwnerUser();
		
		testcar.setStolen(false);
		testcar.setCarType(CarType.PRIVATE);
		
		LicenseNumber testln = new LicenseNumber();
		System.out.println("new LicenseNumber created");
		
		String code = "BA2016";
		Country origin = Country.GERMANY;
		testln.setCode(code);
		testln.setOrigin(origin);
		testln.toString();
		testcar.setLicenseNumber(testln);
		
		testrou.addCar(testcar);
		assertNotNull(testcar);
		System.out.println("test 1 successful");
	}
	
	@Test
	public void testAddCarNull() {
		
		testcar = new Car();
		System.out.println("new Car created");
		
		testcar = null;
		
		testrou = new RawOwnerUser();
		
		try {
		    // call function
		    testrou.addCar(testcar);
		    fail(); // ensure that this point is never reached
		  
		  } catch (Exception e) {
			  System.out.println("expected Exception: " + e);
			  System.out.println("test 2 successful");
		  }
		
		
	}
	
	@After
	public void tearDown() throws Exception {
		System.out.println("after test");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("test completed");
	}

}
