package com.antares.cal.model.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

@RunWith(Suite.class)
@SuiteClasses({ RawMeasurementTest.class, RawOwnerUserTest.class })
public class restAllTests {

}
