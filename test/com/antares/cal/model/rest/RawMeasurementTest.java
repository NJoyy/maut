package com.antares.cal.model.rest;

import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.rest.RawMeasurement;
import org.junit.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */
public class RawMeasurementTest {
	
	private static RawMeasurement RMObject1;
	private static RawMeasurement RMObject2;
	private static RawMeasurement RMObject3;
	
	private static long time = 1;
	private static LicenseNumber licenseNumber;
	private static long interfaceId;
	private static String interfacePW;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
		licenseNumber = new LicenseNumber(Country.GERMANY, "0123456789");
		System.out.println("new Support-Object created");
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
		
		RMObject1 = null;
		RMObject2 = null;
		RMObject3 = null;
	}

	//Constructor test - normal conditions
	@Test
	public void test_RawMeasurementConstruktor1() {

		time = 1L;
		licenseNumber.setOrigin(Country.GERMANY);
		licenseNumber.setCode("0123456789");
		interfaceId = 1;
		interfacePW = "insertpasswordhere";

		RMObject1 = new RawMeasurement(time, licenseNumber, interfaceId, interfacePW);
		assertNotNull(RMObject1);		

		System.out.println("Test 1 successful");
	}
	
	//Constructor test - condition: negative time
	@Test
	public void test_RawMeasurementConstruktor2() {

		time = 0L;
		licenseNumber.setOrigin(Country.GERMANY);
		licenseNumber.setCode("0123456789");
		interfaceId = 1;
		interfacePW = "insertpasswordhere";
		  try {
				RMObject2 = new RawMeasurement(time, licenseNumber, interfaceId, interfacePW);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 2 successful");
			  }
	}
	
	//Constructor test - condition: false licenseNumber
	@Test
	public void test_RawMeasurementConstruktor3() {

		time = 1L;
		licenseNumber = null;
		interfaceId = 1;
		interfacePW = "insertpasswordhere";
		  try {
				RMObject3 = new RawMeasurement(time, licenseNumber, interfaceId, interfacePW);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 3 successful");
			  }
		  }

	
	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}