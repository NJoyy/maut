package com.antares.cal.model.user;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.user.*;

/**
* @author Robin Domeier
* @author Lukas Goethel
*/

public class AgencyUserTest {
	
	private static AgencyUser testagencyuser;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("JUnit-Test start");
		testagencyuser = new AgencyUser( ) {};
		System.out.println("new User created");
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("before test");
	}

	
	//get-set AgencyType
	@Test
	public void test_AgencyType() {		
		testagencyuser.setAgencyType(AgencyType.ADMIN);
		String test = String.valueOf(testagencyuser.getAgencyType());
		assertEquals(test, "ADMIN");
		System.out.println("Test 1 successful");
	}
	
	
	@After
	public void tearDown() throws Exception {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("JUnit-Test completed");
	}

}