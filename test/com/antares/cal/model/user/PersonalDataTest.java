package com.antares.cal.model.user;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */


public class PersonalDataTest {

	private static PersonalData instance;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("start  new test");
		instance = new PersonalData();
		System.out.println("New Personal Data created");
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("before test");
	}


	@Test
	public void testSetFirstName() {
		String first_name = "Max";
		instance.setFirstName(first_name);
		assertEquals(instance.getFirstName(), first_name);
		System.out.println("test 1 successful");
	}
	
	@Test
	public void testSetFirstNameBlank() {
		String first_name = null;
		  try {
			    // call function
			    instance.setFirstName(first_name);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 2 successful");
			  }
	}
	
	@Test
	public void testSetLastName() {
		String last_name = "Mustermann";
		instance.setLastName(last_name);
		assertEquals(instance.getLastName(), last_name);
		System.out.println("test 3 successful");
	}
	
	@Test
	public void testSetLastNameBlank() {
		String last_name = null;
		  try {
			    // call function
			    instance.setFirstName(last_name);
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 4 successful");
			  }
	}
	
	@Test
	public void testSetGender() {
		instance.setGender(Gender.DIVERSE);
		String test = String.valueOf(instance.getGender());
		assertEquals(test, "DIVERSE");
		System.out.println("test 5 successful");
	}
	
	@Test
	public void testGenderBlank() {
		//instance.setGender(null);
		  try {
			    // call function
			    instance.setGender(null);;
			    fail(); // ensure that this point is never reached
			  
			  } catch (Exception e) {
				  System.out.println("expected Exception: " + e);
				  System.out.println("test 6 successful");
			  }
	}
	@After
	public void tearDown() throws Exception {
		System.out.println("after test");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("test completed");
	}
}
