package com.antares.cal.model.user;

import static org.junit.Assert.*;

import java.security.SecureRandom;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256Hash;
import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256HashBuilder;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */
public class UserTest {
	
	private static OwnerUser testuser;

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
		testuser = new OwnerUser();
		System.out.println("new User created");
		
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
	}


	//get-set LoginName
	@Test
	public void test_LoginName() {
		String LoginName = "InsertNameHere";
		testuser.setLoginName( LoginName );
		assertEquals(LoginName, testuser.getLoginName());

		System.out.println("Test 1 successful");
	}

	//set-get Email
	@Test
	public void test_setgetEmail() {
		String newMail = "abc@gmail.com";
		testuser.setEmail( newMail );
		assertEquals(newMail, testuser.getEmail());

		System.out.println("Test 2 successful");
	}    

	
	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}