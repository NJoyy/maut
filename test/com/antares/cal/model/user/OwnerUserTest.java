package com.antares.cal.model.user;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.user.OwnerUser;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class OwnerUserTest {

	private static OwnerUser testowneruser;
    private List<Car> cars_list = new ArrayList<>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("JUnit-Test start");
		testowneruser = new OwnerUser();
		System.out.println("New OwnerUser created");
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("before Test");
	}

	
	//get-set BillingPreference
	@Test
	public void test_BillingPreference() {		
		
		testowneruser.setBillingPreference(BillingPreference.EMAIL);
		
		String test = String.valueOf(testowneruser.getBillingPreference());
		assertEquals(test, "EMAIL");
		System.out.println("Test 1 successful");
	}
	
	
	//get-set Address
	@Test
	public void test_Address() {
		
		Address test_address = new Address();
		test_address.setStreet("Street");
		test_address.setHouseNumber("99");
		test_address.setZipCode("02625");
		test_address.setCity("Bautzen");
		test_address.setCountry(Country.GERMANY);
		
		testowneruser.setAddress(test_address);
		assertEquals(test_address, testowneruser.getAddress());

		System.out.println("Test 2 successful");
	}
	
	//get-set PersonalData
	@Test
	 public void test_PersonalData() {
	  PersonalData pd = new PersonalData();
	  pd.setFirstName("Max");
	  pd.setLastName("Mustermann");
	  pd.setGender(Gender.DIVERSE);
	  
	  testowneruser.setPersonalData(pd);
	  assertEquals(testowneruser.getPersonalData(), pd);
	  System.out.println("test 1 successful");
	 } 

	
	
	//get-set Cars
	@Test
	public void test_Cars() {

		Car car_object = new Car();
		cars_list.add(car_object);
		testowneruser.setCars(cars_list);
		
		assertEquals(cars_list, testowneruser.getCars());
		System.out.println("Test 4 successful");
	}
	

	
	@After
	public void tearDown() throws Exception {
		System.out.println("after Test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("JUnit-Test completed");
		
	}
}
