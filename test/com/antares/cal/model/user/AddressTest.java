package com.antares.cal.model.user;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.basic.Country;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */


public class AddressTest {
	
	private static Address instance;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("start  new test");
		instance = new Address();
		System.out.println("New address created");
	}
	
	@Before
	public void setUp() throws Exception {
		System.out.println("before test");
	}
	
	@Test
	public void testSetCity() {
		String city = "Bautzen";
		instance.setCity(city);
		assertEquals(instance.getCity(), city);
		System.out.println("test 1 successful");
	}
	
	@Test
	public void testSetStreet() {
		String street = "L�bauer Stra�e";
		instance.setStreet(street);
		assertEquals(instance.getStreet(), street);
		System.out.println("test 2 successful");
	}
	
	@Test
	public void testSetHouseNumber() {
		String house_number = "66";
		instance.setHouseNumber(house_number);
		assertEquals(instance.getHouseNumber(), house_number);
		System.out.println("test 3 successful");
	}
	
	@Test
	public void testSetZipCode() {
		String zip_code = "02625";
		instance.setZipCode(zip_code);
		assertEquals(instance.getZipCode(), zip_code);
		System.out.println("test 4 successful");
	}
	
	@Test
	public void testSetCountry() {
		instance.setCountry(Country.GERMANY);
		String test = String.valueOf(instance.getCountry());
		assertEquals(test, "GERMANY");
		System.out.println("test 5 successful");
	}
	
	@After
	public void tearDown() throws Exception {
		System.out.println("after test");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("test completed");
	}
}

