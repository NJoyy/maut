package com.antares.cal.model.user;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

@RunWith(Suite.class)
@SuiteClasses({ AddressTest.class, AgencyUserTest.class, OwnerUserTest.class, PersonalDataTest.class, UserTest.class })
public class userAllTests {

}
