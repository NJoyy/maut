package com.antares.cal.model.basic;

import static org.junit.Assert.*;

import java.security.SecureRandom;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256Hash;
import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256HashBuilder;

/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class PasswordAuthorizedTest {
	
	private static OwnerUser testUser;
	private static MeasureStation testMS;
	
	private static String test_password = "ganzganzgeheim";
    private static final int ITERATIONS = 10;
    private byte[] pwHash;
    private byte[] salt;

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");		
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
		testUser = new OwnerUser() {};
		testMS = new MeasureStation();
		
		
	}
	
	//getLastLogin - User
	@Test
	public void test_LastLoginU() {
		String test = "1";
		String NumberAsString = String.valueOf(testUser.getLastLoginTime());
		assertNotSame("", test, NumberAsString);

		System.out.println("Test 1 successful");
	}


	//login - User
	@Test
	public void test_loginU() {
		test_password = "insertpassword";
		testUser.setPassword(test_password);
		assertTrue(testUser.login(test_password));

		System.out.println("Test 2 successful");

	}


	//setPassword- User
	@Test
	public void test_setPasswordU() {
		test_password = "newPasswordhere1";
		testUser.setPassword(test_password);
		assertTrue(testUser.login(test_password));

		System.out.println("Test 3 successful");
	}
	
	//getLastLogin - MeasureStation
	@Test
	public void test_LastLoginMS() {
		String test = "1";
		String NumberAsString = String.valueOf(testMS.getLastLoginTime());
		assertNotSame("", test, NumberAsString);

		System.out.println("Test 4 successful");
	}


	//login - MeasureStatioon
	@Test
	public void test_loginMS() {
		test_password = "insertpassword";
		testMS.setPassword(test_password);
		assertTrue(testMS.login(test_password));

		System.out.println("Test 5 successful");

	}


	//setPassword- MeasureStation
	@Test
	public void test_setPasswordMS() {
		test_password = "newPasswordhere2";
		testMS.setPassword(test_password);
		assertTrue(testMS.login(test_password));

		System.out.println("Test 6 successful");
	}

	

	//hashPassword
	@Test
	public void test_hashPassword() {

        SecureRandom r = new SecureRandom();
        final byte[] salt = new byte[2];
        r.nextBytes(salt);

        this.pwHash = hashPassword(test_password, salt);
        this.salt = salt;

        final byte [] tryHash = hashPassword(test_password, this.salt);
        assertEquals(Arrays.toString(pwHash), Arrays.toString(tryHash));

		System.out.println("Test 7 successful");

	}
    
	private static byte[] hashPassword(final String password, final byte[] salt) {
        PBKDF2WithHmacSHA256HashBuilder builder = new PBKDF2WithHmacSHA256HashBuilder(salt, PasswordAuthorizedTest.ITERATIONS);
        final PBKDF2WithHmacSHA256Hash hash = builder.hash(password);
        return hash.getHash();
    }
       

	@After
	public void tearDown() {
		System.out.println("after test");
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}