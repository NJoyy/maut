package com.antares.cal.service;

import com.antares.cal.config.Time;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Tim Trense
 */
public class TimeTest {

    private static Time time;

    @BeforeClass
    public static void setUpBeforeClass() throws InterruptedException {
        time = Time.newDebugInstance();
        Thread.sleep(1000);
    }

    @AfterClass
    public static void tearDownAfterClass() {
        time.interrupt();
        time = null;
    }

    @Test
    public void testGetTime() {
        System.out.println("Test GetTime");
        testGetTimeImpl();
    }

    private void testGetTimeImpl(){
        final long offset = Math.abs(time.getTime() - System.currentTimeMillis());
        System.out.println("offset = " + offset);
        assertTrue("Timing virtualization offset less then 10 milliseconds",
                offset < 10);
    }

    @Test
    public void testShiftTime() {
        System.out.println("Test ShiftTime");
        final long diff = 10000;
        long one = time.getTime();
        time.shiftTime(diff);
        long two = time.getTime();
        assertTrue("Shift inaccuracy less then 10 milliseconds",
                (two - one - diff) < 10);
    }

    @Test
    public void testResetTime() {
        System.out.println("Test ResetTime");
        time.reset();
        testGetTimeImpl();
    }
}
