package com.antares.cal.service.logic.alert;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertProperty;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.service.logic.alert.AlertHandler;



/**
 * @author Robin Domeier
 * @author Lukas Goethel
 */

public class AlertSystemTest {

	private static AlertSystemTest alertSystem;
	
    private final List<AlertHandler> handlers;
    private final List<Alert> unhandled;
    private Map<String, String> result = new HashMap<>();
    
    private List<AlertProperty> properties = new ArrayList<>();

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("JUnit-Test start");
	}
	
	@Before
	public void setUp() {
		System.out.println("before test");
	}
	
    public AlertSystemTest() {
        handlers = new ArrayList<>();
        unhandled = new LinkedList<>();
    }
	
	@Test
	public void test_properties() {		
        
		alertSystem = new AlertSystemTest();
		String key = "insertKeyHere";
		String message = "insertMessageHere";
		result.put(key, message);
		
		alertSystem.fireAlert(AlertType.CAR_MISSING, message, alertSystem.getPropertiesMap(result));
		alertSystem.fireAlert(AlertType.STOLEN_CAR, message, alertSystem.getPropertiesMap(result));

		assertFalse(alertSystem.getUnhandled());
		System.out.println("Test 1 successful");

	}

    
    //AlertSystem - Copys
    
    //changed return value to boolean from List<Alert>
    //added if clause
    public boolean getUnhandled() {
        final List<Alert> result;
        boolean testreturn = true;
        synchronized (unhandled) {
            result = Collections.unmodifiableList(unhandled);
            

            if (result.isEmpty() == true) {
            	testreturn = true;
            }else {
            	testreturn = false;
            }
        }
        return testreturn;
    }
    
    public Alert fireAlert(final AlertType type, final String message, final String... infoFlat) {
        if (infoFlat.length % 2 != 0) {
            throw new IllegalArgumentException("cannot parse flattened machine readable info, be aware of pattern [key1], [value1], [key2], [value2], ...");
        }
        final Map<String, String> info = new HashMap<>();
        for (int i = 0; i < infoFlat.length; i += 2) {
            info.put(infoFlat[i], infoFlat[i + 1]);
        }
        return fireAlert(type, message, info);
    }
    
    public Alert fireAlert(final AlertType type, final String message, final Map<String, String> infos) {
        final Alert alert = new Alert();
        alert.setTime(Time.now());
        alert.setType(type);
        alert.setMessage(message);

        final List<AlertProperty> properties = new LinkedList<>();
        if (infos != null) {
            for (String key : infos.keySet()) {
                final AlertProperty property = new AlertProperty();
                property.setAlert(alert);
                property.setKey(key);
                property.setValue(infos.get(key));
                properties.add(property);
            }
        }
        alert.setProperties(properties);
        alert.setProperty("alert-type", type.name());

        fireAlert(alert);
        return alert;
    }
	
    /*package*/void fireAlert(final Alert alert) {
        synchronized (unhandled) {
            unhandled.add(alert);
        }

        for (AlertHandler handler : handlers) {
            handler.handleFire(alert);
        }
    }
    
	//Alert - Copys
    public Map<String, String> getPropertiesMap(Map<String, String> result) {
        //final Map<String, String> result = new HashMap<>();
        for (AlertProperty p : getProperties()) {
            result.put(p.getKey(), p.getValue());
        }
        return Collections.unmodifiableMap(result);
    }
	
    public AlertProperty getProperty(final String key) {
        for (AlertProperty p : getProperties()) {
            if (p.getKey().equals(key)) {
                return p;
            }
        }
        return null;
    }
    
    public List<AlertProperty> getProperties() {
        return properties;
    }
    
	
	@After
	public void tearDown() {
		System.out.println("after test");
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		System.out.println("JUnit-Test completed");
	}

}