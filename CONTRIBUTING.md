# Aufbau einer Commit-Message
Aufbau Titel + Details.
Titel ist frei wählbar (sollte aber sinnvoll sein).
Die Details umfassen JEDE gemachte Änderung und werden je Änderung nach folgendem Schema zeilenweise geschrieben: _Typ_ _Beschreibung_ in _Dateiname_.
Zulässige Typen sind:
- Add _method/class/feature/algorithm-Name_ in _Datei_
- Remove _method/class/feature/algorithm-Name_ in _Datei_
- Bugfix _BugName_
- Update _Beschreibung_ in _Datei_
- Reformat _Datei_
- Minor _Beschreibung_ in _Datei_
- Implement _Feature_ in _Datei_, _Date_, _Datei_,...

Der erste Commit eines neuen Branches heißt "branch commit" und enthält als Detail die Beschreibung, wofür der Branch dient. 

# Commit-Sprache
Als Sprache für Commit-Messages ist ausschließlich Englisch zu wählen. 