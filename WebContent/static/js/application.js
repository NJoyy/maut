/**
 *
 * @return {string} the browsers default language code (eg "de" for german, "en" for english)
 */
function getBrowserLanguage() {
    // noinspection JSDeprecatedSymbols
    return navigator.language || navigator.userLanguage;
}

/**
 * translates the given object, that may either be an HTML- or jQuery-Element, a translation key (string) or any toString-able object.
 * if the object is an HTML/jQuery-Element, the function will automatically pick the text() of it as the translation key
 * and will automatically replace that caption by the translation.
 *
 * A translation key may be "default.greeting.hello" that refers to "Hallo" for language="de" and to "Ciao" for language="it"
 *
 * @param obj {*} the thing to translate
 * @param language {string} {optional} the target language code, if absent getBrowserLanguage will be used
 * @return {string} the translated text
 */
function translate(obj, language) {
    var key;
    if (obj instanceof jQuery) {
        key = obj.text().trim();
    } else if (obj instanceof HTMLElement) {
        key = $(obj).text().trim();
    } else if (obj instanceof String) {
        key = obj;
    } else {
        key = obj.toString();
    }
    if (language == null) {
        language = getBrowserLanguage();
    }
    if (window[language] == null) {
        console.log("no translation table for browser language: " + language);
        return null;
    }
    var text = window[language][key];
    if (text == null) {
        return key;
    } else {

        if (obj instanceof HTMLElement) {
            $(obj).text(text);
        } else if (obj instanceof jQuery) {
            obj.text(text);
        }

        return text;
    }
}

/**
 * queries the translation table for the given language from the server's "translation"-controller.
 * if that fails, the function will try to load the default  which is "de"
 * @param language the language code (eg "en" for english)
 */
function loadTranslation(language) {
    $.getJSON("/translation/" + language, {}, function (response) {
        window[language] = response;
    }).fail(function () {
        console.log("loading translation table for " + language + " failed");
        if (language !== "de") {
            loadTranslation("de");
            // reload "de" for default
        }
    });
}

/**
 * INIT-Script
 */
// WARNING: DO NOT RELY ON THIS. Always include jQuery statically in your layout!!!
// auto-load jQuery
if (jQuery == null) {
    var $;
    var script = document.createElement("SCRIPT");
    script.src = '/static/jquery-3.3.1-min.js';
    script.type = 'text/javascript';
    script.onload = function () {
        $ = window.jQuery;
    };
    document.getElementsByTagName("head")[0].appendChild(script);

    loadTranslation(getBrowserLanguage());
}
