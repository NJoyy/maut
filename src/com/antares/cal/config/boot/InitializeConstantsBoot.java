package com.antares.cal.config.boot;

import com.antares.cal.config.Global;

import java.math.BigDecimal;

/**
 * Initializes the constant PRICE_PER_KM as BigDecimal in EURO
 *
 * @author Tim Trense
 */
class InitializeConstantsBoot extends BootFunction {

    InitializeConstantsBoot(BootStrap bootStrap) {
        super(bootStrap);
    }

    @Override
    void onInit() {
        Global.set("PRICE_PER_KM", new BigDecimal(0.1));
        Global.set("TAXRATE_DRIVE", new BigDecimal(0.1));
        Global.set("TAXRATE_LOCALISATION", new BigDecimal(0.19));
    }
}
