package com.antares.cal.config.boot;

import com.antares.cal.config.Global;
import com.antares.cal.utility.ResourceLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Initializes the ResourceLoader
 *
 * @author Tim Trense
 */
class SetupFileLocations extends BootFunction {
    SetupFileLocations(BootStrap bootStrap) {
        super(bootStrap);
    }

    @Override
    void onInit() {
        ResourceLoader fileLocator;


        // FOR ECLIPSE AND PRODUCTION
        fileLocator = new ResourceLoader() {
            @Override
            public URL locate(String filename) {
                if (!filename.startsWith("/")) {
                    filename = "/" + filename;
                }
                final String path = System.getProperty("jboss.server.base.dir") +
                        "/deployments/" +
                        Global.get("DEPLOYMENT_NAME") +
                        filename;
                try {
                    return new URL("file:///" + path);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        // assume that the web.xml is deployed
        final File f = new File(fileLocator.locate("WEB-INF/web.xml").getPath());

        if (!f.exists()) {
            // FOR INTELLIJ IDEA
            fileLocator = new ResourceLoader() {
                // IntelliJ uses a virtual filesystem for deployment and the ClassLoader is linked to it.
                // that's why we load resources from there
                @Override
                public URL locate(String filename) {
                    return ResourceLoader.class.getClassLoader().getResource(filename);
                }
            };
        }

        Global.set("RES_LOADER", fileLocator);
    }
}
