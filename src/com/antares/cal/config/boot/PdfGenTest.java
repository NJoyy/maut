package com.antares.cal.config.boot;

import com.antares.cal.config.Global;
import com.antares.cal.config.Time;
import com.antares.cal.model.invoice.Invoice;
import com.antares.cal.model.user.Gender;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.logic.invoice.InvoiceService;
import com.antares.cal.utility.pdfGen.BillGenerator;
import com.antares.cal.utility.pdfGen.data.PdfDataDrive;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoice;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoiceItem;
import com.antares.cal.utility.pdfGen.data.PdfDataLocalisation;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

class PdfGenTest extends BootFunction {

	PdfGenTest(BootStrap bootStrap) {
		super(bootStrap);
	}
	
	@Override
	void onInit() throws IOException {
		ArrayList<PdfDataInvoiceItem> items0 = new ArrayList<>();
		items0.add(new PdfDataDrive("D-BIW-AA-1097", new Date(1538474400000L), "A4-19", "A17-21", 76000, new BigDecimal(7.6)));
		items0.add(new PdfDataDrive("D-BIW-AA-1097", new Date(1538733600000L), "A17-12", "A2-47", 12000, new BigDecimal(1.2)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1538906400000L), "A32-10", "A14-8", 7000, new BigDecimal(0.7)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1539252000000L), "A11-18", "A5-75", 89000, new BigDecimal(8.9)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1539943200000L), "A5-111", "A3-26", 95000, new BigDecimal(9.5)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1540029600000L), "A4-15", "A9-78", 20000, new BigDecimal(2.0)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1540288800000L), "A17-18", "A19-56", 11000, new BigDecimal(1.1)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1540724400000L), "A4-1", "A10-5", 5000, new BigDecimal(0.5)));
		items0.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1540897200000L), "A17-17", "A32-38", 3000, new BigDecimal(0.3)));
		items0.add(new PdfDataLocalisation(2, "D-NOL-XZ-1801", new BigDecimal(2)));
		items0.add(new PdfDataLocalisation(8, "D-BIW-AA-1097", new BigDecimal(8)));
		
		ArrayList<PdfDataInvoiceItem> items1 = new ArrayList<>();
		items1.add(new PdfDataDrive("D-BIW-AA-1097", new Date(1535796000000L), "A4-22", "A17-21", 8000, new BigDecimal(0.8)));
		items1.add(new PdfDataDrive("D-BIW-AA-1097", new Date(1536573600000L), "A17-13", "A2-7", 10000, new BigDecimal(1.0)));
		items1.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1537092000000L), "A30-10", "A14-8", 111000, new BigDecimal(11.1)));
		items1.add(new PdfDataDrive("D-NOL-XZ-1801", new Date(1537610400000L), "A12-18", "A5-75", 89000, new BigDecimal(8.9)));
		items1.add(new PdfDataLocalisation(3, "D-NOL-XZ-1801", new BigDecimal(3)));
		items1.add(new PdfDataLocalisation(1, "D-BIW-AA-1097", new BigDecimal(1)));

		int invoiceNumber = 0x123;

		PdfDataInvoice pdi0 = new PdfDataInvoice("Jochen", "Rübli", Gender.MALE, "Bergstraße", "43c", "2504", "Biel", "100000789",
				new Date(1540983600000L), 0.1, 0.19, items0);
		BillGenerator bg = new BillGenerator();
		byte[] invoicePdfFile0 = bg.generate(pdi0);
		Invoice invoice = new Invoice();
		OwnerUser user = Global.get("jruebli-user-1");
		invoice.setOwnerId(user.getId());
		invoice.setInvoiceDate(1540983600000L);
		invoice.setInvoiceNumber(invoiceNumber++);
		invoice.setNetAmount(new BigDecimal(41.80).setScale(2, RoundingMode.FLOOR));
		invoice.setPaid(false);
		invoice.setTaxAmount(new BigDecimal(5.08).setScale(2, RoundingMode.FLOOR));
		invoice.setInvoiceFile(invoicePdfFile0);
		getBootStrap().getEntityManager().persist(invoice);
		
		PdfDataInvoice pdi1 = new PdfDataInvoice("Jochen", "Rübli", Gender.MALE, "Bergstraße", "43c", "2504", "Biel", "100000788",
				new Date(1538301600000L), 0.1, 0.19, items1);
		byte[] invoicePdfFile1 = bg.generate(pdi1);
		Invoice i = new Invoice();
		i.setOwnerId(user.getId());
		i.setInvoiceDate(1538301600000L);
		i.setInvoiceNumber(invoiceNumber);
		i.setNetAmount(new BigDecimal(25.80).setScale(2, RoundingMode.FLOOR));
		i.setPaid(true);
		i.setTaxAmount(new BigDecimal(2.941).setScale(2, RoundingMode.FLOOR));
		i.setInvoiceFile(invoicePdfFile1);
		getBootStrap().getEntityManager().persist(i);
		
		//is.createPdfInvoice(invoice);
	}
	
	@Override
    void onDestroy() {
		
	}

}
