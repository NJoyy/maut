package com.antares.cal.config.boot;

import com.antares.cal.config.Time;

/**
 * Loads the global timing context
 * @author Tim Trense
 */
class StartTimeServiceBoot extends BootFunction {

    StartTimeServiceBoot(BootStrap bootStrap) {
        super(bootStrap);
    }

    @Override
    void onInit() {
        Time.getInstance();
    }

    @Override
    void onDestroy() {
        Time.getInstance().interrupt();
    }
}
