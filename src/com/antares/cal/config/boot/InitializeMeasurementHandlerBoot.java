package com.antares.cal.config.boot;

import com.antares.cal.config.Global;
import com.antares.cal.service.logic.measurement.MeasurementHandlerImpl;

/**
 * Boot action to initialize the default applications measurement handler
 *
 * @author Tim Trense
 */
class InitializeMeasurementHandlerBoot extends BootFunction {

    InitializeMeasurementHandlerBoot(BootStrap bootStrap) {
        super(bootStrap);
    }

    @Override
    void onInit() {
        final MeasurementHandlerImpl handler = new MeasurementHandlerImpl();
        // if necessary, init handler
        Global.set("measurement-handler", handler);
    }
}
