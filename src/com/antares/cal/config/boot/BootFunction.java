package com.antares.cal.config.boot;

/**
 * Any BootFunction needs to be registered in {@link BootStrap}-Constructor
 * @author Tim Trense
 */
// BootFunctions are package-private and all need to be declared here
abstract class BootFunction {

    private final BootStrap bootStrap;

    BootFunction(BootStrap bootStrap) {
        this.bootStrap = bootStrap;
    }

    /**
     *
     * @return access to the bootstrap context
     */
    @SuppressWarnings("WeakerAccess")
    protected BootStrap getBootStrap() {
        return bootStrap;
    }

    /**
     * invoked on application startup
     * @throws Exception if any exception occurs (this will not halt the application, but will be catched)
     */
    @SuppressWarnings("RedundantThrows")
    void onInit() throws Exception{
        // does nothing by default
    }

    /**
     * invoked on application shutdown
     * @throws Exception if any exception occurs (this will not halt the application, but will be catched)
     */
    @SuppressWarnings("RedundantThrows")
    void onDestroy() throws Exception{
        // does nothing by default
    }
}
