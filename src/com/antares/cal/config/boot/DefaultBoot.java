package com.antares.cal.config.boot;

import com.antares.cal.config.Global;

/**
 * Mockup {@link BootFunction}
 *
 * @author Tim Trense
 */
public class DefaultBoot extends BootFunction {

    DefaultBoot(BootStrap bootStrap) {
        super(bootStrap);
    }

    @Override
    public void onInit() {
        Global.set("DEPLOYMENT_NAME", "maut.war");
        System.out.println("Application Startup");
    }

    @Override
    public void onDestroy() {
        System.out.println("Application Shutdown");
    }
}
