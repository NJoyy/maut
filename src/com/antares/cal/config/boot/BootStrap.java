package com.antares.cal.config.boot;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.LinkedList;
import java.util.List;

/**
 * This servlet is invoked on application startup
 *
 * @author Tim Trense
 */
@Singleton
@Startup
public class BootStrap {

    @PersistenceContext(name = "cal-maut")
    private EntityManager entityManager;
    private final List<BootFunction> bootFunctions = new LinkedList<>();

    @PostConstruct
    public void init() {
        System.out.println("Boot: Setup Boot-Function-Order");
        // setup boot order

        // 1: initialize environment data
        bootFunctions.add(new DefaultBoot(this));
        bootFunctions.add(new SetupFileLocations(this));
        bootFunctions.add(new StartTimeServiceBoot(this));

        // 2: initialize default master data
        bootFunctions.add(new TestDataBoot(this));
        bootFunctions.add(new InitializeConstantsBoot(this));
        bootFunctions.add(new PdfGenTest(this));

        // 3: initialize services
        bootFunctions.add(new InitializeMeasurementHandlerBoot(this));

        // execute boot
        callInits();

        System.out.println("Boot: Complete");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Boot: Stopping Boot-Functions");
        callDestroys();
        System.out.println("Boot: Stop Complete");
    }


    private void callInits() {
        System.out.println("Boot: Invoking Boot Functions");
        for (BootFunction f : bootFunctions) {
            try {
                System.out.println("Boot: Invoking " + f.getClass().getSimpleName());
                f.onInit();
            } catch (final Throwable t) {
                t.printStackTrace();
            }
        }
    }

    private void callDestroys() {
        System.out.println("Boot: Invoking Boot-Stop Functions");
        for (BootFunction f : bootFunctions) {
            try {
                System.out.println("Boot: Invoking Stop " + f.getClass().getSimpleName());
                f.onDestroy();
            } catch (final Throwable t) {
                t.printStackTrace();
            }
        }
    }

    /**
     * @return access to the boot entity manager
     */
    EntityManager getEntityManager() {
        return entityManager;
    }
}
