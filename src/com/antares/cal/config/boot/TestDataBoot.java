package com.antares.cal.config.boot;

import com.antares.cal.config.Global;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.CarType;
import com.antares.cal.model.car.Drive;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.model.measurement.MeasureStationType;
import com.antares.cal.model.user.*;
import com.antares.cal.service.logic.alert.AlertSystem;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Initializes one jruebli user on boot
 *
 * @author Tim Trense
 * @author Georg Claude
 */
class TestDataBoot extends BootFunction {

    private OwnerUser jruebli;
    private AgencyUser gov;
    private AgencyUser admin;
    private AgencyUser police;
    private List<MeasureStation> measureStationList = new ArrayList<>();
    private List<Driveway> drivewayList = new ArrayList<>();

    TestDataBoot(BootStrap bootStrap) {
        super(bootStrap);
    }

    private Random rnd = new Random();

    String[] fNames = {"Arnold", "Angela", "Annegret", "Bernd", "Bert", "Birgit", "Barbara", "Claudia",
            "Claus", "Chantal", "Detlef", "Dieter", "Doris", "Eberhardt", "Elise", "Ernie",
            "Gerolf", "Gretel", "Heinz", "Hans", "Horst", "Heinrich", "Hilde", "Ingrid",
            "Igor", "Ingo", "Jürgen", "Klaus", "Kurt", "Norbert", "Olga", "Petra",
            "Peter", "Rudolf", "Rüdiger", "Rolf", "Sabine", "Siegfried", "Siegbert", "Walter"};

    String[] lNames = {"Anders", "Braun", "Christianssen", "Dietrich", "Esbert", "Engel", "Friedmann", "Friedbert",
            "Grossmann", "Gassmann", "Günters", "Ganner", "Hoffman", "Hartmann", "Ingolf", "Jürgensson",
            "Kleinmann", "Klausthal", "Meurich", "Maulich", "Nachtmann", "Osbert", "Obers", "Pjotka",
            "Raumann", "Riedel", "Ritter", "Raller", "Renner", "Schleich", "Trobert", "Ulfinger",
            "Vetterli", "Vossmann", "Walters", "Zaunmann", "Zerger", "Zwegat", "Ziehmlich", "Zuger"};

    String[] citys = {"Bern", "Berlin", "München", "Maybach", "Bautzen", "Düsseldorf", "Wilthen", "Ulm",
            "Nordhausen", "Hessen", "Frankfurt", "Suhl", "Reutlingen", "Stuttgart", "Esslingen", "Hanau"};

    String[] streets = {"Waldstrasse", "Kreuzstr.", "Ringstraße", "Oststrasse", "Hauptstr.", "Berliner Str.",
            "Walldorfstr.", "Entenschenker Straße", "Nordstrasse", "Frankenfelder Str.", "Neuendorfer Str.",
            "Löbauer Str.", "Dorfstrasse", "Weinstrasse", "Randstrasse", "Engelbertstraße"};

    private String generateHouseNumber() {
        String[] c = {"a", "b", "c", "d", "e"};
        int num = rnd.nextInt(200) + 1;
        return Integer.toString(num) + c[rnd.nextInt(5)];
    }

    private String generateZipCode() {
        StringBuilder num = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            num.append(Integer.toString(rnd.nextInt(10)));
        }
        return num.toString();
    }

    private Country generateCountry() {
        ArrayList<Country> countries = new ArrayList<>();
        Collections.addAll(countries, Country.values());
        return countries.get(rnd.nextInt(countries.size()));
    }

    private BillingPreference generateBillingPreference() {
        ArrayList<BillingPreference> bps = new ArrayList<>();
        Collections.addAll(bps, BillingPreference.values());
        return bps.get(rnd.nextInt(bps.size()));
    }

    private String generateEmail(String fName, String lName) {
        return fName + "." + lName + "@" + "mail.com";
    }

    private String generateLoginName(String fName, String lName) {
        return fName.substring(0, 1) + lName;
    }

    @Override
    void onInit() {
        createJRuebli();
        createGov();
        createAdmin();
        createPolice();
        createStreetmap();
        createAlerts();
    }
    
	private void createAlerts() {
		AlertSystem.getInstance().fireAlert(AlertType.INFO, "Test-Info");
		AlertSystem.getInstance().fireAlert(AlertType.CAR_MISSING, "Test-Info Police");
		AlertSystem.getInstance().fireAlert(AlertType.MEASURESTATION_OFFLINE, "Test-Info GOV");
		AlertSystem.getInstance().fireAlert(AlertType.RESCUE_LANE_SPEEDER, "Test-Info Police 2");
	}

    private void createStreetmap() {
        MeasureStation bza, bze, dda1, dda2, dda3, dde1, dde2, dde3, ba, be, ka, ke, mu, li, st, fr, koe, han, ham;
        MeasureStation m;

        // Bautzen - nur Richtung Dresden
        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(1L);
        m.setPassword("pw1");
        m.setName("Bautzen Durchfahrt Richtung Dresden");
        m.setShortName("bza1"); // bautzen approach 1
        measureStationList.add(m);
        bza = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(2L);
        m.setPassword("pw2");
        m.setName("Bautzen Abfahrt von Dresden kommend");
        m.setShortName("bze1"); // bautzen exit 1
        measureStationList.add(m);
        bze = m;

        // Dresden - Dreieck Richtung Bautzen, Berlin, Kassel
        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(3L);
        m.setPassword("pw3");
        m.setName("Dresden Durchfahrt Richtung Bautzen");
        m.setShortName("dda1");
        measureStationList.add(m);
        dda1 = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(4L);
        m.setPassword("pw4");
        m.setName("Dresden Durchfahrt Richtung Berlin");
        m.setShortName("dda2");
        measureStationList.add(m);
        dda2 = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(5L);
        m.setPassword("pw5");
        m.setName("Dresden Durchfahrt Richtung Kassel");
        m.setShortName("dda3");
        measureStationList.add(m);
        dda3 = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(6L);
        m.setPassword("pw6");
        m.setName("Dresden Abfahrt von Bautzen kommend");
        m.setShortName("dde1");
        measureStationList.add(m);
        dde1 = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(7L);
        m.setPassword("pw7");
        m.setName("Dresden Abfahrt von Berlin kommend");
        m.setShortName("dde2");
        measureStationList.add(m);
        dde2 = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(8L);
        m.setPassword("pw8");
        m.setName("Dresden Abfahrt von Kassel kommend");
        m.setShortName("dde3");
        measureStationList.add(m);
        dde3 = m;

        // Berlin - nur Richtung Dresden
        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(9L);
        m.setPassword("pw9");
        m.setName("Berlin Durchfahrt Richtung Dresden");
        m.setShortName("ba1");
        measureStationList.add(m);
        ba = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(10L);
        m.setPassword("pw10");
        m.setName("Berlin Abfahrt von Dresden kommend");
        m.setShortName("be1");
        measureStationList.add(m);
        be = m;

        // Kassel - nur Richtung Dresden
        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(11L);
        m.setPassword("pw11");
        m.setName("Kassel Durchfahrt Richtung Dresden");
        m.setShortName("ka1");
        measureStationList.add(m);
        ka = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(12L);
        m.setPassword("pw12");
        m.setName("Kassel Abfahrt von Dresden kommend");
        m.setShortName("ke1");
        measureStationList.add(m);
        ke = m;

        //New MeasureStations for Testscenario // Presentation

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(131L);
        m.setPassword("pw131");
        m.setName("Muenchen Auffahrt nach Lindau");
        m.setShortName("mu");
        measureStationList.add(m);
        mu = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(129L);
        m.setPassword("pw129");
        m.setName("Lindau Auffahrt nach Stuttgart");
        m.setShortName("li");
        measureStationList.add(m);
        li = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(127L);
        m.setPassword("pw127");
        m.setName("Stuttgart Auffahrt nach Frankfurt");
        m.setShortName("st");
        measureStationList.add(m);
        st = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(111L);
        m.setPassword("pw111");
        m.setName("Frankfurt Auffahrt nach Koeln");
        m.setShortName("fr");
        measureStationList.add(m);
        fr = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(121L);
        m.setPassword("pw121");
        m.setName("Koeln Auffahrt nach Hannover");
        m.setShortName("koe");
        measureStationList.add(m);
        koe = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.APPROACH);
        m.setInterfaceId(117L);
        m.setPassword("pw117");
        m.setName("Hannover Auffahrt nach Hamburg");
        m.setShortName("han");
        measureStationList.add(m);
        han = m;

        m = new MeasureStation();
        m.setType(MeasureStationType.EXIT);
        m.setInterfaceId(116L);
        m.setPassword("pw116");
        m.setName("Hamburg Abfahrt von Hannover kommend");
        m.setShortName("ham");
        measureStationList.add(m);
        ham = m;

        for (MeasureStation measureStation : measureStationList) {
            getBootStrap().getEntityManager().persist(measureStation);
        }

        Driveway d;

        // von Bautzen nach Dresden
        d = new Driveway();
        d.setName("A4 Richtung Dresden Durchfahrt nach Berlin");
        d.setStartPoint(bza);
        d.setEndPoint(dda2);
        d.setDistance(63000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A4 Richtung Dresden Durchfahrt nach Kassel");
        d.setStartPoint(bza);
        d.setEndPoint(dda3);
        d.setDistance(63000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A4 Richtung Dresden Abfahrt");
        d.setStartPoint(bza);
        d.setEndPoint(dde1);
        d.setDistance(63000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // von Dresden nach Bautzen
        d = new Driveway();
        d.setName("A4 Richtung Bautzen Abfahrt");
        d.setStartPoint(dda1);
        d.setEndPoint(bze);
        d.setDistance(63000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // von Berlin nach Dresden
        d = new Driveway();
        d.setName("A13 Richtung Dresden Durchfahrt nach Bautzen");
        d.setStartPoint(ba);
        d.setEndPoint(dda1);
        d.setDistance(193000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A13 Richtung Dresden Durchfahrt nach Kassel");
        d.setStartPoint(ba);
        d.setEndPoint(dda3);
        d.setDistance(193000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A13 Richtung Dresden Abfahrt");
        d.setStartPoint(ba);
        d.setEndPoint(dde2);
        d.setDistance(193000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // von Dresden nach Berlin
        d = new Driveway();
        d.setName("A13 Richtung Berlin Abfahrt");
        d.setStartPoint(dda2);
        d.setEndPoint(be);
        d.setDistance(193000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // von Kassel nach Dresden
        d = new Driveway();
        d.setName("A38 Richtung Dresden Durchfahrt nach Bautzen");
        d.setStartPoint(ka);
        d.setEndPoint(dda1);
        d.setDistance(353000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A38 Richtung Dresden Durchfahrt nach Berlin");
        d.setStartPoint(ka);
        d.setEndPoint(dda2);
        d.setDistance(353000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("A38 Richtung Dresden Abfahrt");
        d.setStartPoint(ka);
        d.setEndPoint(dde3);
        d.setDistance(353000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // von Dresden nach Kassel
        d = new Driveway();
        d.setName("A38 Richtung Kassel Abfahrt");
        d.setStartPoint(dda3);
        d.setEndPoint(ke);
        d.setDistance(353000);
        d.setAverageSpeed(35);
        drivewayList.add(d);



        // Muenchen bis Hannover ONEWAY

        // Muenchen Auffahrt nach Lindau
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Lindau");
        d.setStartPoint(mu);
        d.setEndPoint(li);
        d.setDistance(230000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // Lindau Auffahrt nach Stuttgart
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Stuttgart");
        d.setStartPoint(li);
        d.setEndPoint(st);
        d.setDistance(160000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // Stuttgart Auffahrt nach Frankfurt
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Frankfurt");
        d.setStartPoint(st);
        d.setEndPoint(fr);
        d.setDistance(270000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // Frankfurt Auffahrt nach Koeln
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Koeln");
        d.setStartPoint(fr);
        d.setEndPoint(koe);
        d.setDistance(230000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // Koeln Auffahrt nach Hannover
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Hannover");
        d.setStartPoint(koe);
        d.setEndPoint(han);
        d.setDistance(410000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        // Hannover Auffahrt nach Hamburg
        d = new Driveway();
        d.setName("A11 Durchfahrt Richtung Hamburg");
        d.setStartPoint(han);
        d.setEndPoint(ham);
        d.setDistance(170000);
        d.setAverageSpeed(35);
        drivewayList.add(d);

        /*
        d = new Driveway();
        d.setName("Abfahrt Dresden");
        d.setDistance(2000);
        d.setStartPoint(dda1);
        d.setEndPoint(dde1);
        d.setAverageTimeToCoverSeconds(5 * 60);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("Abfahrt Kassel");
        d.setDistance(2000);
        d.setStartPoint(ka);
        d.setEndPoint(ke);
        d.setAverageTimeToCoverSeconds(5 * 60);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("Abfahrt Bautzen");
        d.setDistance(2000);
        d.setStartPoint(bza);
        d.setEndPoint(bze);
        d.setAverageTimeToCoverSeconds(5 * 60);
        drivewayList.add(d);

        d = new Driveway();
        d.setName("Abfahrt Berlin");
        d.setDistance(2000);
        d.setStartPoint(ba);
        d.setEndPoint(be);
        d.setAverageTimeToCoverSeconds(5 * 60);
        drivewayList.add(d);*/

        for (Driveway driveway : drivewayList) {
            getBootStrap().getEntityManager().persist(driveway);
        }
    }

    private void createJRuebli() {
        jruebli = new OwnerUser();
        Address address = new Address();
        address.setCity("Oberkloppstockenheim");
        address.setCountry(Country.SWITZERLAND);
        address.setHouseNumber("43c");
        address.setStreet("Unterstuckenheimer Strasse");
        address.setZipCode("2504");
        jruebli.setAddress(address);

        jruebli.setBillingPreference(BillingPreference.EMAIL);

        jruebli.setEmail("juergen.ruebli@swiss.ch");
        jruebli.setLoginName("jruebli");
        jruebli.setPassword(".jruebli");

        PersonalData pd = new PersonalData();
        pd.setFirstName("Jochen");
        pd.setGender(Gender.MALE);
        pd.setLastName("Ruebli");
        jruebli.setPersonalData(pd);

        Car car = new Car();

        LicenseNumber licenceNumber = new LicenseNumber(Country.GERMANY, "BZ AB123");
        car.setLicenseNumber(licenceNumber);
        car.setCarType(CarType.PRIVATE);
        car.setOwnerUser(this.jruebli);

        jruebli.addCar(car);

        Car car1 = new Car();
        LicenseNumber licenseNumber1 = new LicenseNumber(Country.GERMANY, "DD BA2016");
        car1.setLicenseNumber(licenseNumber1);
        car1.setCarType(CarType.PRIVATE);
        car1.setOwnerUser(this.jruebli);

        jruebli.addCar(car1);
        
        Drive drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(6000);
        drive.setTime(1541600695132L);
        drive.setCar(car1);
        getBootStrap().getEntityManager().persist(drive);

        Car car2 = new Car();
        LicenseNumber licenseNumber2 = new LicenseNumber(Country.GERMANY, "M BA2017");
        car2.setLicenseNumber(licenseNumber2);
        car2.setCarType(CarType.PRIVATE);
        car2.setOwnerUser(this.jruebli);

        jruebli.addCar(car2);
        
        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(3000);
        drive.setTime(1541600695132L);
        drive.setCar(car2);
        getBootStrap().getEntityManager().persist(drive);

        Car car3 = new Car();
        LicenseNumber licenseNumber3 = new LicenseNumber(Country.GERMANY,"BZ BA2018");
        car3.setLicenseNumber(licenseNumber3);
        car3.setCarType(CarType.PRIVATE);
        car3.setOwnerUser(this.jruebli);

        jruebli.addCar(car3);
        
        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(5000);
        drive.setTime(1541600695132L);
        drive.setCar(car3);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car4 = new Car();
        LicenseNumber licenseNumber4 = new LicenseNumber(Country.POLAND,"KM ST000");
        car4.setLicenseNumber(licenseNumber4);
        car4.setCarType(CarType.PRIVATE);
        car4.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car4);  
        
        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(200);
        drive.setTime(1541600695132L);
        drive.setCar(car4);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car5 = new Car();
        LicenseNumber licenseNumber5 = new LicenseNumber(Country.NETHERLANDS,"KM ST111");
        car5.setLicenseNumber(licenseNumber5);
        car5.setCarType(CarType.PRIVATE);
        car5.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car5);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(600);
        drive.setTime(1541600695132L);
        drive.setCar(car5);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car6 = new Car();
        LicenseNumber licenseNumber6 = new LicenseNumber(Country.FRANCE,"KM ST222");
        car6.setLicenseNumber(licenseNumber6);
        car6.setCarType(CarType.PRIVATE);
        car6.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car6);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(1800);
        drive.setTime(1541600695132L);
        drive.setCar(car6);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car7 = new Car();
        LicenseNumber licenseNumber7 = new LicenseNumber(Country.AUSTRIA,"KM ST333");
        car7.setLicenseNumber(licenseNumber7);
        car7.setCarType(CarType.PRIVATE);
        car7.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car7);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(6000);
        drive.setTime(1541600695132L);
        drive.setCar(car7);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car8 = new Car();
        LicenseNumber licenseNumber8 = new LicenseNumber(Country.RUSSIA,"KM ST444");
        car8.setLicenseNumber(licenseNumber8);
        car8.setCarType(CarType.PRIVATE);
        car8.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car8);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(7630);
        drive.setTime(1541600695132L);
        drive.setCar(car8);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car9 = new Car();
        LicenseNumber licenseNumber9 = new LicenseNumber(Country.CZECH_REPUBLIC,"KM ST555");
        car9.setLicenseNumber(licenseNumber9);
        car9.setCarType(CarType.PRIVATE);
        car9.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car9);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(3400);
        drive.setTime(1541600695132L);
        drive.setCar(car9);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car10 = new Car();
        LicenseNumber licenseNumber10 = new LicenseNumber(Country.LIECHTENSTEIN,"KM ST666");
        car10.setLicenseNumber(licenseNumber10);
        car10.setCarType(CarType.PRIVATE);
        car10.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car10);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(480);
        drive.setTime(1541600695132L);
        drive.setCar(car10);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car11 = new Car();
        LicenseNumber licenseNumber11 = new LicenseNumber(Country.SPAIN,"KM ST777");
        car11.setLicenseNumber(licenseNumber11);
        car11.setCarType(CarType.PRIVATE);
        car11.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car11);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(2200);
        drive.setTime(1541600695132L);
        drive.setCar(car11);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car12 = new Car();
        LicenseNumber licenseNumber12 = new LicenseNumber(Country.GREECE,"KM ST888");
        car12.setLicenseNumber(licenseNumber12);
        car12.setCarType(CarType.PRIVATE);
        car12.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car12);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(2400);
        drive.setTime(1541600695132L);
        drive.setCar(car12);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car13 = new Car();
        LicenseNumber licenseNumber13 = new LicenseNumber(Country.ITALY,"KM ST999");
        car13.setLicenseNumber(licenseNumber13);
        car13.setCarType(CarType.PRIVATE);
        car13.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car13);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(3000);
        drive.setTime(1541600695132L);
        drive.setCar(car13);
        getBootStrap().getEntityManager().persist(drive);
        
        Car car14 = new Car();
        LicenseNumber licenseNumber14 = new LicenseNumber(Country.SWITZERLAND,"KM ST001");
        car14.setLicenseNumber(licenseNumber14);
        car14.setCarType(CarType.PRIVATE);
        car14.setOwnerUser(this.jruebli);
        
        jruebli.addCar(car14);

        drive = new Drive();
        drive.setApproachPoint("m5");
        drive.setExitPoint("m6");
        drive.setDistance(180);
        drive.setTime(1541600695132L);
        drive.setCar(car14);
        getBootStrap().getEntityManager().persist(drive);
        

        getBootStrap().getEntityManager().persist(jruebli);
        Global.set("jruebli-user-1", this.jruebli);
    }

    private void createGov() {
        gov = new AgencyUser();
        gov.setAgencyType(AgencyType.GOVERNMENT);
        gov.setLoginName("gov");
        gov.setPassword(".gov");
        gov.setEmail("gov@germany.de");
        getBootStrap().getEntityManager().persist(gov);
        Global.set("gov-user-1", gov);
    }

    private void createAdmin() {
        admin = new AgencyUser();
        admin.setAgencyType(AgencyType.ADMIN);
        admin.setLoginName("admin");
        admin.setPassword(".admin");
        admin.setEmail("admin@germany.de");
        getBootStrap().getEntityManager().persist(admin);
        Global.set("admin-user-1", admin);
    }

    private void createPolice() {
        police = new AgencyUser();
        police.setAgencyType(AgencyType.POLICE);
        police.setLoginName("police");
        police.setPassword(".police");
        police.setEmail("police@germany.de");
        getBootStrap().getEntityManager().persist(police);
        Global.set("police-user-1", police);

        getBootStrap().getEntityManager().persist(police);
    }

    @Override
    void onDestroy() {
        final EntityManager em = getBootStrap().getEntityManager();
        em.remove(em.merge(jruebli));
        em.remove(em.merge(gov));
        em.remove(em.merge(admin));
        em.remove(em.merge(police));

        for (MeasureStation measureStation : measureStationList) {
            em.remove(em.merge(measureStation));
        }
        for (Driveway driveway : drivewayList) {
            em.remove(em.merge(driveway));
        }
    }
}

