package com.antares.cal.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Global Variables Singleton
 *
 * @author Tim Trense
 */
public class Global {

    private static Global instance;

    public static Global getInstance() {
        if (instance == null) {
            instance = new Global();
        }
        return instance;
    }

    /**
     * global public variables
     */
    private Map<String, Object> variables;

    private Global() {
        variables = new HashMap<>();
    }

    /**
     * checks whether there is a variable with the given name
     *
     * @param name the name of the variable
     * @return true if that variable is present and not null
     */
    @SuppressWarnings("WeakerAccess")
    public boolean hasVariable(final String name) {
        return variables.get(name) != null;
    }

    /**
     * checks whether there is a variable with the given name
     *
     * @param name the name of the variable
     * @return true if that variable is present and not null
     */
    public static boolean has(final String name) {
        return getInstance().hasVariable(name);
    }

    /**
     * gives the class of the variable with that name
     * @param name the name of the variable
     * @return the class of the variable or null, if there is no variable with that name
     */
    @SuppressWarnings("WeakerAccess")
    public Class<?> getTypeOfVariable(final String name) {
        Object o = variables.get(name);
        if (o != null) {
            return o.getClass();
        } else {
            return null;
        }
    }

    /**
     * gives the class of the variable with that name
     * @param name the name of the variable
     * @return the class of the variable or null, if there is no variable with that name
     */
    public static Class<?> getType(final String name) {
        return getInstance().getTypeOfVariable(name);
    }

    /**
     * gives access to any global variable
     *
     * @param name the name of the variable
     * @param <T>  the type of the variables value to cast
     * @return the variables value, or null if there is no variable with that name and specified type
     */
    @SuppressWarnings({"unchecked", "WeakerAccess"})
    public <T> T getVariable(final String name) {
        if (!variables.containsKey(name)) {
            return null;
        }
        try {
            return (T) variables.get(name);
        } catch (final ClassCastException e) {
            return null;
        }
    }

    /**
     * gives access to any global variable
     *
     * @param name the name of the variable
     * @param <T>  the type of the variables value to cast
     * @return the variables value, or null if there is no variable with that name
     */
    @SuppressWarnings("unchecked")
    public static <T> T get(final String name) {
        return getInstance().getVariable(name);
    }

    /**
     * gives access to any global variable
     *
     * @param name  the name of the variable
     * @param value the value to set for that variable
     */
    @SuppressWarnings({"unchecked", "WeakerAccess"})
    public void setVariable(final String name, final Object value) {
        variables.put(name, value);
    }

    /**
     * gives access to any global variable
     *
     * @param name  the name of the variable
     * @param value the value to set for that variable
     */
    public static void set(final String name, final Object value) {
        getInstance().setVariable(name, value);
    }
}
