package com.antares.cal.config;

import java.util.LinkedList;
import java.util.List;

/**
 * Global Timing Function
 *
 * @author Tim Trense
 */
@SuppressWarnings("WeakerAccess")
public class Time extends Thread {

    private static Time instance;

    public static Time getInstance() {
        if (instance == null) {
            instance = new Time();
            instance.start();
        }
        return instance;
    }

    public static Time newDebugInstance() {
        Time t = new Time();
        t.setName("Debug Timing Function");
        t.start();
        return t;
    }

    @SuppressWarnings("FieldCanBeLocal")
    private long accuracy = 1;
    @SuppressWarnings("UnusedAssignment")
    private long time = -1;
    private final List<TimeListener> listeners;

    private Time() {
        setName("Global Timing Function");
        listeners = new LinkedList<>();
        time = System.currentTimeMillis();
    }

    /**
     * replaces {@link System#currentTimeMillis()} for the entire application.
     * fast-forwards {@link #getInstance()}.{@link #getTime()}
     *
     * @return MILLISECONDS the current unix timestamp
     */
    public static long now() {
        return getInstance().getTime();
    }


    /**
     * replaces {@link System#currentTimeMillis()} for the entire application.
     * should not be called from public. Use {@link #now()}
     *
     * @return MILLISECONDS the current unix timestamp
     */
    public long getTime() {
        if (time < 0) {
            return System.currentTimeMillis();
        } else {
            return time;
        }
    }

    /**
     * fakes the applications global time.
     * automatically disables useSystemTime
     *
     * @param time MILLISECONDS a unix timestamp. must be positive
     */
    public void setTime(final long time) {
        if (time <= 0) {
            throw new IllegalArgumentException("cannot set a global nonpositive time");
        }
        this.time = time;
        synchronized (listeners) {
            for (TimeListener l : listeners) {
                l.onTimeChanged(time);
            }
        }
    }

    /**
     * removes the time virtualization. {@link #now()} and {@link #getTime()} will now return the system time.
     *
     * @param useSystemTime whether to use system time or virtualized time
     */
    public void setUseSystemTime(boolean useSystemTime) {
        if (useSystemTime) {
            this.time = -1;
        } else {
            this.time = System.currentTimeMillis();
        }
    }

    /**
     * @return whether the current system time is obtained from the system or the virtualization
     */
    public boolean isUsingSystemTime() {
        return time <= 0;
    }

    /**
     * shifts the current virtual time along the given amount.
     * can only be called if time virtualization is enabled
     *
     * @param diff MILLISECONDS the amount of time to travel (positive values to future)
     */
    public void shiftTime(final long diff) {
        if (isUsingSystemTime()) {
            throw new IllegalThreadStateException("cannot shift the virtual time if useSystemTime is enabled");
        }
        setTime(getTime() + diff);
    }

    @Override
    public void run() {
        /*
        // Removed because must work with debug threads
        if (instance != Thread.currentThread()) {
            throw new IllegalThreadStateException("Cannot run the global timing thread manually");
        }
        */
        long last = System.currentTimeMillis();
        long now;
        setUseSystemTime(false);
        while (!isInterrupted()) {
            now = System.currentTimeMillis();
            shiftTime(now - last); // increment global time
            last = now;
            Global.set("TIME", time); // set virtual global time
            try {
                sleep(accuracy);
            } catch (InterruptedException e) {
                break;
            }
        }
        setUseSystemTime(true);
    }

    /**
     * sets the virtual time to the system time
     */
    public void reset() {
        setTime(System.currentTimeMillis());
    }

    public void addListener(TimeListener listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }
    }

    public void removeListener(TimeListener listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
    }
}
