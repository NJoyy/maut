package com.antares.cal.config;

/**
 * A Listener to be invoked whenever the system time changes.
 * WARNING: That will be fairly often
 *
 * @author Tim Trense
 */
public interface TimeListener {

    /**
     * invoked whenever the system time changes.
     *
     * @param now MILLISECONDS the current unix timestamp
     * @see Time#setTime(long) Time.setTime(long) : this function invokes all listeners of the Time-Singleton
     */
    void onTimeChanged(final long now);
}
