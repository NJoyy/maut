package com.antares.cal.utility;

import com.antares.cal.config.Global;

import java.io.*;
import java.net.URL;

/**
 * Loads resources from the deployed web archive
 *
 * @author Tim Trense
 */
public abstract class ResourceLoader {

    /**
     * opens an input stream for that file
     *
     * @param filename the filename within the archive (eg "WEB-INF/web.xml")
     * @return an input from that file, null if the file could not be found
     */
    public InputStream open(String filename) {
        try {
            final InputStream in;
            final URL f = locate(filename);
            if (f == null) {
                throw new FileNotFoundException(filename);
            }
            in = f.openStream();
            return in;
        } catch (final IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * locates the given file
     *
     * @param filename the filename within the archive (eg "WEB-INF/web.xml")
     * @return the file, null if the file could not be found
     */
    public abstract URL locate(String filename);

    /**
     * loads the specified file
     *
     * @param filename the name of that file
     * @return the content of that file encoded as string, line separated by "\n"
     * @throws IOException if any error occurs
     */
    public String loadString(final String filename) throws IOException {
        final InputStream in = open(filename);
        if (in == null) {
            throw new FileNotFoundException("File not found: " + filename);
        }
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = reader.readLine();
        StringBuilder builder = new StringBuilder();
        while (line != null) {
            builder.append(line).append("\n");
            line = reader.readLine();
        }
        in.close();
        return builder.toString();
    }

    /**
     * loads the specified file
     *
     * @param filename the name of that file
     * @return the content of that file
     * @throws IOException if any error occurs
     */
    public byte[] load(final String filename) throws IOException {
        final InputStream in = open(filename);
        if (in == null) {
            throw new FileNotFoundException("File not found: " + filename);
        }
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        byte[] tmp = new byte[100];
        int count = in.read(tmp);
        while (count == tmp.length) {
            buffer.write(tmp);
            count = in.read(tmp);
        }
        buffer.write(tmp, 0, count);
        in.close();
        return buffer.toByteArray();
    }
}
