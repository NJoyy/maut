package com.antares.cal.utility.convert;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("WeakerAccess")
public class DataFormatter {

	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/YYYY HH:mm");
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/YYYY");
	public static final SimpleDateFormat DATE_FORMAT_MONTH_AND_YEAR = new SimpleDateFormat("MM/YYYY");
	public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static String formatDateTime(final long timestamp) {
		return formatDateTime(new Date(timestamp));
	}

	public static String formatDateTime(final Date timestamp) {
		return DATE_TIME_FORMAT.format(timestamp);
	}

	public static String formatDate(final long timestamp) {
		return formatDate(new Date(timestamp));
	}

	public static String formatDate(final Date timestamp) {
		return DATE_FORMAT.format(timestamp);
	}

	public static String formatDateMonthAndYear(final long timestamp) {
		return formatDateMonthAndYear(new Date(timestamp));
	}

	public static String formatDateMonthAndYear(final Date timestamp) {
		return DATE_FORMAT_MONTH_AND_YEAR.format(timestamp);
	}

	public static String formatTime(final long timestamp) {
		return formatTime(new Date(timestamp));
	}

	public static String formatTime(final Date timestamp) {
		return TIME_FORMAT.format(timestamp);
	}

	public static long parseTime(final String time) {
		try {
			return TIME_FORMAT.parse(time).getTime();
		} catch (ParseException ignored) {
			return -1;
		}
	}

	public static long parseDate(final String time) {
		try {
			return DATE_FORMAT.parse(time).getTime();
		} catch (ParseException ignored) {
			return -1;
		}
	}

	public static long parseDateTime(final String time) {
		try {
			return DATE_TIME_FORMAT.parse(time).getTime();
		} catch (ParseException ignored) {
			return -1;
		}
	}

	public static String formatBigDecimalPrice(BigDecimal bd) {
		DecimalFormat df = new DecimalFormat("#,###.00");
		df.setRoundingMode(RoundingMode.HALF_UP);
		return df.format(bd.doubleValue());
	}
	
	public static String formatBoolean(boolean b) {
		return b? "Ja" : "Nein";
	}
}
