package com.antares.cal.utility.webFilter;

import com.antares.cal.model.user.AgencyType;
import com.antares.cal.model.user.AgencyUser;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * the default implementation of the user filter for agencies
 * @author Tim Trense
 */
/*package*/ abstract class AgencyUserFilter {

    /*package*/ void doFilter(ServletRequest request, ServletResponse response, FilterChain chain, AgencyType required) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        String errorURL = httpRequest.getContextPath() +  "/error.xhtml";

        if (session == null) {
            httpResponse.sendRedirect(errorURL);
            return;
        }

        Object loggedUser = session.getAttribute("loggedUser");

        if(!(loggedUser instanceof AgencyUser) || // first check will automatically check for null
                (((AgencyUser) loggedUser).getAgencyType() != required)) {
            httpResponse.sendRedirect(errorURL);
            return;
        }

        chain.doFilter(httpRequest, httpResponse);
    }
}
