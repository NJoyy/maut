package com.antares.cal.utility.webFilter;

import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.model.user.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Fabian Mittmann
 */
@WebFilter("/owner/*")
public class CheckOwnerLoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        String errorURL = httpRequest.getContextPath() + "/error.xhtml";

        if (session == null) {
            httpResponse.sendRedirect(errorURL);
            return;
        }

        Object loggedUser = session.getAttribute("loggedUser");

        if (!(loggedUser instanceof OwnerUser)) { // will automatically check for null
            httpResponse.sendRedirect(errorURL);
            return;
        }

        chain.doFilter(httpRequest, httpResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

}
