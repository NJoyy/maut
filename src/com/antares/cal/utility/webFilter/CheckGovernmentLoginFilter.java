package com.antares.cal.utility.webFilter;

import com.antares.cal.model.user.AgencyType;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author Tim Trense
 * @author Fabian Mittmann
 */
@WebFilter("/gov/*")
public class CheckGovernmentLoginFilter extends AgencyUserFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doFilter(request, response, chain, AgencyType.GOVERNMENT);
    }

    @Override
    public void init(FilterConfig filterConfig) { }
    @Override
    public void destroy() { }

}
