package com.antares.cal.utility.webFilter;

import com.antares.cal.model.user.AgencyType;
import com.antares.cal.model.user.AgencyUser;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Tim Trense
 * @author Fabian Mittmann
 */
@WebFilter("/police/*")
public class CheckPoliceLoginFilter extends AgencyUserFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doFilter(request, response, chain, AgencyType.POLICE);
    }

    @Override
    public void init(FilterConfig filterConfig) { }
    @Override
    public void destroy() { }

}
