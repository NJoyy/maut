package com.antares.cal.utility.hash;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * A hash that was computed using salt and iterative algorithms
 * 
 * @author Tim Trense
 *
 */
public class SecureHash extends Hash {

	private final @NotNull byte[] salt;
	private final @Positive int iterations;

	/**
	 * instantiate a hash that is generated using the algorithm multiple times
	 * 
	 * @param hash
	 *            the generated hash
	 * @param salt
	 *            the salt used
	 * @param builder
	 *            the generator of this hash
	 * @param iterations
	 *            how many times the algorithm was applied
	 */
	public SecureHash(final @NotNull byte[] hash, final @NotBlank SecureHashBuilder<?> builder, final @NotNull byte[] salt,
			final @Positive int iterations) {
		super(hash, builder);
		this.salt = salt;
		this.iterations = iterations;
	}

	/**
	 * 
	 * @return the used salt
	 */
	public @NotNull byte[] getSalt() {
		return salt;
	}

	/**
	 * 
	 * @return how many times the algorithm was applied
	 */
	public @Positive int getIterations() {
		return iterations;
	}

	@Override
	public @NotNull SecureHashBuilder<?> getBuilder() {
		return (@NotNull SecureHashBuilder<?>) super.getBuilder();
	}
}
