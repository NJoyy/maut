package com.antares.cal.utility.hash;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * The generator for hashes of the password-based-key-derivation-function-2
 * encoded key that was build upon the HMAC-on-SHA-256-algorithm<br>
 * the algorithms name is (without quotes) "PBKDF2WithHmacSHA256"
 * 
 * @author Tim Trense
 *
 */
public class PBKDF2WithHmacSHA256HashBuilder extends SecureHashBuilder<PBKDF2WithHmacSHA256Hash> {

	public PBKDF2WithHmacSHA256HashBuilder(final @NotNull byte[] salt, final @Positive int iterations) {
		super(salt, iterations);
	}

	@Override
	public PBKDF2WithHmacSHA256Hash hash(final @NotNull byte[] data, final int off, final int len) {
		final CharBuffer cb = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(data, off, len));
		//noinspection ConstantConditions
		if (cb != null)
			return hash(cb.array());
		else
			return null;
	}

	public PBKDF2WithHmacSHA256Hash hash(final @NotBlank String data) {
		return hash(data.toCharArray());
	}

	public PBKDF2WithHmacSHA256Hash hash(final @NotNull char[] data) {
		try {
			final byte[] salt = getSalt();
			final int iterations = getIterations();
			final PBEKeySpec spec = new PBEKeySpec(data, salt, iterations, 256);
			final SecretKeyFactory fact = SecretKeyFactory.getInstance(getAlgorithm());
			if (fact == null)
				return null;
			final SecretKey key = fact.generateSecret(spec);
			if (key == null)
				return null;
			return new PBKDF2WithHmacSHA256Hash(key.getEncoded(), this, salt, iterations);
		} catch (final InvalidKeySpecException | NoSuchAlgorithmException e) {
			return null;
		}
	}

	@Override
	public @NotBlank String getAlgorithm() {
		return "PBKDF2WithHmacSHA256";
	}
	
	public static PBKDF2WithHmacSHA256Hash buildHash(final byte[] data, final byte[] salt, final int iterations) {
		final PBKDF2WithHmacSHA256HashBuilder hb = new PBKDF2WithHmacSHA256HashBuilder(salt, iterations);
		return hb.hash(data);
	}
	
	public static PBKDF2WithHmacSHA256Hash buildHash(final byte[] data) {
		return buildHash(data, buildSalt(64), 500_000);
	}
}
