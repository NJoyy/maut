package com.antares.cal.utility.hash;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.security.SecureRandom;

/**
 * A builder for secure hashes
 *
 * @author Tim Trense
 * @see #buildSalt(int)
 */
public abstract class SecureHashBuilder<T extends SecureHash> extends HashBuilder<T> {

    private static final SecureRandom random;

    static {
        random = new SecureRandom();
        // initialize by pseudo random offset
        // this may not be entirely secure, but makes the randomization harder to break
        for (int i = 0; i < Math.random() * 31415926; i++) {
            random.nextInt();
        }
    }

    private @NotNull byte[] salt;
    private @Positive int iterations;

    public SecureHashBuilder(final @NotNull byte[] salt, final @Positive int iterations) {
        this.salt = salt;
        this.iterations = iterations;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(final @NotNull byte[] salt) {
        this.salt = salt;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(final @Positive int iterations) {
        this.iterations = iterations;
    }

    /**
     * computes a random salt of the given length
     *
     * @param length the number of bytes in the result salt
     * @return a salt
     */
    public static byte[] buildSalt(final @Positive int length) {
        final byte[] salt = new byte[length];
        random.nextBytes(salt);
        return salt;
    }

    @Override
    public abstract T hash(final @NotNull byte[] data, final int off, final int len);

}
