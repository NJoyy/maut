package com.antares.cal.utility.jpaConverter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

    private static final LocalDateAttributeConverter instance = new LocalDateAttributeConverter();

    @SuppressWarnings("WeakerAccess")
    public static LocalDateAttributeConverter getInstance() {
        return instance;
    }

    public static Date localDateToDate(LocalDate locDate) {
        return getInstance().convertToDatabaseColumn(locDate);
    }

    public static LocalDate dateToLocalDateTime(Date timestamp) {
        return getInstance().convertToEntityAttribute(timestamp);
    }

    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
        return (locDate == null ? null : Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date sqlDate) {
        return (sqlDate == null ? null : sqlDate.toLocalDate());
    }

}
