package com.antares.cal.utility.jpaConverter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

    private static final LocalDateTimeAttributeConverter instance = new LocalDateTimeAttributeConverter();

    @SuppressWarnings("WeakerAccess")
    public static LocalDateTimeAttributeConverter getInstance() {
        return instance;
    }

    public static Timestamp localDateTimeToTimestamp(LocalDateTime locDateTime) {
        return getInstance().convertToDatabaseColumn(locDateTime);
    }

    public static LocalDateTime timestampToLocalDateTime(Timestamp timestamp) {
        return getInstance().convertToEntityAttribute(timestamp);
    }

    @Override
    public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
        return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
        return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
    }

}
