package com.antares.cal.utility.pdfGen;

import com.antares.cal.config.Global;
import com.antares.cal.utility.ResourceLoader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.IOException;
import java.io.InputStream;

@SuppressWarnings("WeakerAccess")
public class PageManager {

    public static void drawPageNumbers(PDDocument pdfDoc) throws IOException {
        int numPages = pdfDoc.getNumberOfPages();
        for (int i = 0; i < numPages; i++) {
            PDPage currentPage = pdfDoc.getPage(i);
            PDPageContentStream currentPageContentStream = new PDPageContentStream(pdfDoc, currentPage,
                    PDPageContentStream.AppendMode.APPEND, true);

            currentPageContentStream.beginText();
            InputStream font = ((ResourceLoader) Global.get("RES_LOADER")).open("static/data/DejaVuSansMono.ttf");
            currentPageContentStream.setFont(PDType0Font.load(pdfDoc, font), 12);
            font.close();
            currentPageContentStream.newLineAtOffset(460, 28);
            String line = "Seite " + (i + 1) + " von " + numPages;
            currentPageContentStream.showText(line);
            currentPageContentStream.endText();
            currentPageContentStream.close();
        }
    }

    public static void drawFooterOnLastPage(PDDocument pdfDoc) throws IOException {
        // Draw footer on last Page
        PDPage lastPage = pdfDoc.getPage(pdfDoc.getNumberOfPages() - 1);

        PDPageContentStream lastPageContentStream = new PDPageContentStream(pdfDoc, lastPage,
                PDPageContentStream.AppendMode.APPEND, true);

        lastPageContentStream.beginText();

        // Setting the font to the Content stream
        InputStream font = ((ResourceLoader) Global.get("RES_LOADER")).open("static/data/DejaVuSansMono.ttf");
        // File font = new
        // File("/root/git/maut/WebContent/static/data/DejaVuSansMono.ttf");
        lastPageContentStream.setFont(PDType0Font.load(pdfDoc, font), 12);
        font.close();
        lastPageContentStream.setLeading(18f);
        lastPageContentStream.newLineAtOffset(60, 105);

        String line1 = "Bitte überweisen Sie den Rechungsbetrag auf unser Konto (siehe unten).";
        String line2 = "Geben Sie bitte die Rechnunsnummer als Verwendungszweck an.";
        String line3 = "Empfänger          Antares GmbH   IBAN   DE21283500005374341625";
        String line4 = "Verwendungszweck   1000000789     BIC    BRLADE21ANO";

        lastPageContentStream.showText(line1);
        lastPageContentStream.newLine();
        lastPageContentStream.showText(line2);
        lastPageContentStream.newLine();
        lastPageContentStream.newLine();
        InputStream font1 = ((ResourceLoader) Global.get("RES_LOADER")).open("static/data/DejaVuSansMono.ttf");

        lastPageContentStream.setFont(PDType0Font.load(pdfDoc, font1), 10);
        font1.close();
        lastPageContentStream.showText(line3);
        lastPageContentStream.newLine();
        lastPageContentStream.showText(line4);
        lastPageContentStream.endText();

        lastPageContentStream.close();
    }

    public static void drawLogoFooter(PDDocument pdfDoc) throws IOException {
        // Draw footer on last Page
        int numPages = pdfDoc.getNumberOfPages();
        for (int i = 0; i < numPages; i++) {
            PDPage currentPage = pdfDoc.getPage(i);
            PDPageContentStream currentPageContentStream = new PDPageContentStream(pdfDoc, currentPage,
                    PDPageContentStream.AppendMode.APPEND, true);

            byte[] buffer = ((ResourceLoader) Global.get("RES_LOADER")).load("static/data/img/CompanyLogo-Cut-Black.png");
            PDImageXObject pdImage = PDImageXObject.createFromByteArray(pdfDoc, buffer, "CompanyLogo-Cut-Black");
            pdImage.setWidth(100);
            pdImage.setHeight(25);
            currentPageContentStream.drawImage(pdImage, 460, 40);

            currentPageContentStream.close();
        }
    }
}
