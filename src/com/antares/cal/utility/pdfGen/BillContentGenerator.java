package com.antares.cal.utility.pdfGen;

import com.antares.cal.utility.pdfGen.data.PdfDataDrive;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoice;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoiceItem;
import com.antares.cal.utility.pdfGen.data.PdfDataLocalisation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class BillContentGenerator {
	
    public static String[][] getFinalizedContent(String[][] content, double sum) {
        int maxStringLenghtInColumn = 0;
        for (int i = 0; i < content[0].length; i++) {
            maxStringLenghtInColumn = 0;
            for (String[] col : content) {
            	if (col[i] != null) { //
            		if (col[i].length() > maxStringLenghtInColumn) {
            			maxStringLenghtInColumn = col[i].length();
            		}
            	}
            }
            for (String[] col : content) {
                switch (i) { //default aligns columns at the right
                    case 0:
                    case 1:
                    case 2:
                        break;
                    default:
                        //Assemble Spacer
                        String spacer = assembleSpacer(maxStringLenghtInColumn, col[i]);
                        col[i] = spacer + col[i];
                }
            }
        }
        content = addHeader(content);
        content = addFooter(content, sum, maxStringLenghtInColumn);
        return content;
    }


    private static String assembleSpacer(int maxStringLenghtInColumn, String text) {
        String spacer = "";
        for (int k = 0; k < (maxStringLenghtInColumn - text.length()); k++) {
            spacer += " ";
        }
        return spacer;
    }

    private static String[][] addHeader(String content[][]) {
        String[][] contentWithHeader = new String[content.length + 1][];
        String[] header = {"KFZ", "Datum", "Auf-/Abfahrt", "Distanz", "Basispreis", "Steuern", "Endpreis"};
        contentWithHeader[0] = header;
        for (int i = 0; i < content.length; i++) {
            contentWithHeader[i + 1] = content[i];
        }

        return contentWithHeader;
    }

    private static String[][] addFooter(String content[][], double sum, int maxStringLenghtInColumn) {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.FLOOR); 
        String[][] contentWithFooter = new String[content.length + 1][];
        String rpSpacer = assembleSpacer(maxStringLenghtInColumn, (df.format(sum) + " €"));
        String rechnungspreis = rpSpacer + (df.format(sum) + " €");
        String[] sumFooter = {"", "", "", "Rechnungsbetrag", "", "", rechnungspreis};
        for (int i = 0; i < content.length; i++) {
            contentWithFooter[i] = content[i];
        }
        contentWithFooter[contentWithFooter.length - 1] = sumFooter;
        return contentWithFooter;
    }

    public static String[][] generateContent(PdfDataInvoice r, double sumDistanz, double sumBasispreis,
                                             double sumSteuern, double sumEndpreis, int postenOffset) {
        List<PdfDataInvoiceItem> rps = r.getPdfDataInvoiceItem();
        String[][] content = new String[rps.size() + 1][];
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.FLOOR); 
        for (int i = 0; i < rps.size(); i++) {
            String nr = Integer.toString(i);
            String datum = "";
            String startpunktEndpunkt = "";
            int distanzD = 0;
            String distanz = "";
            BigDecimal basispreisD = null;
            String basispreis = "";
            BigDecimal steuernD = null;
            String steuern = "";
            BigDecimal endpreisD = null;
            String endpreis = "";
            if (rps.get(i) instanceof PdfDataDrive) {
                PdfDataDrive drive = (PdfDataDrive) rps.get(i);
                nr = drive.getLicence();
                datum = drive.getDateAsString();
                startpunktEndpunkt = drive.getApproach() + " " + drive.getExit();
                distanzD = drive.getDistance();
                distanz = df.format(distanzD / 1000d) + " km";
                basispreisD = drive.getPrice();
                basispreis = df.format(basispreisD) + " €";
                steuernD = basispreisD.multiply(r.getTaxRateDriveAsBigDecimal());
                steuern = df.format(steuernD) + " €";
                endpreisD = basispreisD.add(steuernD);
                endpreis = df.format(endpreisD) + " €";
            } else if (rps.get(i) instanceof PdfDataLocalisation) {
                PdfDataLocalisation loc = (PdfDataLocalisation) rps.get(i);
                nr = loc.getCount() + " Ortung(en) für " + loc.getLicence();
                datum = "";
                startpunktEndpunkt = "";
                distanzD = 0;
                distanz = "";
                basispreisD = loc.getPrice();
                basispreis = df.format(basispreisD) + " €";
                steuernD = basispreisD.multiply(r.getTaxRateLocalisationAsBigDecimal());
                steuern = df.format(steuernD) + " €";
                endpreisD = basispreisD.add(steuernD);
                endpreis = df.format(endpreisD) + " €";
            }
            String[] posten = {nr, datum, startpunktEndpunkt, distanz, basispreis, steuern, endpreis};
            content[i] = posten;
        }
        String[] sumFahrten = {"Σ", "", "", df.format(sumDistanz) + " km", df.format(sumBasispreis) + " €",
                df.format(sumSteuern) + " €", df.format(sumEndpreis) + " €"};
        content[content.length - 1] = sumFahrten;
        double rechnungspreis = sumEndpreis;
        return content;
    }
    //*Spaghetti ende*
}
