package com.antares.cal.utility.pdfGen.data;

import com.antares.cal.model.invoice.InvoiceItem;
import com.antares.cal.model.user.Gender;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The head data for a pdf invoice
 * @author Tim Trense
 * @author Georg Claude
 */
public class PdfDataInvoice {
	
    private String prename;
    private String sirname;
    private Gender gender;
    private String street;
    private String houseNumber;
    private String postcode;
    private String city;
    private String invoiceNumber;
    private Date invoiceDate;
    private double taxRateDrive;
    private double taxRateLocalisation;
    private ArrayList<PdfDataInvoiceItem> pdfDataInvoiceItem;

    public PdfDataInvoice(String prename, String sirname, Gender gender, String street, String houseNumber, String postcode, String city,
                          String invoiceNumber, Date invoiceDate, double taxRateDrive,
                          double taxRateLocalisation, ArrayList<PdfDataInvoiceItem> pdfDataInvoiceItem) {
        super();
        this.prename = prename;
        this.sirname = sirname;
        this.gender = gender;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postcode = postcode;
        this.city = city;
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.taxRateDrive = taxRateDrive;
        this.taxRateLocalisation = taxRateLocalisation;
        this.pdfDataInvoiceItem = pdfDataInvoiceItem;
    }

    public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getSirname() {
        return sirname;
    }

    public void setSirname(String sirname) {
        this.sirname = sirname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public String getRechnungsdatumAsStringddMMyyyy() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(invoiceDate);
    }

    public String getRechnungsdatumAsStringMonthyyyy() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMMMMMMMMM yyyy");
        return sdf.format(invoiceDate);
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public double getTaxRateDrive() {
        return taxRateDrive;
    }

    public BigDecimal getTaxRateDriveAsBigDecimal() {
        return new BigDecimal(taxRateDrive);
    }

    public void setTaxRateDrive(int taxRateDrive) {
        this.taxRateDrive = taxRateDrive;
    }

    public double getTaxRateLocalisation() {
        return taxRateLocalisation;
    }

    public BigDecimal getTaxRateLocalisationAsBigDecimal() {
        return new BigDecimal(taxRateLocalisation);
    }

    public void setTaxRateLocalisation(int taxRateLocalisation) {
        this.taxRateLocalisation = taxRateLocalisation;
    }

    public List<PdfDataInvoiceItem> getPdfDataInvoiceItem() {
        return pdfDataInvoiceItem;
    }

    public void setPdfDataInvoiceItem(ArrayList<PdfDataInvoiceItem> pdfDataInvoiceItem) {
        this.pdfDataInvoiceItem = pdfDataInvoiceItem;
    }

    public BigDecimal getBasePriceSum() {
        BigDecimal sum = BigDecimal.ZERO;
        for (PdfDataInvoiceItem item : getPdfDataInvoiceItem()) {
            sum = sum.add(item.getPrice());
        }
        return sum;
    }
    
    public BigDecimal getPriceSum() {
        BigDecimal sum = BigDecimal.ZERO;
        for (PdfDataInvoiceItem item : getPdfDataInvoiceItem()) {
        	if (item instanceof PdfDataDrive) {
        		sum = sum.add(item.getPrice().add(item.getPrice().multiply(getTaxRateDriveAsBigDecimal())));
        	} else if (item instanceof PdfDataLocalisation) {
        		sum = sum.add(item.getPrice().add(item.getPrice().multiply(getTaxRateLocalisationAsBigDecimal())));
        	}
        }
        return sum;
    }
    
    public BigDecimal getDriveTaxSum() {
    	BigDecimal sum = BigDecimal.ZERO;
    	for (PdfDataInvoiceItem item : getPdfDataInvoiceItem()) {
    		 if (item instanceof PdfDataDrive) {
    			 sum = sum.add(item.getPrice().multiply(BigDecimal.valueOf(this.taxRateDrive)));
             }
        }
    	return sum;
    }

    public BigDecimal getLocalistionTaxSum() {
    	BigDecimal sum = BigDecimal.ZERO;
    	for (PdfDataInvoiceItem item : getPdfDataInvoiceItem()) {
	    	if (item instanceof PdfDataLocalisation) {
	       		sum = sum.add(item.getPrice().multiply(BigDecimal.valueOf(this.taxRateLocalisation)));
	        }
    	}
    	return sum;
    }
    
    public BigDecimal getTaxSum() {
    	BigDecimal sum = getDriveTaxSum().add(getLocalistionTaxSum());
    	return sum;
    }
    
    /**
     * @return METERS
     */
    public int getDistanzSum() {
        int sum = 0;
        for (PdfDataInvoiceItem item : getPdfDataInvoiceItem()) {
            if (!(item instanceof PdfDataDrive)) {
                continue;
            }
            sum += ((PdfDataDrive) item).getDistance();
        }
        return sum;
    }
    
    
}
