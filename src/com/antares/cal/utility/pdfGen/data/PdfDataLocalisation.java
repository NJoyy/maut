package com.antares.cal.utility.pdfGen.data;

import java.math.BigDecimal;

/**
 * @author Tim Trense
 * @author Georg Claude
 */
public class PdfDataLocalisation implements PdfDataInvoiceItem {

	private long count;
	private String licence;
	private BigDecimal price;

	/**
	 *  @param count the number of localisations of that car taken place in this month
	 * @param licence the string serialized form of the licence number of the localised car
	 * @param price the price for all localisations
	 */
	public PdfDataLocalisation(long count, String licence, BigDecimal price) {
		this.count = count;
		this.licence = licence;
		this.price = price;
	}

	public long getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	@Override
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
