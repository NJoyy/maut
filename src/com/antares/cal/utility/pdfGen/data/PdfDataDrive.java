package com.antares.cal.utility.pdfGen.data;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tim Trense
 * @author Georg Claude
 */
public class PdfDataDrive implements PdfDataInvoiceItem {

    private String licence;
    private Date date;
    private String approach;
    private String exit;
    private int distance;
    private BigDecimal price;

    /**
     *  @param licence the string serialized form of the licence number
     * @param date the time that this drive was taken
     * @param approach the name of the first measure station of the drive
     * @param exit the name of the last measure station of the drive
     * @param distance METERS the distance of this drive in meters
     * @param price EURO the price of this drive
     */
    public PdfDataDrive(String licence, Date date, String approach, String exit, int distance, BigDecimal price) {
        this.licence = licence;
    	this.date = date;
        this.approach = approach;
        this.exit = exit;
        this.distance = distance;
        this.price = price;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public Date getDate() {
        return date;
    }

    public String getDateAsString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy");
        return sdf.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setApproach(String approach) {
        this.approach = approach;
    }

    public void setExit(String exit) {
        this.exit = exit;
    }

    /**
     *
     * @param distance METERS
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getApproach() {
        return approach;
    }

    public String getExit() {
        return exit;
    }

    public int getDistance() {
        return distance;
    }
}
