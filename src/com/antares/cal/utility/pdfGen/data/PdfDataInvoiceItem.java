package com.antares.cal.utility.pdfGen.data;

import java.math.BigDecimal;

/**
 * @author Tim Trense
 * @author Georg Claude
 */
public interface PdfDataInvoiceItem {

    /**
     * @return EURO the price of this item including taxes
     */
    BigDecimal getPrice();
}
