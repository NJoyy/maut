package com.antares.cal.utility.pdfGen;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;

import com.antares.cal.config.Global;
import com.antares.cal.utility.ResourceLoader;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoice;

public class BillGenerator {
    private String billPath = "";
    private String billFileName = null;

    public BillGenerator() {
    }

    public BillGenerator(String billPath) {
        this.billPath = billPath;
    }

    public BillGenerator(String billPath, String billFileName) {
        super();
        this.billPath = billPath;
        this.billFileName = billFileName;
    }

    public String getBillPath() {
        return billPath;
    }

    public void setBillPath(String billPath) {
        this.billPath = billPath;
    }

    public String getBillFileName() {
        return billFileName;
    }

    public void setBillFileName(String billFileName) {
        this.billFileName = billFileName;
    }

    public byte[] generate(PdfDataInvoice pdfDataInvoice) throws IOException {
        InputStream in = ((ResourceLoader) Global.get("RES_LOADER"))
                .open("static/data/InvoiceForm.pdf");

        PDDocument pdfDoc = PDDocument.load(in);
        in.close();
        PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();

        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.FLOOR);

        String salutaion;
        switch (pdfDataInvoice.getGender()) {
            case MALE:
                salutaion = "Herr";
                break;
            case FEMALE:
                salutaion = "Frau";
                break;
            case DIVERSE:
                salutaion = "Hallo";
                break;
            default:
                throw new RuntimeException("This should not happen! (Gender Unknown)");
        }

        acroForm.getField("salutation").setValue(salutaion);
        acroForm.getField("name").setValue(pdfDataInvoice.getPrename() + " " + pdfDataInvoice.getSirname());
        acroForm.getField("street").setValue(pdfDataInvoice.getStreet() + " " + pdfDataInvoice.getHouseNumber());
        acroForm.getField("city").setValue(pdfDataInvoice.getPostcode() + " " + pdfDataInvoice.getCity());
        acroForm.getField("billno").setValue(pdfDataInvoice.getInvoiceNumber());
        acroForm.getField("date").setValue(pdfDataInvoice.getRechnungsdatumAsStringddMMyyyy());

        BigDecimal PRICE_PER_KM = Global.get("PRICE_PER_KM");

        acroForm.getField("priceperkm").setValue(df.format(PRICE_PER_KM) + " €");
        // entfällt
        // acroForm.getField("priceperlocalization").setValue(df.format(pdfDataInvoice.getOrtungspreis())
        // + " € ");
        acroForm.getField("tolltax").setValue(df.format(pdfDataInvoice.getTaxRateDrive() * 100) + " %");
        acroForm.getField("localisationtax").setValue(df.format(pdfDataInvoice.getTaxRateLocalisation() * 100) + " %");
        acroForm.getField("tolltaxineur").setValue(df.format(pdfDataInvoice.getDriveTaxSum()) + " € ");
        acroForm.getField("localisationtaxineur").setValue(df.format(pdfDataInvoice.getLocalistionTaxSum()) + " € ");
        acroForm.getField("taxfreeprice").setValue(df.format(pdfDataInvoice.getBasePriceSum()) + " € ");

        // PDPage page = pdfDoc.getPage(0);

        // boolean carry = true;

        double invoicePrice = pdfDataInvoice.getPriceSum().doubleValue();
        double invoiceDistance = pdfDataInvoice.getDistanzSum() / 1000d;
        double invoiceBaseprice = pdfDataInvoice.getBasePriceSum().doubleValue();// .subtract(pdfDataInvoice.getTaxSum())).doubleValue();
        double invoiceTaxesSum = pdfDataInvoice.getTaxSum().doubleValue();
        // double rechnungsEndpreisFahrten;
        /*
         * ArrayList<PdfDataInvoice> rechnungen = pdfDataInvoice.split(); int i = 0; int
         * postenOffset = 0; for(PdfDataInvoice r: rechnungen) { PDPage my_page; if (i
         * == 0) { my_page = pdfDoc.getPage(0); } else { my_page = new
         * PDPage(PDRectangle.A4); pdfDoc.addPage(my_page); }
         */
        PDPage my_page = pdfDoc.getPage(0);
        PDPageContentStream contentStream = new PDPageContentStream(pdfDoc, my_page,
                PDPageContentStream.AppendMode.APPEND, true);

        // Offsetnummer für rechnungsposten auf neuer Seite ermitteln
        // postenOffset += (i == 0)? 0 : rechnungen.get(i -
        // 1).getPdfDataInvoiceItem().size();
        int postenOffset = 0;

        String[][] content = BillContentGenerator.generateContent(pdfDataInvoice, invoiceDistance, invoiceBaseprice,
                invoiceTaxesSum, invoicePrice, postenOffset);
        String fContent[][] = BillContentGenerator.getFinalizedContent(content, invoicePrice);

        /*
         * if (i == 0) { carry = !(rechnungen.size() == 1);
         * TableDrawer.drawTable(pdfDoc, my_page, contentStream, 398, 55, carry,
         * fContent); } else { carry = !(rechnungen.size() - 1 == i);
         * TableDrawer.drawTable(pdfDoc, my_page, contentStream, 800, 55, carry,
         * fContent); } contentStream.close(); i++; }
         */
        TableDrawer.drawTable(pdfDoc, my_page, contentStream, 398, 55, false, fContent);
        contentStream.close();

        PageManager.drawPageNumbers(pdfDoc);
        PageManager.drawLogoFooter(pdfDoc);
        PageManager.drawFooterOnLastPage(pdfDoc);

        String fName;
        String standardName = "Cal-PdfDataInvoice-" + pdfDataInvoice.getInvoiceNumber() + "-"
                + pdfDataInvoice.getRechnungsdatumAsStringMonthyyyy();
        if (billFileName == null) {
            fName = standardName;
        } else {
            fName = billFileName;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        pdfDoc.save(baos);
        //pdfDoc.save(billPath + fName + ".pdf");
        pdfDoc.close();
        return baos.toByteArray();
    }
}
