package com.antares.cal.utility.pdfGen;
import java.io.IOException;
import java.io.InputStream;

import com.antares.cal.config.Global;
import com.antares.cal.utility.ResourceLoader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

public class TableDrawer {
	/**
	 * @param page
	 * @param contentStream
	 * @param y the y-coordinate of the first row
	 * @param margin the padding on left and right of table
	 * @param content a 2d array containing the table data
	 * @throws IOException
	 */
	public static void drawTable(PDDocument pdfDoc, PDPage page, PDPageContentStream contentStream,
	                            float y, float margin, boolean carry,
	                            String[][] rawContent) throws IOException {
		String[][] content;
		if (carry) {
			content = new String[rawContent.length - 3][];
			for (int i = 0; i < content.length; i++) {
				content[i] = rawContent[i];
			}
		} else {
			content = rawContent;
		}
	    final int rows = content.length;
	    final int cols = content[0].length;
	    final float rowHeight = 20f;
	    final float tableWidth = page.getMediaBox().getWidth()-(2*margin)+25;
	    final float tableHeight = rowHeight * rows;
	    final float colWidth = tableWidth/(float)cols;
	    final float cellMargin=5f;
	 
	    
	    
	    //draw the rows
	    float nexty = y ;
	    for (int i = 0; i <= rows; i++) {
	    	if (carry) {
	    		if ((i == 1) || (i == rows)) {
			    	contentStream.moveTo(margin, nexty);
			        contentStream.lineTo(margin+tableWidth, nexty);
			        contentStream.stroke();
		        }
	    	} else {
		        //contentStream.drawLine(margin,nexty,margin+tableWidth,nexty);
		        if ((i == 1) || (i >= rows - 2 && i <= rows - 1)) {
			    	contentStream.moveTo(margin, nexty);
			        contentStream.lineTo(margin+tableWidth, nexty);
			        contentStream.stroke();
		        } else if (i == rows) {
		        	contentStream.moveTo(margin + 255, nexty);
			        contentStream.lineTo(margin+tableWidth, nexty);
			        contentStream.stroke();
			        contentStream.moveTo(margin + 255, nexty + 2);
			        contentStream.lineTo(margin+tableWidth, nexty + 2);
			        contentStream.stroke();
		        }
	    	}
	        nexty-= rowHeight;
	    }
	 
	    //draw the columns
	    float nextx = margin;
	    for (int i = 0; i <= cols; i++) {
	        //contentStream.drawLine(nextx,y,nextx,y-tableHeight);
	    	/*contentStream.moveTo(nextx, y);
	    	contentStream.lineTo(nextx, y-tableHeight);
	    	contentStream.stroke();*/
	    	nextx += colWidth;	
	    }
	 
	    //now add the text
	    InputStream f = ((ResourceLoader) Global.get("RES_LOADER")).open("static/data/DejaVuSansMono.ttf");
	    //File f = new File("/root/git/maut/WebContent/static/data/DejaVuSansMono.ttf");
	    contentStream.setFont(PDType0Font.load(pdfDoc, f),10);
	    f.close();
	 
	    float textx = margin+cellMargin;
	    float texty = y-15;
	    for(int i = 0; i < content.length; i++){
	        for(int j = 0 ; j < content[i].length; j++){
	            String text = content[i][j];
	            if (text == null) {
	            	text = "";
	            }
	            contentStream.beginText();
	            //contentStream.moveTextPositionByAmount(textx,texty);
	            
	            contentStream.newLineAtOffset(textx, texty);
	            //contentStream.drawString(text);
	            contentStream.showText(text);
	            contentStream.endText();
	            switch (j) { //Configuration of ColumnWidth
	            	case 0: textx += 84; break;
	            	case 1: textx += 60; break;
	            	case 2: textx += 110; break;
	            	case 3: textx += 70; break;
	            	case 4: textx += colWidth; break;
	            	case 5: textx += 50; break;
	            	case 6: textx += colWidth; break;
	            	default: textx += colWidth; break;
	            }
	        }
	        texty-=rowHeight;
	        textx = margin+cellMargin;
	    }
	}
	
	
}
