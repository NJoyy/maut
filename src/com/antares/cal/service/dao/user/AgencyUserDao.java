package com.antares.cal.service.dao.user;

import com.antares.cal.model.user.AgencyUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * this provides all database methods needed for object {@link AgencyUser}
 * @author Fabian Mittmann
 */
@Stateless
public class AgencyUserDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link AgencyUser}
     * @param id an identifier for the database query
     * @return an {@link AgencyUser} object from database found by the given id
     */
    public AgencyUser findById(Long id) {
        return em.find(AgencyUser.class, id);
    }

    /**
     * this loads a {@link Collection} of objects from class {@link AgencyUser}
     * @param lastName a last name as parameter for the database query
     * @return an {@link Collection} of objects from class {@link AgencyUser} from database found by the given last name
     */
    public Collection<AgencyUser> findByLastName(String lastName) {
        final String JPQL = "SELECT au FROM AgencyUser au WHERE au.lastName = :ln";
        try {
            TypedQuery<AgencyUser> query = em.createQuery(JPQL, AgencyUser.class);
            query.setParameter("ln", lastName);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No user found with last name " + lastName + ".");
        }
        return null;
    }

    /**
     * inserts an object of class {@link AgencyUser} into database
     * @param user to persist in database
     */
    public void insertAgencyUser(AgencyUser user) {
        em.persist(user);
    }

    /**
     * updates an object of class {@link AgencyUser} in database
     * @param user to update in database
     */
    public void updateAgencyUser(AgencyUser user) {
        em.merge(user);
    }

    /**
     * deletes an object of class {@link AgencyUser} from database
     * @param user to remove from database
     */
    public void deleteAgencyUser(AgencyUser user) {
        em.remove(em.merge(user));
    }

}
