package com.antares.cal.service.dao.user;

import com.antares.cal.model.user.OwnerUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * this provides all database methods needed for object {@link OwnerUser}
 * @author Fabian Mittmann
 */
@Stateless
public class OwnerUserDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link OwnerUser}
     * @param id an identifier for the database query
     * @return an OwnerUser object from database found by the given id
     */
    public OwnerUser findById(Long id) {
        return em.find(OwnerUser.class, id);
    }

    /**
     * load all {@link OwnerUser}s from database
     * @return a {@link Collection} of {@link OwnerUser}s
     */
    public Collection<OwnerUser> getAllOwnerUsers() {
        final String JPQL = "SELECT ou FROM OwnerUser ou";
        try {
            TypedQuery<OwnerUser> query = em.createQuery(JPQL, OwnerUser.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No owners found in database.");
        }
        return null;
    }

    /**
     * this loads a {@link Collection} of objects from class {@link OwnerUser}
     * @param lastName a last name as parameter for the database query
     * @return an {@link Collection} of objects from class {@link OwnerUser} from database found by the given last name
     */
    public Collection<OwnerUser> findByLastName(String lastName) {
        final String JPQL = "SELECT ou FROM OwnerUser ou " +
                            "WHERE ou.personalData.lastName = :ln";
        try {
            TypedQuery<OwnerUser> query = em.createQuery(JPQL, OwnerUser.class);
            query.setParameter("ln", lastName);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No user found with last name " + lastName + ".");
        }
        return null;
    }

    /**
     * inserts an object of class {@link OwnerUser} into database
     * @param user to persist in database
     */
    public void insertOwnerUser(OwnerUser user) {
        em.persist(user);
    }

    /**
     * updates an object of class {@link OwnerUser} in database
     * @param user to update in database
     */
    public void updateOwnerUser(OwnerUser user) {
        em.merge(user);
    }

    /**
     * deletes an object of class {@link OwnerUser} from database
     * @param user to remove from database
     */
    public void deleteOwnerUser(OwnerUser user) {
        em.remove(em.merge(user));
    }


    /**
     * loads a user by its first name and last name
     * @param ownerUser to find in database firstName and lastName
     * @return an {@link OwnerUser} to find in database
     */
    public OwnerUser findByLastNameAndFirstName(OwnerUser ownerUser) {
        final String JQPL = "Select ou FROM OwnerUser ou " +
                            "WHERE personalData.lastName = :ln AND personalData.firstName = :fn";
        try {
            TypedQuery<OwnerUser> query = em.createQuery(JQPL, OwnerUser.class);
            query.setParameter("ln", ownerUser.getPersonalData().getLastName());
            query.setParameter("fn", ownerUser.getPersonalData().getFirstName());
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No user found with name " + ownerUser.getPersonalData().getFirstName()
            + " " + ownerUser.getPersonalData().getLastName() + ".");
        }
        return null;
    }

}
