package com.antares.cal.service.dao.user;

import com.antares.cal.model.user.AgencyUser;
import com.antares.cal.model.user.OwnerUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * this provides all database methods needed for the login
 * @author Fabian Mittmann
 */
@Stateless
public class LoginDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads the owner user from the database if the login data is correct
     * @param loginName the name the user wants to use for login
     * @return an instance of the logged owner user if the login was successful, else null
     */
    public OwnerUser findOwnerUserByLogin(String loginName) {
        String JPQL = "SELECT ou FROM OwnerUser ou WHERE " +
                      "ou.loginName = :ln";
        try {
            TypedQuery<OwnerUser> query = em.createQuery(JPQL, OwnerUser.class);
            query.setParameter("ln", loginName);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("Owner login failed with username " + loginName + ".");
        }
        return null;
    }

    /**
     * this loads the agency user from the database if the login data is correct
     * @param loginName the name the user wants to use for login
     * @return an instance of the logged agency user if the login was successful, else null
     */
    public AgencyUser findAgencyUserByLogin(String loginName) {
        String JPQL = "SELECT au FROM AgencyUser au WHERE " +
                      "au.loginName = :ln";
        try {
            TypedQuery<AgencyUser> query = em.createQuery(JPQL, AgencyUser.class);
            query.setParameter("ln", loginName);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("Agency login failed with username " + loginName + ".");
        }
        return null;
    }

}
