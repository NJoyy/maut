package com.antares.cal.service.dao.statistic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.antares.cal.model.statistic.LongHaulDrives;

/**
 * 
 * @author arved
 *
 */
@Stateless
public class BuildStatisticDao {
	
	@PersistenceContext(name = "cal-maut")
	private EntityManager em;
	
	/**
	 * sums up the covert distance by non German cars
	 * @param startDate begin of time span
	 * @param endDate end of time span
	 * @return returns a list of objects LongHaulDriver for the "Kilometerfresser" statistic
	 */
	public Collection<LongHaulDrives> createLongHaulDriverStatisticForeign(long startDate, long endDate) {
		//noinspection JpaQlInspection
		final String JPQL = "SELECT l.origin, SUM(d.distance) FROM Car c " +
							"INNER JOIN c.drives d " +
							"INNER JOIN c.licenseNumber l " +
							"WHERE NOT l.origin = 'GERMANY' AND d.time BETWEEN :sd AND :ed " +
							"GROUP BY l.origin " +
							"ORDER BY SUM(d.distance) DESC";
		try {
			Query query = em.createQuery(JPQL);
			query.setParameter("sd", startDate);
			query.setParameter("ed", endDate);
			List<Object[]> list = query.setFirstResult(0).setMaxResults(10).getResultList();
			Collection<LongHaulDrives> longhauldriver = new ArrayList<LongHaulDrives>();
			for (Object[] obj : list) {
				LongHaulDrives lhd = new LongHaulDrives();
				lhd.setCountry(obj[0].toString());
				lhd.setDistance(Long.parseLong(obj[1].toString()));
				longhauldriver.add(lhd);
			}
			return longhauldriver;
		} catch (NoResultException e) {
			System.err.println("No drives could be found.");
		}
		return null;
	}
	
	public Collection<LongHaulDrives> createLongHaulDriverStatisticAll(long startDate, long endDate) {
		//noinspection JpaQlInspection
		final String JPQL = "SELECT l.origin, SUM(d.distance) FROM Car c " +
							"INNER JOIN c.drives d " +
							"INNER JOIN c.licenseNumber l " +
							"WHERE d.time BETWEEN :sd AND :ed " +
							"GROUP BY l.origin " +
							"ORDER BY SUM(d.distance) DESC";
		try {
			Query query = em.createQuery(JPQL);
			query.setParameter("sd", startDate);
			query.setParameter("ed", endDate);
			List<Object[]> list = query.setFirstResult(0).setMaxResults(10).getResultList();
			Collection<LongHaulDrives> longhauldriver = new ArrayList<LongHaulDrives>();
			for (Object[] obj : list) {
				LongHaulDrives lhd = new LongHaulDrives();
				lhd.setCountry(obj[0].toString());
				lhd.setDistance(Long.parseLong(obj[1].toString()));
				longhauldriver.add(lhd);
			}
			return longhauldriver;
		} catch (NoResultException e) {
			System.err.println("No drives could be found.");
		}
		return null;
	}
	
}
