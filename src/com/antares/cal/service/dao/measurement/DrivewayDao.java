package com.antares.cal.service.dao.measurement;

import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.model.measurement.MeasureStation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * this provides all database methods needed for object {@link Driveway}
 *
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Stateless
public class DrivewayDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link Driveway}
     *
     * @param id an identifier for the database query
     * @return an {@link Driveway} object from database found by the given id
     */
    public Driveway findById(Long id) {
        return em.find(Driveway.class, id);
    }

    /**
     * this loads a {@link Collection} of all objects from class {@link Driveway} from database
     *
     * @return a {@link Collection} of all objects from class {@link Driveway} from database
     */
    public Collection<Driveway> findAllDriveways() {
        final String JPQL = "SELECT d FROM Driveway d";
        try {
            TypedQuery<Driveway> query = em.createQuery(JPQL, Driveway.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No driveway entities found in database.");
        }
        return null;
    }


    /**
     * this inserts an object of class {@link Driveway} into database
     *
     * @param driveway to persist in database
     */
    public void insertDriveway(Driveway driveway) {
        em.persist(driveway);
    }

    /**
     * this deletes an object of class {@link Driveway} from database
     *
     * @param driveway to remove from database
     */
    public void removeDriveway(Driveway driveway) {
        em.remove(em.merge(driveway));
    }


    /**
     * finds a {@link Driveway} by its start measure station and end measure station
     * @param startPoint the start point {@link MeasureStation}
     * @param endPoint the end point {@link MeasureStation}
     * @return a {@link Driveway} object with the given start point and end point
     */
    public Driveway findByEdge(MeasureStation startPoint, MeasureStation endPoint) {
        final String JPQL = "SELECT d FROM Driveway d WHERE :f MEMBER OF d.progress AND :t MEMBER OF d.progress";
        try {
            TypedQuery<Driveway> query = em.createQuery(JPQL, Driveway.class);
            query.setParameter("f", startPoint);
            query.setParameter("t", endPoint);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No driveway found with startpoint " +
                    startPoint.getName() + " and endpoint " + endPoint.getName() + ".");
        }
        return null;
    }

    /**
     * finds all driveways with starting point = parameter
     *
     * @param measureStation the starting point of the driveways to find
     * @return all driveways from there, if any
     */
    public Collection<Driveway> findAllDrivewaysStartingAt(MeasureStation measureStation) {
        final String JPQL = "SELECT d FROM Driveway d WHERE :sp MEMBER OF d.progress";
        try {
            TypedQuery<Driveway> query = em.createQuery(JPQL, Driveway.class);
            query.setParameter("sp", measureStation);
            List<Driveway> driveways = query.getResultList();
            if (driveways == null || driveways.isEmpty()) {
                throw new NoResultException();
            }
            // filter out the driveways that are of opposite direction
            List<Driveway> result = new LinkedList<>();
            for (Driveway d : driveways) {
                if (!d.getStartPoint().equals(measureStation)) {
                    continue;
                }
                result.add(d);
            }
            if (result.isEmpty()) {
                throw new NoResultException();
            }
            return result;
        } catch (NoResultException e) {
            System.err.println("No driveway found with startpoint " + measureStation.getShortName());
        }
        return null;
    }

    /**
     * finds all driveways with ending point = parameter
     *
     * @param measureStation the ending point of the driveways to find
     * @return all driveways to there, if any
     */
    public Collection<Driveway> findAllDrivewaysEndingAt(MeasureStation measureStation) {
        final String JPQL = "SELECT d FROM Driveway d WHERE :ep MEMBER OF d.progress";
        try {
            TypedQuery<Driveway> query = em.createQuery(JPQL, Driveway.class);
            query.setParameter("ep", measureStation);
            List<Driveway> driveways = query.getResultList();
            if (driveways == null || driveways.isEmpty()) {
                throw new NoResultException();
            }
            // filter out the driveways that are of opposite direction
            List<Driveway> result = new LinkedList<>();
            for (Driveway d : driveways) {
                if (!d.getEndPoint().equals(measureStation)) {
                    continue;
                }
                result.add(d);
            }
            if (result.isEmpty()) {
                throw new NoResultException();
            }
            return result;
        } catch (NoResultException e) {
            System.err.println("No driveway found with endpoint " + measureStation.getShortName());
        }
        return null;
    }
}
