package com.antares.cal.service.dao.measurement;

import com.antares.cal.model.measurement.MeasureStation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * provides the methods needed for {@link MeasureStation}
 * @author Paul Konietzny
 * @author Fabian Mittmann
 */
@Stateless
public class MeasureStationDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * finds a MeasureStation by its id
     * @param id to find a MeasureStation in database by shortName
     * @return returns the MeasureStation searched by id
     */
    public MeasureStation findById(Long id) {
        return em.find(MeasureStation.class, id);
    }

    public MeasureStation findByShortName(String shortName) {
        final String JPQL = "SELECT ms FROM MeasureStation ms WHERE ms.shortName = :sn";
        try {
            TypedQuery<MeasureStation> query = em.createQuery(JPQL, MeasureStation.class);
            query.setParameter("sn", shortName);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No user found with short name " + shortName +".");
        }
        return null;
    }

    /**
     * to insert a new MeasureStation in database
     * @param measureStation to add a new MeasureStation in database
     */
    public void insertMeasurestation(MeasureStation measureStation){
        em.persist(measureStation);
    }

    /**
     * to update the data of an MeasureStation
     * @param measureStation to update in database
     */
    public void updateMeasureStation(MeasureStation measureStation){
        em.merge(measureStation);
    }


    /**
     * finds the MeasureStation by shortName and remove it
     * @param measureStation to remove from database
     */
    public void deleteMeasureStation(MeasureStation measureStation){
        em.remove(em.merge(measureStation));
    }


    /**
     * find all MeasureStations in database
     * @return all MeasureStations from database
     */
    public Collection<MeasureStation> findAllMeasureStations() {
        //noinspection JpaQlInspection
        String JPQL = "SELECT ms FROM MeasureStation ms";
        try {
            TypedQuery<MeasureStation> query = em.createQuery(JPQL, MeasureStation.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No MeasureStations found in database.");
            e.printStackTrace();
        }
        return null;
    }


    public MeasureStation findByInterfaceId(long interfaceId) {
        String JPQL = "SELECT ms FROM MeasureStation ms " +
                "WHERE ms.interfaceId= :id";
        try {
            TypedQuery<MeasureStation> query = em.createQuery(JPQL, MeasureStation.class);
            query.setParameter("id", interfaceId);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No measure station found with interfaceId " + interfaceId + ".");
        }
        return null;
    }
}
