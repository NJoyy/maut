package com.antares.cal.service.dao.measurement;

import com.antares.cal.model.measurement.MeasurementCollection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * provides the database methods needed for {@link MeasurementCollection}
 * @author Paul Konietzny
 * @author Fabian Mittmann
 */
@Stateless
public class MeasurementCollectionDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link MeasurementCollection}
     * @param id an identifier for the database query
     * @return an {@link MeasurementCollection} object from database found by the given id
     */
    public MeasurementCollection findById(Long id) {
        return em.find(MeasurementCollection.class, id);
    }

    /**
     * to insert collection data in database {@link MeasurementCollection}
     * @param collection to insert in database
     */
    public void insertMeasurementCollection (MeasurementCollection collection) {
        em.persist(collection);
    }

    /**
     * updates an object of class {@link MeasurementCollection} in database
     * @param collection to update in database
     */
    public void updateMeasurementCollection(MeasurementCollection collection) {
        em.merge(collection);
    }

    /**
     * this deletes an object of class {@link MeasurementCollection} from database
     * @param collection to remove from database
     */
    public void removeMeasurementCollection(MeasurementCollection collection) {
        em.remove(em.merge(collection));
    }

}
