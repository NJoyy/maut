package com.antares.cal.service.dao.alert;

import com.antares.cal.model.alert.Alert;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * this provides all database methods needed for object {@link Alert}
 * @author Fabian Mittmann
 */
@Stateless
public class AlertDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link Alert} from database
     * @param id an identifier for the database query
     * @return an Invoice object from database found by the given id
     */
    public Alert findById(Long id) {
        return em.find(Alert.class, id);
    }

    /**
     * loads all {@link Alert}s from database
     * @return a {@link Collection} of all {@link Alert}s found from database
     */
    public Collection<Alert> findAllAlerts() {
        final String JPQL = "SELECT a FROM Alert a";
        try {
            TypedQuery<Alert> query = em.createQuery(JPQL, Alert.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No alert found in database.");
        }
        return null;
    }

    /**
     * inserts a new {@link Alert} in database
     * @param alert an {@link Alert} to persist in database
     */
    public void insertAlert(Alert alert) {
        em.persist(alert);
    }

    /**
     * removes an {@link Alert} from database
     * @param alert an {@link Alert} which will be removed from database
     */
    public void deleteAlert(Alert alert) {
        em.remove(em.merge(alert));
    }

}
