package com.antares.cal.service.dao.car;

import com.antares.cal.model.car.Drive;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Fabian Mittmann
 * this provides all database methods needed for object {@link Drive}
 */
@Stateless
public class DriveDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link Drive}
     * @param id an identifier for the database query
     * @return an {@link Drive} object from database found by the given id
     */
    public Drive findById(Long id) {
        return em.find(Drive.class, id);
    }

    /**
     * inserts an object of class {@link Drive} into database
     * @param drive to persist in database
     */
    public void insertDrive(Drive drive) {
        em.persist(drive);
    }

}
