package com.antares.cal.service.dao.car;

import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.user.OwnerUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * this provides all database methods needed for object {@link Car}
 * @author Arved Niedner
 */
@Stateless
public class CarDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link Car}
     * @param id an identifier for the database query
     * @return an Car object from database found by the given id
     */
    public Car findById(Long id) {
        return em.find(Car.class, id);
    }
    
    /**
     * this loads an object of class {@link Car}
     * @param licenseNumber as an unique identifier for the car
     * @return a Car object from database found by the given id
     */
    public Car findByLicenseNumber(LicenseNumber licenseNumber) {
        final String JPQL = "SELECT c FROM Car c " +
                            "WHERE c.licenseNumber= :ln";
        try {
            TypedQuery<Car> query = em.createQuery(JPQL, Car.class);
            query.setParameter("ln", licenseNumber);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No car found with licence number " + licenseNumber.getOrigin() +
                               "-" + licenseNumber.getCode() + ".");
        }
        return null;
    }
    
    /**
     * this loads an collection of objects of class OwnerUser to find its cars
     * @param ownerUser to find a car by its owner
     * @return list of Car objects from database found by the given id
     */
    public Collection<Car> findByOwnerUser(OwnerUser ownerUser) {
        final String JPQL = "SELECT c FROM Car c " +
                            "WHERE c.ownerUser = :ou";
        try {
            TypedQuery<Car> query = em.createQuery(JPQL, Car.class);
            query.setParameter("ou", ownerUser);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No cars could be found.");
        }
        return null;
    }

    /**
     * inserts an object of class {@link Car} into database
     * @param car to persist in database
     */
    public void insertCar(Car car) {
        em.persist(car);
    }

    /**
     * updates an object of class {@link Car} in database
     * @param car to update in database
     */
    public void updateCar(Car car) {
        em.merge(car);
    }

    /**
     * deletes an object of class {@link Car} from database
     * @param car to remove from database
     */
    public void deleteCar(Car car) {
        em.remove(em.merge(car));
    }

    /**
     *
     * @return all cars in the database
     */
    public Collection<Car> findAllCars() {
        final String JPQL = "SELECT c FROM Car c";
        try {
            TypedQuery<Car> query = em.createQuery(JPQL, Car.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No cars could be found.");
        }
        return null;
    }
}