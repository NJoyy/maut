package com.antares.cal.service.dao.car;

import com.antares.cal.model.car.CarLocalization;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * this provides all database methods needed for object {@link CarLocalization}
 * @author Fabian Mittmann
 */
@Stateless
public class CarLocalizationDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link CarLocalization}
     * @param id an identifier for the database query
     * @return an {@link CarLocalization} object from database found by the given id
     */
    public CarLocalization findById(Long id) {
        return em.find(CarLocalization.class, id);
    }

    /**
     * inserts an object of class {@link CarLocalization} into database
     * @param carLocalization to persist in database
     */
    public void insertCarLocalization(CarLocalization carLocalization) {
        em.persist(carLocalization);
    }

}
