package com.antares.cal.service.dao.invoice;

import com.antares.cal.model.invoice.Invoice;
import com.antares.cal.model.user.OwnerUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * this provides all database methods needed for object {@link Invoice}
 * @author Arved Niedner
 * @author Fabian Mittmann
 */
@Stateless
public class InvoiceDao {
	
	@PersistenceContext(name = "cal-maut")
	private EntityManager em;
	
	/**
     * this loads an object of class {@link Invoice}
     * @param id an identifier for the database query
     * @return an Invoice object from database found by the given id
     */
	public Invoice findById(long id) {
		return em.find(Invoice.class, id);
	}

    /**
     * loads all {@link Invoice}s from database
     * @return a {@link Collection} of all {@link Invoice}s
     */
	public Collection<Invoice> findAllInvoices() {
	    final String JPQL = "SELECT i FROM Invoice i";
	    try {
	        TypedQuery<Invoice> query = em.createQuery(JPQL, Invoice.class);
	        return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No invoice found in database.");
        }
        return null;
    }

    /**
     * loads all {@link Invoice}s from database that are not paid yet
     * @return a {@link Collection} of all unpaid {@link Invoice}s
     */
	public Collection<Invoice> findAllUnpaidInvoices() {
	    final String JPQL = "SELECT i FROM Invoice i WHERE i.paid = FALSE";
	    try {
	        TypedQuery<Invoice> query = em.createQuery(JPQL, Invoice.class);
	        return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No unpaid invoice found.");
        }
        return null;
    }

    /**
     * to find invoice via invoiceNumber
     * @param invoiceNumber to get invoice by invoiceNumber
     * @return invoice
     */
	public Invoice findByInvoiceNumber(long invoiceNumber) {
        final String JQPL = "Select iv from Invoice iv WHERE iv.invoiceNumber = :in";
        try {
            TypedQuery<Invoice> query = em.createQuery(JQPL, Invoice.class);
            query.setParameter("in", invoiceNumber);
            return query.getSingleResult();
        } catch (NoResultException e) {
            System.err.println("No invoice with invoiceNumber " + invoiceNumber + " found in database.");
        }
        return null;
    }


	/**
     * this loads a collection of objects of class {@link Invoice}
     * @param ownerUser to get owner_Id of invoices for database query
     * @return a collection of invoices
     */
	public Collection<Invoice> findByOwnerUser(OwnerUser ownerUser) {
		final String JPQL = "SELECT iv FROM Invoice iv WHERE iv.ownerId = :ou";
		try {
            TypedQuery<Invoice> query = em.createQuery(JPQL, Invoice.class);
            query.setParameter("ou", ownerUser.getId());
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No cars could be found.");
        }
        return null;
	}
	
	/**
     * this loads a collection of objects of class {@link Invoice}
     * @param ownerUser to get owner_Id of invoices for database query
     * @param startDate as begin of time span
     * @param endDate as end of time span
     * @return a collection of invoices
     */
	public Collection<Invoice> findByDateAndOwnerUser(OwnerUser ownerUser, long startDate, long endDate) {
		final String JPQL = "SELECT iv FROM Invoice iv WHERE iv.ownerId = :ou AND (iv.invoiceDate BETWEEN :sd AND :ed)";
		try {
            TypedQuery<Invoice> query = em.createQuery(JPQL, Invoice.class);
            query.setParameter("ou", ownerUser.getId());
            query.setParameter("sd", startDate);
            query.setParameter("ed", endDate);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No cars could be found.");
        }
        return null;
	}
	
    /**
     * inserts an object of class {@link Invoice} into database
     * @param invoice to persist in database
     */
    public void insertInvoice(Invoice invoice) {
        em.persist(invoice);
    }

    /**
     * updates an object of class {@link Invoice} in database
     * @param invoice to update in database
     */
    public void updateInvoice(Invoice invoice) {
        em.merge(invoice);
    }

    /**
     * deletes an object of class {@link Invoice} from database
     * @param invoice to remove from database
     */
    public void deleteInvoice(Invoice invoice) {
        em.remove(em.merge(invoice));
    }

}
