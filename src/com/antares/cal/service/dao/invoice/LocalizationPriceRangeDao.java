package com.antares.cal.service.dao.invoice;

import com.antares.cal.model.invoice.LocalizationPriceRange;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * this provides all database methods needed for object {@link LocalizationPriceRange}
 *
 * @author Tim Trense
 */
@Stateless
public class LocalizationPriceRangeDao {

    @PersistenceContext(name = "cal-maut")
    private EntityManager em;

    /**
     * this loads an object of class {@link LocalizationPriceRange}
     *
     * @param id an identifier for the database query
     * @return a LocalizationPriceRange object from database found by the given id
     */
    public LocalizationPriceRange findById(long id) {
        return em.find(LocalizationPriceRange.class, id);
    }

    /**
     * @return all localisation prices
     */
    public Collection<LocalizationPriceRange> getAll() {
        final String JPQL = "SELECT lpr FROM LocalizationPriceRange lpr";
        try {
            TypedQuery<LocalizationPriceRange> query = em.createQuery(JPQL, LocalizationPriceRange.class);
            return query.getResultList();
        } catch (NoResultException e) {
            System.err.println("No localisation price range found in database.");
        }
        return null;
    }

    /**
     * @param count the number of localisations for which the price is to determine
     * @return the price for every localisation, because __count__ localisations are done
     */
    public BigDecimal getPriceForLocalisationCount(final long count) {
        final String JPQL = "SELECT lpr FROM LocalizationPriceRange lpr " +
                "WHERE lpr.from <= :count AND lpr.to >= :count";
        try {
            TypedQuery<LocalizationPriceRange> query = em.createQuery(JPQL, LocalizationPriceRange.class);
            query.setParameter("count", count);
            List<LocalizationPriceRange> result = query.getResultList();
            if (result.isEmpty()) {
                throw new NoResultException("No localisation price range found in database");
            } else if (result.size() > 1) {
                throw new RuntimeException("overlapping localisation price range at " + count + " in database: database inconsistent!!!");
            }
            return result.get(0).getPrice();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * inserts an object of class {@link LocalizationPriceRange} into database
     *
     * @param localizationPriceRange to persist in database
     */
    public void insertLocalisationPriceRange(LocalizationPriceRange localizationPriceRange) {
        em.persist(localizationPriceRange);
    }

    /**
     * updates an object of class {@link LocalizationPriceRange} in database
     *
     * @param localizationPriceRange to update in database
     */
    public void updateLocalisationPriceRange(LocalizationPriceRange localizationPriceRange) {
        em.merge(localizationPriceRange);
    }

    /**
     * deletes an object of class {@link LocalizationPriceRange} from database
     *
     * @param localizationPriceRange to remove from database
     */
    public void deleteLocalisationPriceRange(LocalizationPriceRange localizationPriceRange) {
        em.remove(em.merge(localizationPriceRange));
    }

}
