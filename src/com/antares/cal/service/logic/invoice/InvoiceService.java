package com.antares.cal.service.logic.invoice;

import com.antares.cal.config.Global;
import com.antares.cal.config.Time;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.CarLocalization;
import com.antares.cal.model.car.Drive;
import com.antares.cal.model.invoice.Invoice;
import com.antares.cal.model.invoice.InvoiceItem;
import com.antares.cal.model.user.Address;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.model.user.PersonalData;
import com.antares.cal.service.dao.invoice.InvoiceDao;
import com.antares.cal.service.dao.invoice.LocalizationPriceRangeDao;
import com.antares.cal.service.dao.user.OwnerUserDao;
import com.antares.cal.utility.pdfGen.BillGenerator;
import com.antares.cal.utility.pdfGen.data.PdfDataDrive;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoice;
import com.antares.cal.utility.pdfGen.data.PdfDataInvoiceItem;
import com.antares.cal.utility.pdfGen.data.PdfDataLocalisation;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("CdiInjectionPointsInspection")
@Stateless
public class InvoiceService {

    @Inject
    private InvoiceDao invoiceDao;

    @Inject
    private OwnerUserDao userDao;

    @Inject
    private LocalizationPriceRangeDao localizationPriceRangeDao;

    @SuppressWarnings("WeakerAccess")
    public void createInvoice(OwnerUser owner) {

        BigDecimal pricePerKm = Global.get("PRICE_PER_KM");
        BigDecimal taxRateDrive = Global.get("TAXRATE_DRIVE");
        BigDecimal taxRateLocalisation = Global.get("TAXRATE_LOCALISATION");

        Invoice invoice = new Invoice();
        invoice.setInvoiceDate(Time.now());

        // get drives and localisations of all cars
        ArrayList<Drive> drives = new ArrayList<>();
        ArrayList<CarLocalization> localisations = new ArrayList<>();
        for (Car car : owner.getCars()) {
            drives.addAll(car.getDrives());
            localisations.add(car.getCarLocalization());
        }
        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
        invoiceItems.addAll(drives);
        invoiceItems.addAll(localisations);

        invoice.setInvoiceItems(invoiceItems);
        invoice.setInvoiceNumber(invoice.getId());

        BigDecimal netAmount = BigDecimal.ZERO;
        BigDecimal taxAmount = BigDecimal.ZERO;
        for (InvoiceItem ii : invoiceItems) {
            if (ii instanceof Drive) {
                Drive d = (Drive) ii;
                BigDecimal price = pricePerKm.multiply(BigDecimal.valueOf(d.getDistance() / 1000d));
                d.setPrice(price);
                netAmount = netAmount.add(price);
                taxAmount = taxAmount.add(price.multiply(taxRateDrive));
            } else if (ii instanceof CarLocalization) {
                CarLocalization l = (CarLocalization) ii;
                BigDecimal price = localizationPriceRangeDao.getPriceForLocalisationCount(l.getCounter());
                l.setPrice(price);
                netAmount = netAmount.add(price);
                taxAmount = taxAmount.add(price.multiply(taxRateLocalisation));
            }
        }

        invoice.setNetAmount(netAmount.setScale(2, RoundingMode.FLOOR));
        invoice.setOwnerId(owner.getId());
        invoice.setTaxAmount(taxAmount.setScale(2, RoundingMode.FLOOR));
        invoiceDao.insertInvoice(invoice);

        // reset drives and localisations of the current month, because a new month begins now
        for (Car c : owner.getCars()) {
            c.setCarLocalization(null);
            c.setDrives(null);
        }
    }

    public void createPdfInvoice(Invoice invoice) throws IOException {
        BigDecimal taxRateDrive = Global.get("TAXRATE_DRIVE");
        BigDecimal taxRateLocalisation = Global.get("TAXRATE_LOCALISATION");

        ArrayList<PdfDataInvoiceItem> items = new ArrayList<>();
        ArrayList<InvoiceItem> iis = new ArrayList<>(invoice.getInvoiceItems());
        for (InvoiceItem invoiceItem : iis) {
            // invoice item is a Drive
            if (invoiceItem instanceof Drive) {
                Drive drv = (Drive) invoiceItem;
                String license = drv.getCar().getLicenseNumber().toString();
                //TODO check if getTime returns the Date of the Drive
                Date date = new Date(drv.getTime());
                String approach = drv.getApproachPoint();
                String exit = drv.getExitPoint();
                int distance = drv.getDistance();
                BigDecimal price = drv.getPrice();
                items.add(new PdfDataDrive(license, date, approach, exit, distance, price));
                // invoice item is a CarLocalisation
            } else if (invoiceItem instanceof CarLocalization) {
                CarLocalization loc = (CarLocalization) invoiceItem;
                long count = loc.getCounter();
                String license = loc.getCar().getLicenseNumber().toString();
                BigDecimal price = loc.getPrice();
                items.add(new PdfDataLocalisation(count, license, price));
            }
        }
        // get user data for bill
        OwnerUser user = userDao.findById(invoice.getOwnerId());
        PersonalData pd = user.getPersonalData();
        Address address = user.getAddress();
        PdfDataInvoice pdi = new PdfDataInvoice(pd.getFirstName(), pd.getLastName(), pd.getGender(),
                address.getStreet(), address.getHouseNumber(), address.getZipCode(), address.getCity(),
                invoice.getId().toString(), new Date(invoice.getInvoiceDate()), taxRateDrive.doubleValue(),
                taxRateLocalisation.doubleValue(), items);
        // generate bill
        BillGenerator bg = new BillGenerator();
        byte[] invoicePdfFile = bg.generate(pdi);
        invoice.setInvoiceFile(invoicePdfFile);
        invoiceDao.updateInvoice(invoice);
    }

}
