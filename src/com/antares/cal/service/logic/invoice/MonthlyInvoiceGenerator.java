package com.antares.cal.service.logic.invoice;

import com.antares.cal.config.Global;
import com.antares.cal.config.Time;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.dao.user.OwnerUserDao;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * a thread to generate all invoices once a month
 *
 * @author Tim Trense
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Singleton
public class MonthlyInvoiceGenerator extends Thread {

    @Inject
    private InvoiceService invoiceService;

    @Inject
    private OwnerUserDao ownerUserDao;

    private long interval;

    private MonthlyInvoiceGenerator() {
        setName("MonthlyInvoiceGenerator");
        setInterval(1000 * 10);
    }

    @PostConstruct
    public void init() {
        start();
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    @Override
    public void run() {
        long lastInvoiceGeneration = Time.now();
        long now;
        while (!isInterrupted()) {
            try {
                sleep(interval);
            } catch (InterruptedException e) {
                break;
            }

            now = Time.now();
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(now);

            if (now - lastInvoiceGeneration < TimeUnit.DAYS.toMillis(2)) {
                continue;
            }

            //noinspection MagicConstant
            if (c.get(Calendar.DAY_OF_MONTH) == 1) {
                c.add(Calendar.DATE, -1);
                generateInvoices();
                lastInvoiceGeneration = now;
            }
        }
    }

    /**
     * invokes the {@link InvoiceService} for every {@link OwnerUser} in the database
     */
    private void generateInvoices() {
        for (OwnerUser u : ownerUserDao.getAllOwnerUsers()) {
            invoiceService.createInvoice(u);
        }
    }
}
