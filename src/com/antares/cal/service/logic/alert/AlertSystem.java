package com.antares.cal.service.logic.alert;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertProperty;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.measurement.Driveway;

import java.util.*;

/**
 * Singleton to hold all currently open (unhandled) {@link Alert}s
 *
 * @author Tim Trense
 */
@SuppressWarnings("WeakerAccess")
public class AlertSystem {

    private static AlertSystem instance;

    private final List<AlertHandler> handlers;
    private final List<Alert> unhandled;

    private AlertSystem() {
        handlers = new ArrayList<>();
        unhandled = new LinkedList<>();
    }

    /**
     * creates a new alert, lists it and calls all {@link AlertHandler#handleFire(Alert)}.
     * fast-forwards {@link #fireAlert(AlertType, String, Map)}
     *
     * @param type     the type of the alert to fire
     * @param message  the message to display
     * @param infoFlat additional machine-readable information in pattern key1, value1, key2, value2, ...
     * @return the created alert
     */
    public Alert fireAlert(final AlertType type, final String message, final String... infoFlat) {
        if (infoFlat.length % 2 != 0) {
            throw new IllegalArgumentException("cannot parse flattened machine readable info, be aware of pattern [key1], [value1], [key2], [value2], ...");
        }
        final Map<String, String> info = new HashMap<>();
        for (int i = 0; i < infoFlat.length; i += 2) {
            info.put(infoFlat[i], infoFlat[i + 1]);
        }
        return fireAlert(type, message, info);
    }

    /**
     * creates a new alert, lists it and calls all {@link AlertHandler#handleFire(Alert)}
     *
     * @param type    the type of the alert to fire
     * @param message the message to display
     * @param infos   additional machine-readable information
     * @return the created alert
     */
    public Alert fireAlert(final AlertType type, final String message, final Map<String, String> infos) {
        final Alert alert = new Alert();
        alert.setTime(Time.now());
        alert.setType(type);
        alert.setMessage(message);

        final List<AlertProperty> properties = new LinkedList<>();
        if (infos != null) {
            for (String key : infos.keySet()) {
                final AlertProperty property = new AlertProperty();
                property.setAlert(alert);
                property.setKey(key);
                property.setValue(infos.get(key));
                properties.add(property);
            }
        }
        alert.setProperties(properties);
        alert.setProperty("alert-type", type.name());

        fireAlert(alert);
        return alert;
    }

    /**
     * unlists the given alert and calls all {@link AlertHandler#handleClose(Alert)}
     *
     * @param alert the alert to remove
     */
    public void close(final Alert alert) {
        synchronized (unhandled) {
            unhandled.remove(alert);
        }

        for (AlertHandler handler : handlers) {
            handler.handleClose(alert);
        }
    }

    /**
     * @return an unmodifiable view to all currently open (unhandled) {@link Alert}s
     */
    @SuppressWarnings("WeakerAccess")
    public List<Alert> getUnhandled() {
        final List<Alert> result;
        synchronized (unhandled) {
            result = Collections.unmodifiableList(unhandled);
        }
        return result;
    }

    /**
     * filters {@link #getUnhandled()} and only returns the {@link Alert}s which have any of the given {@link AlertType}s
     *
     * @param filter a set of types to filter the alerts by
     * @return an unmodifiable view to all currently open (unhandled) {@link Alert}s filtered by their respective type
     */
    public List<Alert> getUnhandled(final AlertType... filter) {
        final List<Alert> result = new LinkedList<>();
        synchronized (unhandled) {
            for (Alert a : unhandled) {
                for (AlertType t : filter) {
                    if (a.getType() == t) {
                        result.add(a);
                        break;
                    }
                }
            }
        }
        return Collections.unmodifiableList(result);
    }

    /**
     * @return a list of all alerts upon cars that are missing and not on a jammed driveway (because there all cars are missing)
     */
    public List<Alert> getUnhandledMissingCarsOnNonJammedDriveways() {
        return getUnhandledMissingCars(false);
    }

    /**
     * @return a list of all alerts upon cars that are missing and are on a jammed driveway (that means: list all cars affected by a traffic jam)
     */
    public List<Alert> getUnhandledMissingCarsOnJammedDriveways() {
        return getUnhandledMissingCars(true);
    }

    /**
     * @param searchForOnJammedDriveways whether to search for the cars that are missing AND NOT OR AND ARE on a jammed driveway
     * @return a list as described by the parameter
     */
    public List<Alert> getUnhandledMissingCars(final boolean searchForOnJammedDriveways) {
        final List<Alert> missing = getUnhandled(AlertType.CAR_MISSING);
        final List<Alert> jams = getUnhandled(AlertType.TRAFFIC_JAM);
        final List<Alert> result = new LinkedList<>();
        for (Alert m : missing) {
            boolean onJammedDriveway = false;
            for (Alert j : jams) {
                final Driveway driveway = null;
                // TODO: find driveway by Long.parseLong(j.getPropertyValue("driveway")) as ID
                if (driveway.getStartPoint().getId().toString().equals(m.getPropertyValue("last-location"))) {
                    onJammedDriveway = true;
                    break;
                }
            }
            if ((searchForOnJammedDriveways && onJammedDriveway)
                    || (!searchForOnJammedDriveways && !onJammedDriveway)) {
                result.add(m);
            }
        }
        return Collections.unmodifiableList(result);
    }

    /**
     * registers the handler to be invoked on fireAlert and markHandled
     *
     * @param handler the handler to invoke
     */
    public void addHandler(final AlertHandler handler) {
        if (handler == null) {
            throw new IllegalArgumentException("cannot register a null AlertHandler");
        }
        handlers.add(handler);
    }

    /**
     * unregisters the handler to be invoked on fireAlert and markHandled
     *
     * @param handler the handler to not longer invoke
     */
    public void removeHandler(final AlertHandler handler) {
        if (handler == null) {
            throw new IllegalArgumentException("cannot unregister a null AlertHandler");
        }
        handlers.remove(handler);
    }

    /**
     * Singleton-Access-Method
     *
     * @return the singleton instance
     */
    public static AlertSystem getInstance() {
        if (instance == null) {
            instance = new AlertSystem();
            // synchronized is unnecessary, because no one else can possibly have access to that new instance
            // unhandled alerts are loaded by PersistenceAlertHandler
        }
        return instance;
    }

    @Override
    protected void finalize() throws Throwable {
        // synchronized is unnecessary, because no one else can possibly have access to this instance while destruction
        // unhandled alerts are saved by PersistenceAlertHandler
        super.finalize();
    }

    /**
     * registers that alert in the internal list and calls all handlers upon it
     *
     * @param alert the alert to fire
     */
    /*package*/
    public void fireAlert(final Alert alert) {
        synchronized (unhandled) {
            unhandled.add(alert);
        }

        for (AlertHandler handler : handlers) {
            handler.handleFire(alert);
        }
    }
}
