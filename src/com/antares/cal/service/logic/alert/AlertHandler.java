package com.antares.cal.service.logic.alert;

import com.antares.cal.model.alert.Alert;

/**
 * This can perform actions whenever an {@link Alert} is fired and closed
 * @author Tim Trense
 */
public interface AlertHandler {

    /**
     * invoked for every fired {@link Alert}
     * @param alert the alert that was just fired
     */
    void handleFire(final Alert alert);

    /**
     * invoked for every closed {@link Alert}
     * @param alert the alert that was just closed
     */
    void handleClose(final Alert alert);

}
