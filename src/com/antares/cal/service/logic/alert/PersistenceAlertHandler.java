package com.antares.cal.service.logic.alert;

import com.antares.cal.model.alert.Alert;
import com.antares.cal.service.dao.alert.AlertDao;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Handler that persists all alerts in the database and restores them after reboot
 *
 * @author Tim Trense
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Singleton
@Startup
public class PersistenceAlertHandler implements AlertHandler {

    @Inject
    private AlertDao alertDao;

    @Override
    public void handleFire(Alert alert) {
        alertDao.insertAlert(alert);
    }

    @Override
    public void handleClose(Alert alert) {
        alertDao.deleteAlert(alert);
    }

    @PostConstruct
    public void init() {
        for (Alert a : alertDao.findAllAlerts()) {
            AlertSystem.getInstance().fireAlert(a);
        }
        // register this as handler AFTER restoring the alerts, to not be invoked while restoring
        AlertSystem.getInstance().addHandler(this);
    }

    @PreDestroy
    public void uninit() {
        // unregister this as handler BEFORE persisting the alerts, to not be invoked while persisting
        AlertSystem.getInstance().removeHandler(this);
        // manually persisting is unnecessary, because it is already done when any alert is fired
    }
}
