package com.antares.cal.service.logic.measurement;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.Drive;
import com.antares.cal.model.measurement.*;
import com.antares.cal.model.rest.RawMeasurement;
import com.antares.cal.service.dao.car.CarDao;
import com.antares.cal.service.dao.car.DriveDao;
import com.antares.cal.service.dao.measurement.DrivewayDao;
import com.antares.cal.service.dao.measurement.MeasureStationDao;
import com.antares.cal.service.dao.measurement.MeasurementCollectionDao;
import com.antares.cal.service.logic.alert.AlertSystem;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Map;

/**
 * The implementation of the global handler for incoming measurements
 *
 * @author Tim Trense
 */
@SuppressWarnings("CdiInjectionPointsInspection") // this will actually work
@Stateless
public class MeasurementHandlerImpl implements MeasurementHandler {

    @Inject
    private CarDao carDao;

    @Inject
    private MeasureStationDao measureStationDao;

    @Inject
    private DrivewayDao drivewayDao;

    @Inject
    private MeasurementCollectionDao collectionDao;

    @Inject
    private DriveDao driveDao;

    @Inject
    private MissingCarsTracker missingCarsTracker;

    @Inject
    private TrafficJamTracker trafficJamTracker;

    @Override
    protected void finalize() throws Throwable {
        missingCarsTracker.interrupt();
        trafficJamTracker.interrupt();
        missingCarsTracker.join();
        trafficJamTracker.join();
        super.finalize();
    }

    @Override
    public void handle(final RawMeasurement raw) {
        AlertSystem alertSystem = AlertSystem.getInstance();
        // find concerned car
        Car car = carDao.findByLicenseNumber(raw.getLicenseNumber());
        if (car == null) { // if car is not found
            alertSystem.fireAlert(AlertType.UNKNOWN_CAR, "There is no database record for the car with licence number " + raw.getLicenseNumber().toString(),
                    "license", raw.getLicenseNumber().serialize());
            return;
        }

        // find recording measure station
        MeasureStation measureStation = measureStationDao.findByInterfaceId(raw.getInterfaceId());
        if (measureStation == null) { // if measure station is not found
            alertSystem.fireAlert(AlertType.MALFUNCTION, "Unknown MeasureStation.interfaceId " + raw.getInterfaceId(),
                    "iid", raw.getInterfaceId().toString());
            return;
        }

        // construct edge between last measure station and current one
        // IF this is the first measurement, leave it null
        Measurement lastMeasurement = car.getCurrentMeasurements().getLast();
        Driveway driveway = null;
        if (lastMeasurement != null) {
            driveway = drivewayDao.findByEdge(lastMeasurement.getMeasureStation(), measureStation);
        }

        Measurement measurement = new Measurement();
        measurement.setTime(raw.getTime());
        measurement.setMeasurementCollection(car.getCurrentMeasurements());
        measurement.setMeasureStation(measureStation);
        missingCarsTracker.onMeasured(measurement);
        trafficJamTracker.onMeasured(measurement);

        boolean normal = true;

        if (car.isStolen()) {
            alertSystem.fireAlert(AlertType.STOLEN_CAR,
                    "Stolen Car detected. Licence number: " + car.getLicenseNumber().toString() + " location: " + measureStation.getName(),
                    "licence", car.getLicenseNumber().serialize(),
                    "at", measureStation.getId().toString(),
                    "at-iid", measureStation.getInterfaceId().toString(),
                    "at-name", measureStation.getName(),
                    "at-type", measureStation.getType().name());
            normal = false;
        }

        if (driveway != null && driveway.getStartPoint() == measureStation && !car.getCarType().isIgnoreWrongWay()) {
            // TODO: (optional) acquire driveway from current measure station to next one and give direction hint in message
            alertSystem.fireAlert(AlertType.WRONG_WAY_DRIVER,
                    "Wrong way driver detected. Licence number: " + car.getLicenseNumber().toString()
                            + " location " + measureStation.getName() + " on route " + driveway.getName(),
                    "licence", car.getLicenseNumber().serialize(),
                    "at", measureStation.getId().toString(),
                    "at-iid", measureStation.getInterfaceId().toString(),
                    "at-name", measureStation.getName(),
                    "at-type", measureStation.getType().name());
            normal = false;
        }

        if (measureStation.getType() == MeasureStationType.EXIT && lastMeasurement == null && !car.getCarType().isIgnoreWrongWay()) {
            // lastMeasurement == null indicates, that this is the first measurement of the current drive
            alertSystem.fireAlert(AlertType.WRONG_WAY_DRIVER,
                    "Driver entered an exit: Wrong way driver detected. Licence number: "
                            + car.getLicenseNumber().toString()
                            + " location " + measureStation.getName(),
                    "licence", car.getLicenseNumber().serialize(),
                    "at", measureStation.getId().toString(),
                    "at-iid", measureStation.getInterfaceId().toString(),
                    "at-name", measureStation.getName(),
                    "at-type", measureStation.getType().name());
            normal = false;
        }

        if (driveway != null && !car.getCarType().isIgnoreRescueLaneSpeeding() && measureStation.getType() != MeasureStationType.EXIT) {
            boolean speeding = false;
            Map<Driveway, Long> jams = trafficJamTracker.getJams();
            for (Driveway d : jams.keySet()) {
                if (driveway.getId().equals(d.getId())) {
                    speeding = true;
                    break;
                }
            }
            if (speeding) {
                alertSystem.fireAlert(AlertType.RESCUE_LANE_SPEEDER,
                        "Rescue Lane speeder detected at " + driveway.getName() +
                                " licence number: " + car.getLicenseNumber().toString(),
                        "at", measureStation.getId().toString(),
                        "at-name", measureStation.getName(),
                        "at-iid", measureStation.getInterfaceId().toString(),
                        "at-type", measureStation.getType().name(),
                        "licence", car.getLicenseNumber().serialize());
            }
        }

        if (normal) {
            if (measurement.getMeasureStation().getType() == MeasureStationType.EXIT) {
                System.out.println("DRIVE ENDS!");

                MeasurementCollection collection = car.getCurrentMeasurements();
                final Drive drive = new Drive();
                drive.setCar(car);
                drive.setTime(Time.now());
                drive.setApproachPoint(collection.getFirst().getMeasureStation().getShortName());
                drive.setExitPoint(collection.getFirst().getMeasureStation().getShortName());
                int meters = getDistance(collection);
                drive.setDistance(meters);
                car.setCurrentMeasurements(new MeasurementCollection());
                car.addDrive(drive);

                // database methods
                collectionDao.removeMeasurementCollection(collection);
                driveDao.insertDrive(drive);
            } else if (lastMeasurement == null) {// this is the first measurement of the current drive
                System.out.println("DRIVE BEGINS!");

                final MeasurementCollection collection = new MeasurementCollection();
                collection.addMeasurement(measurement);
                collection.setCar(car);
                measurement.setMeasurementCollection(collection);
                car.setCurrentMeasurements(collection);
                carDao.updateCar(car);
            } else {
                System.out.println("DRIVE GOES ON!");

                car.getCurrentMeasurements().addMeasurement(measurement);
                carDao.updateCar(car);
            }
        }
    }


    /**
     * @return METERS the distance from {@link MeasurementCollection#getFirst()} to {@link MeasurementCollection#getLast()} over all covered stations
     */
    private int getDistance(MeasurementCollection collection) {
        int meters = 0;
        Measurement last = null;
        for (Measurement m : collection.getMeasurements()) {
            if (last == null) {
                last = m;
                continue;
            }

            Driveway d = drivewayDao.findByEdge(last.getMeasureStation(), m.getMeasureStation());
            if (d == null) {
                throw new RuntimeException("cannot find the edge between " + last.getMeasureStation().getShortName() +
                        " and " + m.getMeasureStation().getShortName());
            }
            meters += d.getDistance();

            last = m;
        }
        return meters;
    }
}
