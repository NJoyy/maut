package com.antares.cal.service.logic.measurement;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.model.measurement.Measurement;
import com.antares.cal.service.dao.measurement.DrivewayDao;
import com.antares.cal.service.logic.alert.AlertSystem;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Thread to keep up with uncompleted drives and watch for jams cars
 *
 * @author Tim Trense
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Singleton
@Startup
public class TrafficJamTracker extends Thread {

    @Inject
    private DrivewayDao drivewayDao;

    private double tolerance;
    private long interval;
    private final Set<TrafficJamTrack> jams;
    private final Map<MeasureStation, Measurement> lastMeasurements;

    @SuppressWarnings("WeakerAccess")
    public TrafficJamTracker() {
        setName("TrafficJamTracker");
        tolerance = 0.9;
        interval = 1000; // 1 second
        jams = new HashSet<>();
        lastMeasurements = new HashMap<>();
    }

    @PostConstruct
    public void init() {
        System.out.println("Starting TrafficJamsTracker");
        start();
    }

    @PreDestroy
    public void uninit() {
        System.out.println("Stopping TrafficJamsTracker");
        interrupt();
        try {
            join();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopped TrafficJamsTracker");
    }

    /**
     * default 1000 = 1 second
     *
     * @return MILLISECONDS between every check for jams
     */
    public long getInterval() {
        return interval;
    }

    /**
     * this value may be decreased for high-load systems
     *
     * @param interval MILLISECONDS between every check for jams
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * default 90%
     *
     * @return PERCENT how much more time to tolerance than any car should take (on average) to traverse from the last measurement to the next
     */
    public double getTolerance() {
        return tolerance;
    }

    /**
     * default 90%
     *
     * @param tolerance PERCENT how much more time to tolerance than any car should take (on average) to traverse from the last measurement to the next
     */
    public void setTolerance(final double tolerance) {
        this.tolerance = tolerance;
    }

    /**
     * @return a map from any currently jammed driveway to the duration since the incident began (in milliseconds)
     */
    @SuppressWarnings("WeakerAccess")
    public Map<Driveway, Long> getJams() {
        final Map<Driveway, Long> result = new HashMap<>();
        synchronized (jams) {
            for (TrafficJamTrack track : jams) {
                result.put(track.getDriveway(), track.getDuration());
            }
        }
        return result;
    }

    @Override
    public void run() {
        System.out.println("Started TrafficJamsTracker");
        long time;
        while (!isInterrupted()) {
            //noinspection UnusedAssignment
            time = Time.now();

            for (Driveway d : drivewayDao.findAllDriveways()) {
                if (isJammed(d)) {
                    continue;
                }
                Measurement lastAtEnd = lastMeasurements.get(d.getEndPoint());
                Measurement lastAtStart = lastMeasurements.get(d.getStartPoint());

                if (lastAtStart == null || lastAtEnd == null) {
                    continue;
                }

                if (lastAtStart.getCar().equals(lastAtEnd.getCar())) {
                    continue;
                }

                if (time - lastAtEnd.getTime() > d.getAverageTimeToCover() * tolerance) {
                    onTrafficJam(d);
                }
            }

            try {
                sleep(interval); // this reduces CPU-Load
            } catch (final InterruptedException ignored) {
                break; // thread is indicated to stop
            }
        }
    }

    /**
     * @param d the driveway to check
     * @return whether it is marked as jammed
     */
    private boolean isJammed(final Driveway d) {
        for (TrafficJamTrack t : jams) {
            if (t.getDriveway().equals(d)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        if (isAlive()) {
            interrupt();
            join();
        }
        super.finalize();
    }

    // called by onMeasured
    private void onTrafficJamEnded(final TrafficJamTrack track) {
        synchronized (jams) {
            jams.remove(track);
        }
        AlertSystem.getInstance().close(track.getAlert());
    }

    // called by the thread itself
    private TrafficJamTrack onTrafficJam(final Driveway driveway) {
        final Alert alert = AlertSystem.getInstance().fireAlert(AlertType.CAR_MISSING,
                "Traffic jam detected on " + driveway.getName(),
                "driveway", driveway.getId().toString(),
                "driveway-name", driveway.getName());
        TrafficJamTrack track = new TrafficJamTrack(driveway, alert);
        synchronized (jams) {
            jams.add(track);
        }
        return track;
    }

    /**
     * may be called by any {@link MeasurementHandler}
     *
     * @param measurement the measurement that was taken of any car
     */
    @SuppressWarnings("WeakerAccess")
    public void onMeasured(final Measurement measurement) {
        TrafficJamTrack track = null;
        for (TrafficJamTrack t : jams) {
            if (t.getDriveway().getEndPoint() == measurement.getMeasureStation()) {
                track = t;
            }
        }
        lastMeasurements.put(measurement.getMeasureStation(), measurement);
        if (track != null) {
            onTrafficJamEnded(track);
        }
    }
}
