package com.antares.cal.service.logic.measurement;

import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.model.rest.RawMeasurement;

/**
 * This handles any incoming {@link RawMeasurement} from a {@link MeasureStation}
 * @author Tim Trense
 * @since 0.1
 */
public interface MeasurementHandler {

    /**
     * will be invoked as soon as the parameter measurement comes into this system
     * @param measurement an incoming measurement
     */
    void handle(final RawMeasurement measurement);

}
