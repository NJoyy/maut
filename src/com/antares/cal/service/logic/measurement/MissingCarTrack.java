package com.antares.cal.service.logic.measurement;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.measurement.Measurement;

/**
 * This is an in-memory record for the {@link MissingCarsTracker} to keep up with a missing car
 *
 * @author Tim Trense
 */
// no database entity
public class MissingCarTrack {

    private final Car car;
    private final Measurement lastMeasurement;
    private final Alert alert;

    @SuppressWarnings("WeakerAccess")
    public MissingCarTrack(Car car, Measurement lastMeasurement, Alert alert) {
        this.car = car;
        this.lastMeasurement = lastMeasurement;
        this.alert = alert;

        if (car == null) {
            throw new IllegalArgumentException("cannot create a tracker for a null missing car");
        }
        if (lastMeasurement == null) {
            throw new IllegalArgumentException("cannot create a tracker for a missing car that has no currently open drive");
        }
        if (alert == null) {
            throw new IllegalArgumentException("any missing car track must be upon a car that was alerted about");
        }
    }

    /**
     *
     * @return the actual missing car
     */
    public Car getCar() {
        return car;
    }

    /**
     *
     * @return the last record we have from that car
     */
    public Measurement getLastMeasurement() {
        return lastMeasurement;
    }

    /**
     *
     * @return the open alert upon that incident
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     *
     * @return MILLISECONDS the duration from the last measurement until now
     */
    @SuppressWarnings("WeakerAccess")
    public long getMissingTime() {
        return Time.now() - lastMeasurement.getTime();
    }
}
