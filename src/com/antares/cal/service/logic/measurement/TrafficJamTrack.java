package com.antares.cal.service.logic.measurement;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.service.logic.measurement.MissingCarsTracker;

/**
 * This is an in-memory record for the {@link MissingCarsTracker} to keep up with a missing car
 *
 * @author Tim Trense
 */
// no database entity
public class TrafficJamTrack {

    private final Driveway driveway;
    private final Alert alert;

    public TrafficJamTrack(Driveway driveway, Alert alert) {
        this.driveway = driveway;
        this.alert = alert;

        if (driveway == null) {
            throw new IllegalArgumentException("cannot create a tracker for a null drive way");
        }
        if (alert == null) {
            throw new IllegalArgumentException("any traffic jam track must be upon a driveway that was alerted about");
        }
    }

    /**
     *
     * @return the actual jammed driveway
     */
    public Driveway getDriveway() {
        return driveway;
    }

    /**
     *
     * @return the open alert upon that incident
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     *
     * @return MILLISECONDS the duration from the beginning of the alert
     */
    public long getDuration() {
        return Time.now() - alert.getTime();
    }
}
