package com.antares.cal.service.logic.measurement;

import com.antares.cal.config.Time;
import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.model.measurement.Measurement;
import com.antares.cal.service.dao.car.CarDao;
import com.antares.cal.service.dao.measurement.DrivewayDao;
import com.antares.cal.service.logic.alert.AlertSystem;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.*;

/**
 * Thread to keep up with uncompleted drives and watch for missing cars
 *
 * @author Tim Trense
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Singleton
@Startup
public class MissingCarsTracker extends Thread {

    @Inject
    private CarDao carDao;

    @Inject
    private DrivewayDao drivewayDao;

    private double tolerance;
    private long interval;
    private final Set<MissingCarTrack> missing;

    @SuppressWarnings("WeakerAccess")
    public MissingCarsTracker() {
        setName("MissingCarsTracker");
        tolerance = 0.9;
        interval = 1000; // 1 second
        missing = new HashSet<>();
    }

    @PostConstruct
    public void init() {
        System.out.println("Starting MissingCarsTracker");
        start();
    }

    @PreDestroy
    public void uninit() {
        System.out.println("Stopping MissingCarsTracker");
        interrupt();
        try {
            join();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopped MissingCarsTracker");
    }

    /**
     * default 1000 = 1 second
     *
     * @return MILLISECONDS between every check for missing cars
     */
    public long getInterval() {
        return interval;
    }

    /**
     * this value may be decreased for high-load systems
     *
     * @param interval MILLISECONDS between every check for missing cars
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * default 90%
     *
     * @return PERCENT how much more time to tolerance than any car should take (on average) to traverse from the last measurement to the next
     */
    @SuppressWarnings("WeakerAccess")
    public double getTolerance() {
        return tolerance;
    }

    /**
     * default 90%
     *
     * @param tolerance PERCENT how much more time to tolerance than any car should take (on average) to traverse from the last measurement to the next
     */
    public void setTolerance(final double tolerance) {
        this.tolerance = tolerance;
    }

    /**
     * @return a map from any currently missing car to the duration since the last measurement of it (in milliseconds)
     */
    public Map<Car, Long> getMissing() {
        final Map<Car, Long> result = new HashMap<>();
        synchronized (missing) {
            for (MissingCarTrack track : missing) {
                result.put(track.getCar(), track.getMissingTime());
            }
        }
        return result;
    }

    @Override
    public void run() {
        System.out.println("Started MissingCarsTracker");
        long time;
        while (!isInterrupted()) {
            //noinspection UnusedAssignment
            time = Time.now();

            for (Car c : carDao.findAllCars()) {
                // filter for relevant cars to check: not-ignore-missing && with-open-drive && not-already-missing
                if (c.getCarType().isIgnoreMissing()) {
                    continue;
                }
                if (!c.getCurrentMeasurements().isOpen()) {
                    continue;
                }
                if (isMissing(c)) {
                    continue;
                }
                long timeSinceLastMeasurement = time - c.getCurrentMeasurements().getLast().getTime();
                Collection<Driveway> driveways = drivewayDao.findAllDrivewaysStartingAt(c.getCurrentMeasurements().getLast().getMeasureStation());
                if (driveways == null || driveways.isEmpty()) {
                    continue;
                }

                // Strategy to detect if car missing:
                // car is missing if and only if it exceeded the detection timeout for
                // all possible routes starting at the last measure station it passed

                boolean missingOnAll = true;
                for (Driveway d : driveways) {
                    long timeoutToMissing = (long) (d.getDistance() / (d.getAverageTimeToCover() * (1d + getTolerance()))) * 1000L;
                    if (timeSinceLastMeasurement < timeoutToMissing) {
                        missingOnAll = false;
                        break;
                    }
                }
                if (missingOnAll) {
                    onCarMissing(c);
                }
            }

            try {
                sleep(interval); // this reduces CPU-Load
            } catch (final InterruptedException ignored) {
                break; // thread is indicated to stop
            }
        }
    }

    /**
     * @param c the car to check
     * @return whether that car is currently missing
     */
    @SuppressWarnings("WeakerAccess")
    public boolean isMissing(final Car c) {
        for (MissingCarTrack t : missing) {
            if (t.getCar().equals(c)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        if (isAlive()) {
            interrupt();
            join();
        }
        super.finalize();
    }

    // called by onMeasured
    private void onCarReFound(final MissingCarTrack track) {
        synchronized (missing) {
            missing.remove(track);
        }
        AlertSystem.getInstance().close(track.getAlert());
    }

    // called by the thread itself
    private MissingCarTrack onCarMissing(final Car car) {
        Measurement lastMeasurement = car.getCurrentMeasurements().getLast();
        final Alert alert = AlertSystem.getInstance().fireAlert(AlertType.CAR_MISSING,
                "Car is missing. Licence number: " + car.getLicenseNumber().toString() +
                        " last location: " + lastMeasurement.getMeasureStation().getName(),
                "licence", car.getLicenseNumber().serialize(),
                "last-location", lastMeasurement.getMeasureStation().getId().toString(),
                "last-location-name", lastMeasurement.getMeasureStation().getName());
        MissingCarTrack track = new MissingCarTrack(car, lastMeasurement, alert);
        synchronized (missing) {
            missing.add(track);
        }
        return track;
    }

    /**
     * may be called by any {@link MeasurementHandler}
     *
     * @param measurement the measurement that was taken of any car
     */
    @SuppressWarnings("WeakerAccess")
    public void onMeasured(final Measurement measurement) {
        MissingCarTrack missingTrack = null;
        for (MissingCarTrack track : missing) {
            if (track.getCar() == measurement.getCar()) {
                missingTrack = track;
            }
        }
        if (missingTrack != null) {
            onCarReFound(missingTrack);
        }


    }
}
