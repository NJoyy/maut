package com.antares.cal.model.user;

/**
 * @author Fabian Mittmann
 */
public enum Gender {
    DIVERSE, FEMALE, MALE
}
