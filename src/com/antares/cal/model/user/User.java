package com.antares.cal.model.user;

import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.basic.PasswordAuthorized;

import javax.persistence.*;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class User extends PasswordAuthorized {

    @Column(name = "login_name", unique = true,
            nullable = false, length = 30)
    private String loginName;

    @Column(name = "email", unique = true,
            nullable = false, length = 70)
    private String email;

    @SuppressWarnings("WeakerAccess")
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * filter for alerts based on the access rights of the user
     *
     * @param alert the alert to check
     * @return whether to user is authorized to see it
     */
    public abstract boolean canSeeAlert(final Alert alert);
}
