package com.antares.cal.model.user;

/**
 * @author Fabian Mittmann
 */
public enum AgencyType {
    ADMIN, GOVERNMENT, POLICE
}
