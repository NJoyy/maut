package com.antares.cal.model.user;

import com.antares.cal.model.alert.Alert;

import javax.persistence.*;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Entity
@Table(name = "agency_users")
public class AgencyUser extends User {

    @Column(name = "agency_type", nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private AgencyType agencyType;

    public AgencyType getAgencyType() {
        return agencyType;
    }

    public void setAgencyType(AgencyType agencyType) {
        this.agencyType = agencyType;
    }

    @Override
    public boolean canSeeAlert(Alert alert) {

        // Administrator can see each and every alert
        if (agencyType == AgencyType.ADMIN) {
            return true;
        }

        if (agencyType == AgencyType.GOVERNMENT) {
            switch (alert.getType()) {
                case INFO:
                case TRAFFIC_JAM:
                    return true;

                // FOR DEBUG PURPOSE
                // TODO: remove before flight
                case MEASURESTATION_OFFLINE:
                case MEASURESTATION_OFFLINE_TEST:
                case MEASURESTATION_OFFLINE_TEST2:
                    return true;
            }
        }

        if (agencyType == AgencyType.POLICE) {
            switch (alert.getType()) {
                case CAR_MISSING:
                case WRONG_WAY_DRIVER:
                case STOLEN_CAR:
                case RESCUE_LANE_SPEEDER:
                    return true;
            }
        }

        return false;
    }
}
