package com.antares.cal.model.user;

import java.math.BigDecimal;

/**
 * @author Fabian Mittmann
 */
public enum BillingPreference {
    EMAIL, LETTER, ONLINE;

    /**
     * tries to find the correct instance to that name
     *
     * @param name the {@link #name()}-value of a BillingPreference
     * @return the according instance, or null if not found
     */
    public static BillingPreference getByName(final String name) {
        for (BillingPreference p : BillingPreference.values()) {
            if (p.name().equals(name)) {
                return p;
            }
        }
        return null;
    }
}
