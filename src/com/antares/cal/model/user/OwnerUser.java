package com.antares.cal.model.user;

import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertType;
import com.antares.cal.model.car.Car;
import com.antares.cal.model.invoice.Invoice;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Entity
@Table(name = "owner_users")
public class OwnerUser extends User {

    // No-Argument Constructor for REST
    public OwnerUser() {
    }

    @Column(name = "billing_preference", nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private BillingPreference billingPreference;

    @Embedded
    private Address address;

    @Embedded
    private PersonalData personalData;

    @OneToMany(mappedBy = "ownerUser", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Car> cars = new ArrayList<>();

    public BillingPreference getBillingPreference() {
        return billingPreference;
    }

    public void setBillingPreference(BillingPreference billingPreference) {
        this.billingPreference = billingPreference;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(final Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Cannot add a null Car to an OwnerUser");
        }
        getCars().add(car);
    }

    @Override
    public boolean canSeeAlert(Alert alert) {
        switch (alert.getType()) {
            // For following types, check in detail
            case CAR_MISSING: // fall-through
            case INFO:
                break;

            // for any other type: deny access
            default:
                return false;
        }

        // user can see MissingCar-Alerts for his own cars
        if (alert.getType() == AlertType.CAR_MISSING) {
            for (Car c : getCars()) {
                if (c.getLicenseNumber().serialize().equals(alert.getPropertyValue("licence")))
                    return true;
            }
            return false;
        }
        // user can see Info-Alerts that link to his account
        if (alert.getType().equals(AlertType.INFO)) {
            //noinspection RedundantIfStatement
            if (getId().toString().equals(alert.getPropertyValue("user"))) {
                return true;
            }
        }
        return false;
    }
}
