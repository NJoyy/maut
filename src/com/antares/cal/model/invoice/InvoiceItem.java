package com.antares.cal.model.invoice;

import com.antares.cal.model.basic.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "invoice_items")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class InvoiceItem extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }
    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public abstract String getDescription();
    public abstract BigDecimal getPrice();

}
