package com.antares.cal.model.invoice;

import com.antares.cal.model.basic.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * a range from lower localisation count (included) to upper localisation count (included too) for which
 * every {@link com.antares.cal.model.car.CarLocalization} costs the given price
 *
 * @author Tim Trense
 */
@Entity
@Table(name = "localization_price_range")
public class LocalizationPriceRange extends BaseEntity {

    @Column(name = "from_value", nullable = false)
    private Long from;

    @Column(name = "to_value", nullable = false)
    private Long to;

    @Column(name = "price_per_localisation", nullable = false)
    @Digits(integer = 2, fraction = 2)
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(long to) {
        this.to = to;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }
}
