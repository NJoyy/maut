package com.antares.cal.model.invoice;

import com.antares.cal.model.basic.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "invoices")
public class Invoice extends BaseEntity {

    // DO NOT MARK AS FOREIGN KEY! ONLY USED FOR QUERIES
    @Column(name = "owner_id", nullable = false)
    private Long ownerId;

    @Column(name = "invoice_date", nullable = false)
    private Long invoiceDate;

    @Column(name = "invoice_number", unique = true, nullable = false)
    private Long invoiceNumber;

    @Column(name = "net_amount", nullable = false)
    @Digits(integer = 5, fraction = 2)
    private BigDecimal netAmount;

    @Column(name = "tax_amount", nullable = false)
    @Digits(integer = 5, fraction = 2)
    private BigDecimal taxAmount;

    @Column(name = "invoice_file", nullable = false)
    @Lob
    private byte[] invoiceFile;

    @Column(name = "is_paid", nullable = false)
    private Boolean paid = false;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.LAZY)
    private List<InvoiceItem> invoiceItems = new ArrayList<>();

    public Long getOwnerId() {
        return ownerId;
    }
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getInvoiceDate() {
        return invoiceDate;
    }
    public void setInvoiceDate(long invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public long getInvoiceNumber() {
        return invoiceNumber;
    }
    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }
    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }
    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public byte[] getInvoiceFile() {
        return invoiceFile;
    }
    public void setInvoiceFile(byte[] invoiceFile) {
        this.invoiceFile = invoiceFile;
    }

    public Boolean isPaid() {
        return paid;
    }
    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public List<InvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }
    public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

}
