package com.antares.cal.model.basic;

import com.antares.cal.config.Time;
import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256Hash;
import com.antares.cal.utility.hash.PBKDF2WithHmacSHA256HashBuilder;

import javax.persistence.*;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * A class to implement the authorization data and login process
 *
 * @author Tim Trense
 */
@Entity
@Table(name = "passwords")
@Inheritance(strategy = InheritanceType.JOINED)
public class PasswordAuthorized extends BaseEntity {

    /**
     * Security Level of passwords.
     * //TODO: better use something like 100000 to 10 million
     */
    private static final int ITERATIONS = 10;

    @Column(name = "password", nullable = false, length = 33)
    private byte[] pwHash;

    @Column(name = "salt", nullable = false, length = 16)
    private byte[] salt;

    @SuppressWarnings("DefaultAnnotationParam")
    @Column(name = "last_login", nullable = true)
    private Long lastLoginTime = 0L;

    /**
     *
     * @return MILLISECONDS the timestamp of the last login that succeeded
     */
    public long getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * changes the password
     * @param password the new password
     */
    public void setPassword(String password) {
        SecureRandom r = new SecureRandom();
        final byte[] salt = new byte[2];
        r.nextBytes(salt);
        this.pwHash = hashPassword(password, salt);
        this.salt = salt;
    }

    /**
     * checks whether the given password is correct
     * @param password the password the user entered to try to login
     * @return whether the login should succeed due to correct password
     */
    public boolean login(String password) {
        final byte[] tryHash = hashPassword(password, this.salt);
        if (!Arrays.equals(tryHash, this.pwHash)) {
            return false;
        }
        this.lastLoginTime = Time.now();
        return true;
    }

    /**
     * generates a SHA-256 on Hmac Hash via PBKDF-2
     * @param password the data to hash
     * @param salt the salt to uniquify that data
     * @return the 32-Byte hash of that data
     */
    private static byte[] hashPassword(final String password, final byte[] salt) {
        PBKDF2WithHmacSHA256HashBuilder builder = new PBKDF2WithHmacSHA256HashBuilder(salt, ITERATIONS);
        final PBKDF2WithHmacSHA256Hash hash = builder.hash(password);
        return hash.getHash();
    }

}
