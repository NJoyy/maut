package com.antares.cal.model.basic;

import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.LicenseNumber;

/**
 * The origin country of a {@link Car} that is associated with the car's {@link LicenseNumber}
 *
 * @author Tim Trense
 * @author Arved Niedner
 * @author Fabian Mittmann
 * @since 0.1
 */
public enum Country {

	AUSTRIA("A"),
	AFGHANISTAN("AFG"),
	ALBANIA("AL"),
	ARMENIA("AM"),
	ANDORRA("AND"),
	AUSTRALIA("AUS"),
	AZERBAIJAN("AZ"),
	BELGIUM("B"),
	BANGLADESH("BD"),
	BARBADOS("BDS"),
	BURKINA_FASO("BF"),
	BULGARIA("BG"),
	BELIZE("BH"),
	BOSNIA_AND_HERZEGOVINA("BIH"),
	BOLIVIA("BOL"),
	BRAZIL("BR"),
	BAHRAIN("BRN"),
	BRUNEI("BRU"),
	BAHAMAS("BS"),
	MYANMAR("BUR"),
	BRITISH_VIRGIN_ISLANDS("BVI"),
	BOTSWANA("BW"),
	BELARUS("BY"),
	CUBA("C"),
	CAMEROON("CAM"),
	CANADA("CDN"),
	DEMOCRATIC_REPUBLIC_OF_THE_CONGO("CGO"),
	SWITZERLAND("CH"),
	IVORY_COAST("CI"),
	SRI_LANKA("CL"),
	COLOMBIA("CO"),
	COSTA_RICA("CR"),
	CYPRUS("CY"),
	CZECH_REPUBLIC("CZ"),
	GERMANY("D"),
	DENMARK("DK"),
	DOMINICAN_REPUBLIC("DOM"),
	BENIN("DY"),
	ALGERIA("DZ"),
	SPAIN("E"),
	KENYA("EAK"),
	TANZANIA("EAT"),
	UGANDA("EAU"),
	ZANZIBAR("EAZ"),
	ECUADOR("EC"),
	ERITREA("ER"),
	EL_SALVADOR("ES"),
	ESTONIA("EST"),
	EGYPT("ET"),
	ETHIOPIA("ETH"),
	FRANCE("F"),
	FINLAND("FIN"),
	FIJI("FJI"),
	LIECHTENSTEIN("FL"),
	FAROE_ISLAND("FO"),
	GABON("G"),
	UNITED_KINGDOM("GB"),
	ALDERNEY("GBA"),
	GUERNSEY("GBG"),
	JERSEY("GBJ"),
	ISLE_OF_MAN("GBM"),
	GIBRALTAR("GBZ"),
	GUATEMALA("GCA"),
	GEORGIA("GE"),
	GHANA("GH"),
	GREECE("GR"),
	GUYANA("GUY"),
	HUNGARY("H"),
	JORDAN("HKJ"),
	HONDURAS("HN"),
	CROATIA("HR"),
	ITALY("I"),
	ISRAEL("IL"),
	INDIA("IND"),
	IRAN("IR"),
	IRELAND("IRL"),
	IRAQ("IRQ"),
	ICELAND("IS"),
	JAPAN("J"),
	JAMAICA("JA"),
	CAMBODIA("K"),
	KYRGYZSTAN("KS"),
	SAUDI_ARABIA("KSA"),
	KUWAIT("KWT"),
	KAZAKHSTAN("KZ"),
	LUXEMBOURG("L"),
	LAOS("LAO"),
	LIBYA("LAR"),
	LIBERIA("LB"),
	LESOTHO("LS"),
	LITHUANIA("LT"),
	LATVIA("LV"),
	MALTA("M"),
	MOROCCO("MA"),
	MALAYSIA("MAL"),
	MONACO("MC"),
	MOLDOVA("MD"),
	MEXICO("MEX"),
	MACEDONIA("MK"),
	MONTENEGRO("MNE"),
	MONGOLIA("MNG"),
	MOZAMBIQUE("MOC"),
	MAURITIUS("MS"),
	MALAWI("MW"),
	NORWAY("N"),
	NETHERLANDS_ANTILLES("NA"),
	NAMIBIA("NAM"),
	NAURU("NAU"),
	NEPAL("NEP"),
	NICARAGUA("NIC"),
	NETHERLANDS("NL"),
	NEW_ZEALAND("NZ"),
	PORTUGAL("P"),
	PANAMA("PA"),
	PERU("PE"),
	PAKISTAN("PK"),
	POLAND("PL"),
	PAPUA_NEW_GUINEA("PNG"),
	PARAGUAY("PY"),
	QATAR("Q"),
	ARGENTINA("RA"),
	REPUBLIC_OF_CHINA_("RC"),
	CENTRAL_AFRICAN_REPUBLIC("RCA"),
	REPUBLIC_OF_THE_CONGO("RCB"),
	CHILE("RCH"),
	GUINEA("RG"),
	HAITI("RH"),
	INDONESIA("RI"),
	MAURITANIA("RIM"),
	KOSOVO("RKS"),
	LEBANON("RL"),
	MADAGASCAR("RM"),
	MALI("RMM"),
	NIGER("RN"),
	ROMANIA("RO"),
	SOUTH_KOREA("ROK"),
	PHILIPPINES("RP"),
	SAN_MARINO("RSM"),
	BURUNDI("RU"),
	RUSSIA("RUS"),
	RWANDA("RWA"),
	SWEDEN("S"),
	SWAZILAND("SD"),
	SINGAPORE("SGP"),
	SLOVAKIA("SK"),
	SLOVENIA("SLO"),
	SURINAME("SME"),
	SENEGAL("SN"),
	SOMALIA("SO"),
	SERBIA("SRB"),
	SUDAN("SUD"),
	SEYCHELLES("SY"),
	SYRIA("SYR"),
	THAILAND("T"),
	CHAD("TD"),
	TOGO("TG"),
	TAJIKISTAN("TJ"),
	TURKMENISTAN("TM"),
	TUNISIA("TN"),
	TURKEY("TR"),
	TRINIDAD_AND_TOBAGO("TT"),
	UKRAINE("UA"),
	UNITED_ARAB_EMIRATES("UAE"),
	UNITED_STATES("USA"),
	URUGUAY("UY"),
	UZBEKISTAN("UZ"),
	VATICAN_CITY("V"),
	VIETNAM("VN"),
	GAMBIA("WAG"),
	SIERRA_LEONE("WAL"),
	NIGERIA("WAN"),
	DOMINICA("WD"),
	GRENADA("WG"),
	SAINT_LUCIA("WL"),
	SAMOA("WS"),
	SAINT_VINCENT_AND_THE_GRENADINES("WV"),
	YEMEN("YAR"),
	VENEZUELA("YV"),
	ZAMBIA("Z"),
	SOUTH_AFRICA("ZA"),
	ZIMBABWE("ZW"),
    OTHER(null);

    private final String licencePrefix;

    Country(final String licencePrefix) {
        this.licencePrefix = licencePrefix;
    }

    /**
     * the result may be null for and only for {@link Country#OTHER}
     *
     * @return the locale prefix as written on the licence plate (eg "D" for Germany)
     */
    public String getLicencePrefix() {
        return licencePrefix;
    }

    /**
     * @param licencePrefix the locale prefix as written on the licence plate (eg "D" for Germany)
     * @return the associated origin country ({@link Country#OTHER} if the given licencePrefix is not found)
     */
    public static Country byLicencePrefix(final String licencePrefix) {
        for (Country c : Country.values()) {
            if (c.licencePrefix.equals(licencePrefix)) {
                return c;
            }
        }
        return OTHER;
    }
}
