package com.antares.cal.model.statistic;

/**
 * 
 * @author arved
 * object for the long haul driver statistic result query 
 */
public class LongHaulDrives {
	
	private String country;
	private long distance;
	
	public LongHaulDrives() {
		
	}
	
	public LongHaulDrives(String country, long distance) {
		this.country = country;
		this.distance = distance;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}
	
}
