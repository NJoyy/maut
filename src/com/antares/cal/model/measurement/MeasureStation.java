package com.antares.cal.model.measurement;

import com.antares.cal.model.basic.PasswordAuthorized;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The base data record for a physically existing measurement station
 *
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Entity
@Table(name = "measure_stations")
public class MeasureStation extends PasswordAuthorized {

    /**
     * The ID that the station itself knows about.
     * The BaseEntity-id of this class is only used internally,
     * while this id is sent along with any raw measurement to identify the measurement station.
     * This id exists, because on different instances of this system, the object id's may be different at runtime
     */
    @Column(name = "interface_id", unique = true, nullable = false)
    private long interfaceId;

    @Column(name = "name", unique = true,
            nullable = false, length = 45)
    private String name;

    @Column(name = "short_name", unique = true,
            nullable = false, length = 25)
    private String shortName;

    @Column(name = "type", nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MeasureStationType type;

    @OneToMany(mappedBy = "measureStation", fetch = FetchType.LAZY)
    private List<Measurement> measurements = new ArrayList<>();

    @ManyToMany(mappedBy = "progress",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    private List<Driveway> driveways = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public MeasureStationType getType() {
        return type;
    }

    public void setType(MeasureStationType type) {
        this.type = type;
    }

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    public List<Driveway> getDriveways() {
        return driveways;
    }

    public void setDriveways(List<Driveway> driveways) {
        this.driveways = driveways;
    }

    public Long getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(Long interfaceId) {
        this.interfaceId = interfaceId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof MeasureStation) {
            MeasureStation d = (MeasureStation) obj;
            if (d.getId().equals(this.getId())) {
                return true;
            }
            if (d.getInterfaceId().equals(this.getInterfaceId())) {
                return true;
            }
            if (d.getShortName().equals(this.getShortName())) {
                return true;
            }
            if (d.getName().equals(this.getName())) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
}
