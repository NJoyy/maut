package com.antares.cal.model.measurement;

import com.antares.cal.model.basic.BaseEntity;
import com.antares.cal.model.car.Car;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "measurement_collections")
public class MeasurementCollection extends BaseEntity {

    @OneToMany(mappedBy = "measurementCollection", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Measurement> measurements = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "car_id")
    private Car car;

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        if (measurements == null) {
            throw new IllegalArgumentException("Cannot set a null list of measurements for a measurement collection");
        }
        this.measurements = measurements;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Cannot assign a measurement collection to no (null) car");
        }
        this.car = car;
    }

    /**
     * @return the most recent measurement, if any, otherwise null
     */
    public Measurement getLast() {
        if (measurements.isEmpty()) {
            return null;
        } else {
            return measurements.get(measurements.size() - 1);
        }
    }

    /**
     * @return the most past measurement, if any, otherwise null
     */
    public Measurement getFirst() {
        if (measurements.isEmpty()) {
            return null;
        } else {
            return measurements.get(0);
        }
    }

    /**
     * @return whether the open drive is complete
     * @see #isStarted()
     * @see #isOpen()
     */
    public boolean isComplete() {
        Measurement last = getLast();
        if (last == null) {
            return false;
        }
        return getLast().getMeasureStation().getType() == MeasureStationType.EXIT;
    }

    /**
     * @return whether the open drive has begun
     * @see #isComplete()
     * @see #isOpen()
     */
    @SuppressWarnings("WeakerAccess")
    public boolean isStarted() {
        return !measurements.isEmpty();
    }

    /**
     * @return whether the drive has begun and not yet completed
     * @see #isComplete()
     * @see #isStarted()
     */
    public boolean isOpen() {
        return isStarted() && !isComplete();
    }

    /**
     * @param measurement a measurement to add to this open drive
     */
    public void addMeasurement(Measurement measurement) {
        if (isComplete()) {
            throw new IllegalStateException("cannot add a Measurement to a complete MeasurementCollection");
        }
        measurements.add(measurement);
    }
}
