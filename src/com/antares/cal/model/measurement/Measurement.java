package com.antares.cal.model.measurement;

import com.antares.cal.model.basic.BaseEntity;
import com.antares.cal.model.car.Car;

import javax.persistence.*;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "measurements")
public class Measurement extends BaseEntity {

    @Column(name = "time_stamp", nullable = false)
    private Long time;

    @ManyToOne
    @JoinColumn(name = "measure_station_id")
    private MeasureStation measureStation;

    @ManyToOne
    @JoinColumn(name = "measurement_collection_id")
    private MeasurementCollection measurementCollection;

    public Long getTime() {
        return time;
    }
    public void setTime(long timeStamp) {
        this.time = timeStamp;
    }

    public MeasureStation getMeasureStation() {
        return measureStation;
    }
    public void setMeasureStation(MeasureStation measureStation) {
        this.measureStation = measureStation;
    }

    public MeasurementCollection getMeasurementCollection() {
        return measurementCollection;
    }
    public void setMeasurementCollection(MeasurementCollection measurementCollection) {
        if(measurementCollection == null) {
            throw new IllegalArgumentException("Cannot assign a measurement to no (null) measurement collection");
        }
        this.measurementCollection = measurementCollection;
    }

    public Car getCar(){ return measurementCollection.getCar(); }
}
