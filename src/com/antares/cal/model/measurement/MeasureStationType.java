package com.antares.cal.model.measurement;

/**
 * @author Fabian Mittmann
 */
public enum MeasureStationType {
    APPROACH, EXIT
}
