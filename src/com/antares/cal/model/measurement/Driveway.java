package com.antares.cal.model.measurement;

import com.antares.cal.model.basic.BaseEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * the connecting directional edge between two {@link MeasureStation}s in the street map mesh
 *
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "driveways")
public class Driveway extends BaseEntity {

    @Column(name = "name", unique = true,
            nullable = false, length = 50)
    private String name;

    @Column(name = "distance", nullable = false)
    private Integer distance; // in meters

    @Column(name = "average_time")
    private Long averageTimeToCover;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "measurestations_driveways_join",
            joinColumns = @JoinColumn(name = "driveway_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "measurestation_id", referencedColumnName = "id"))
    private List<MeasureStation> progress = new ArrayList<>(2);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return METER distance between the to associated {@link MeasureStation}
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * @param distance METER distance between the to associated {@link MeasureStation}
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     * This value may never be higher than the minimum traverse time that can be achieved by driving as fast as law permits.
     *
     * @return MILLISECONDS average time that any car needs to drive from the starting {@link MeasureStation} to the ending one
     */
    public Long getAverageTimeToCover() {
        return averageTimeToCover;
    }

    /**
     * This value may never be higher than the minimum traverse time that can be achieved by driving as fast as law permits.
     *
     * @param averageTimeToCover MILLISECONDS average time that any car needs to drive from the starting {@link MeasureStation} to the ending one
     */
    @SuppressWarnings("WeakerAccess")
    public void setAverageTimeToCover(Long averageTimeToCover) {
        this.averageTimeToCover = averageTimeToCover;
    }

    /**
     * This value may never be higher than the minimum traverse time that can be achieved by driving as fast as law permits.
     *
     * @param averageTimeToCover SECONDS average time that any car needs to drive from the starting {@link MeasureStation} to the ending one
     */
    public void setAverageTimeToCoverSeconds(long averageTimeToCover) {
        setAverageTimeToCover(averageTimeToCover * 1000L);
    }

    /**
     * This value may never be higher than the speed that is permitted by law.
     *
     * @param mps METERS PER SECOND the average speed on this driveway
     */
    public void setAverageSpeed(int mps) {
        this.averageTimeToCover = (long) ((distance * 1000d) / mps);
    }

    /**
     * This value may never be higher than the speed that is permitted by law.
     *
     * @return METERS PER SECOND the average speed on this driveway
     */
    public double getAverageSpeed() {
        return (distance * 1000d) / averageTimeToCover;
    }

    public List<MeasureStation> getProgress() {
        return progress;
    }

    /**
     * gives access to the start point of the driveway
     *
     * @return the start point {@link MeasureStation}
     */
    public MeasureStation getStartPoint() {
        checkProgress();
        return progress.get(0);
    }

    /**
     * gives access to set the value of the start point of the driveway
     *
     * @param startPoint the {@link MeasureStation} that should be set as start point
     */
    public void setStartPoint(MeasureStation startPoint) {
        checkProgress();
        this.progress.set(0, startPoint);
    }

    /**
     * gives access to the end point of the driveway
     *
     * @return the end point {@link MeasureStation}
     */
    public MeasureStation getEndPoint() {
        checkProgress();
        return progress.get(1);
    }

    /**
     * gives access to set the value of the end point of the driveway
     *
     * @param endPoint the {@link MeasureStation} that should be set as end point
     */
    public void setEndPoint(MeasureStation endPoint) {
        checkProgress();
        this.progress.set(1, endPoint);
    }

    private void checkProgress() {
        if (this.progress == null) {
            this.progress = new ArrayList<>();
        }
        if (this.progress.size() != 2) {
            this.progress.clear();
            this.progress.add(null);
            this.progress.add(null);
        }
    }
}
