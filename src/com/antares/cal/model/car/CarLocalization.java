package com.antares.cal.model.car;

import com.antares.cal.model.invoice.InvoiceItem;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "car_localizations")
public class CarLocalization extends InvoiceItem {

    @Column(name = "counter", nullable = false)
    private long counter = 0;

    @Column(name = "price", nullable = false)
    private BigDecimal price = BigDecimal.ZERO;

    @OneToOne
    @JoinColumn(name = "car_id")
    private Car car;

    public Car getCar() {
        return car;
    }
    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getCounter() {
        return counter;
    }

    /**
     * this increases the counter plus one
     */
    public void incrementCounter() {
        this.counter++;
    }

    /**
     * this resets the counter to 0
     */
    public void resetCounter() {
        this.counter = 0;
    }

    /**
     * this generates a String which is used to describe the car localizations in the invoice
     * @return a String which composes of all properties in this class to describe the car localizations
     */
    @Override
    public String getDescription() {
        return "Your car " + car.getLicenseNumber().getCode() + " was localized " + counter +
               " times last month.";
    }

}
