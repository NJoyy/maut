package com.antares.cal.model.car;

import com.antares.cal.model.invoice.InvoiceItem;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * @author Fabian Mittmann
 */
@Entity
@Table(name = "drives")
public class Drive extends InvoiceItem {

    @Column(name = "time", nullable = false)
    private Long time;

    @Column(name = "distance", nullable = false)
    private Integer distance;

    @Column(name = "price", nullable = false)
    @Digits(integer = 3, fraction = 2)
    private BigDecimal price = BigDecimal.ZERO;

    @Column(name = "approach_point", nullable = false, length = 45)
    private String approachPoint;

    @Column(name = "exit_point", nullable = false, length = 45)
    private String exitPoint;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    public Long getTime() {
        return time;
    }

    public void setTime(long date) {
        this.time = date;
    }

    /**
     * @return METERS
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * @param distance METERS the distance of the entire drive
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getApproachPoint() {
        return approachPoint;
    }

    public void setApproachPoint(String approachPoint) {
        this.approachPoint = approachPoint;
    }

    public String getExitPoint() {
        return exitPoint;
    }

    public void setExitPoint(String exitPoint) {
        this.exitPoint = exitPoint;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    /**
     * this generates a String which is used to describe the drive in the invoice
     *
     * @return a String which composes of all properties in this class to describe the drive
     */
    @Override
    public String getDescription() {
        return time + ": Drive from " + approachPoint + " to " + exitPoint + " with your car " +
                car.getLicenseNumber().getCode() + " (distance" + distance + "km).";
    }

}
