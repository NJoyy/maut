package com.antares.cal.model.car;

import com.antares.cal.model.basic.Country;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * The ID to a {@link Car}. The serialized form has the pattern (originPrefix)({@link #ORIGIN_SEPARATOR})(licenceCode).
 *
 * @author Tim Trense
 * @author Fabian Mittmann
 * @since 0.1
 */
@Embeddable
public class LicenseNumber {

    /**
     * the separator between the origin prefix and the licence code.
     * value = "-"
     */
    @SuppressWarnings("WeakerAccess")
    public static final String ORIGIN_SEPARATOR = "-";

    @Column(name = "origin", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Country origin;

    @Column(name = "code", nullable = false, length = 10)
    private String code;

    /**
     * jpa-constructor, do not use manually
     */
    public LicenseNumber() {
    }

    /**
     * @param origin the origin country of the associated {@link Car}
     * @param code   the licence code as written on the licence plate
     */
    @SuppressWarnings("WeakerAccess")
    public LicenseNumber(final Country origin, final String code) {
        this.origin = origin;
        this.code = code;

        if (origin == null) {
            throw new IllegalArgumentException("the origin of a licence number cannot be null");
        }
        if (code == null || code.trim().isEmpty()) {
            throw new IllegalArgumentException("the code of a licence number can neither be null nor blank");
        }
    }

    /**
     * @return the origin country of the associated {@link Car}
     */
    public Country getOrigin() {
        return origin;
    }

    /**
     * @return the licence code as written on the licence plate
     */
    public String getCode() {
        return code;
    }

    /**
     * jpa-method, do not use manually
     * @param origin the origin country of the associated {@link Car}
     */
    public void setOrigin(Country origin) {
        this.origin = origin;
    }

    /**
     * jpa-method, do not use manually
     * @param code the licence code as written on the licence plate
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the serialized string representation of this licence number
     * @see #parse(String)
     */
    @SuppressWarnings("WeakerAccess")
    public String serialize() {
        return origin.getLicencePrefix() + ORIGIN_SEPARATOR + code;
    }

    /**
     * @param serial the serialized string representation of a licence number
     * @return a licence number
     * @see #serialize()
     */
    @SuppressWarnings("WeakerAccess")
    public static LicenseNumber parse(final String serial) {
        if (serial == null || serial.trim().isEmpty()) {
            throw new IllegalArgumentException("the given serial string can neither be null nor empty");
        }
        if (!serial.contains("-")) {
            throw new IllegalArgumentException("the given serial string is no valid licence number serial");
        }
        String originPrefix = serial.substring(0, serial.indexOf(ORIGIN_SEPARATOR));
        String code = serial.substring(serial.indexOf(ORIGIN_SEPARATOR) + 1);
        Country origin = Country.byLicencePrefix(originPrefix);
        return new LicenseNumber(origin, code);
    }

    @Override
    public String toString() {
        return serialize();
    }
}
