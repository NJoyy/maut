package com.antares.cal.model.car;

import com.antares.cal.model.basic.BaseEntity;
import com.antares.cal.model.measurement.MeasurementCollection;
import com.antares.cal.model.user.OwnerUser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A car that can drive along the mauted streets
 *
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Entity
@Table(name = "cars")
public class Car extends BaseEntity {

    @Embedded
    private LicenseNumber licenseNumber;

    @Column(name = "is_stolen", nullable = false)
    private Boolean stolen = Boolean.FALSE;

    @Column(name = "car_type", nullable = false, length = 55)
    @Enumerated(value = EnumType.STRING)
    private CarType carType;

    @ManyToOne
    @JoinColumn(name = "owner_user_id")
    private OwnerUser ownerUser;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Drive> drives = new ArrayList<>();

    @OneToOne(mappedBy = "car", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private CarLocalization carLocalization;

    @OneToOne(mappedBy = "car", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private MeasurementCollection currentMeasurements = new MeasurementCollection();

    public Car() {
        setCarLocalization(null);
    }

    public LicenseNumber getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(LicenseNumber licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Boolean getStolen() {
        return stolen;
    }

    public void setStolen(Boolean stolen) {
        this.stolen = stolen;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public OwnerUser getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(OwnerUser ownerUser) {
        this.ownerUser = ownerUser;
    }

    public List<Drive> getDrives() {
        return drives;
    }

    public void setDrives(List<Drive> drives) {
        if (drives == null) {
            drives = new LinkedList<>();
        }
        this.drives = drives;
    }

    public CarLocalization getCarLocalization() {
        return carLocalization;
    }

    public void setCarLocalization(CarLocalization carLocalization) {
        if (carLocalization == null) {
            carLocalization = new CarLocalization();
            carLocalization.setCar(this);
        }
        this.carLocalization = carLocalization;
    }

    public MeasurementCollection getCurrentMeasurements() {
        if (currentMeasurements == null) {
            currentMeasurements = new MeasurementCollection();
        }
        return currentMeasurements;
    }

    public void setCurrentMeasurements(MeasurementCollection currentMeasurements) {
        if (currentMeasurements == null) {
            currentMeasurements = new MeasurementCollection();
        }
        this.currentMeasurements = currentMeasurements;
    }

    public boolean isStolen() {
        return stolen;
    }

    public void setStolen(final boolean stolen) {
        this.stolen = stolen;
    }

    public void resetStolen() {
        this.stolen = false;
    }

    /**
     * adds the drive to the temporary buffer of drives of this month
     *
     * @param drive the drive to add
     */
    public void addDrive(Drive drive) {
        if (drive == null) {
            throw new IllegalArgumentException("Cannot add a null Drive to a Cars open drives of this month");
        }
        getDrives().add(drive);
    }
}
