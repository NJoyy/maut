package com.antares.cal.model.car;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
public enum CarType {

    AMBULANCE(false, true, false, true),
    CONSTRUCTION(false, true, true, false),
    GOVERNMENT(false, false, false, false),
    POLICE(true, true, false, true),
    PRIVATE(false, false, false, false);

    private final boolean ignoreWrongWay;
    private final boolean ignoreSpeeding;
    private final boolean ignoreMissing;
    private final boolean ignoreRescueLaneSpeeding;

    CarType(boolean ignoreWrongWay, boolean ignoreSpeeding, boolean ignoreMissing, boolean ignoreRescueLaneSpeeding) {
        this.ignoreWrongWay = ignoreWrongWay;
        this.ignoreSpeeding = ignoreSpeeding;
        this.ignoreMissing = ignoreMissing;
        this.ignoreRescueLaneSpeeding = ignoreRescueLaneSpeeding;
    }

    public boolean isIgnoreMissing() {
        return ignoreMissing;
    }

    public boolean isIgnoreSpeeding() {
        return ignoreSpeeding;
    }

    public boolean isIgnoreWrongWay() {
        return ignoreWrongWay;
    }

    public boolean isIgnoreRescueLaneSpeeding() {
        return ignoreRescueLaneSpeeding;
    }
}
