package com.antares.cal.model.alert;

import com.antares.cal.model.basic.BaseEntity;
import com.antares.cal.model.basic.Country;
import com.antares.cal.model.car.LicenseNumber;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * one machine readable additional information to an {@link Alert}
 *
 * @author Tim Trense
 */
@SuppressWarnings("WeakerAccess")
@Entity
public class AlertProperty extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "alert_id")
    private Alert alert;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    /**
     * @return the associated alert to which this information belongs
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     * @param alert the associated alert to which this information belongs
     */
    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    /**
     * @return the parameter key by which a third party system can identify this information
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the parameter key by which a third party system can identify this information
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the actual information
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the actual information
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return {@link #getValue()} as Integer, null if non-parsable
     */
    public Integer getValueInt() {
        try {
            return Integer.parseInt(getValue());
        } catch (NumberFormatException ignored) {
            return null;
        }
    }

    /**
     * @return {@link #getValue()} as Long, null if non-parsable
     */
    public Long getValueLong() {
        try {
            return Long.parseLong(getValue());
        } catch (NumberFormatException ignored) {
            return null;
        }
    }

    /**
     * @return {@link #getValue()} as Double, null if non-parsable
     */
    public Double getValueDouble() {
        try {
            return Double.parseDouble(getValue());
        } catch (NumberFormatException ignored) {
            return null;
        }
    }

    /**
     * @return {@link #getValue()} as LicenseNumber, null if non-parsable
     */
    public LicenseNumber getValueLicenceNumber() {
        try {
            return LicenseNumber.parse(getValue());
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }

    /**
     * @return {@link #getValue()} as Country, {@link Country#OTHER} if non-parsable
     */
    public Country getValueCountry() {
        return Country.byLicencePrefix(getValue());
    }

    /**
     * @return {@link #getValue()} as AlertType, null if non-parsable
     */
    public AlertType getValueAlertType() {
        try {
            return AlertType.valueOf(getValue());
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }
}
