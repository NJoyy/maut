package com.antares.cal.model.alert;

/**
 * Data about an {@link Alert} to be displayed to a user
 *
 * @author Tim Trense
 */
public class AlertRenderType {

    private final Alert origin;
    private final String date;
    private final String type;
    private final String message;

    /**
     * @param origin  the original {@link Alert}
     * @param date    string serialized form of {@link Alert#getTime()}
     * @param type    string serialized form of {@link Alert#getType()}
     * @param message string serialized form of {@link Alert#getMessage()}
     */
    public AlertRenderType(final Alert origin, String date, String type, String message) {
        this.origin = origin;
        this.date = date;
        this.type = type;
        this.message = message;
    }

    /**
     *
     * @return the original alert, this render component was created for
     */
    public Alert getOrigin() {
        return origin;
    }

    /**
     * @return string serialized form of {@link Alert#getMessage()}
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return string serialized form of {@link Alert#getType()}
     */
    public String getType() {
        return type;
    }

    /**
     * @return string serialized form of {@link Alert#getTime()}
     */
    public String getDate() {
        return date;
    }
}
