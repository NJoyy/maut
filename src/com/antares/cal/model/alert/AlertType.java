package com.antares.cal.model.alert;

/**
 * Any type of {@link Alert}
 * @author Tim Trense
 */
public enum AlertType {

    // system alerts
    INFO,
    MALFUNCTION,

    // measure station alerts
    MEASURESTATION_OFFLINE, // ms. diconnected

    //Test case
    MEASURESTATION_OFFLINE_TEST,
    MEASURESTATION_OFFLINE_TEST2,

    // traffic alerts
    STOLEN_CAR,
    WRONG_WAY_DRIVER,
    TRAFFIC_JAM,
    RESCUE_LANE_SPEEDER,
    CAR_MISSING,
    UNKNOWN_CAR,
    ;

}
