package com.antares.cal.model.alert;

import com.antares.cal.model.basic.BaseEntity;

import javax.persistence.*;
import java.util.*;

/**
 * A message within the AlertSystem
 *
 * @author Tim Trense
 */
@Entity
public class Alert extends BaseEntity {

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "time", nullable = false)
    private Long time;

    @Column(name = "type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AlertType type;

    @OneToMany(mappedBy = "alert", cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private List<AlertProperty> properties = new ArrayList<>();

    /**
     * @return the time at which this message occurred
     */
    @SuppressWarnings("WeakerAccess")
    public Long getTime() {
        return time;
    }

    /**
     * wraps {@link #getTime()} within a {@link Date}
     *
     * @return the time at which this message occurred as a {@link Date}
     */
    public Date getDate() {
        return new Date(getTime());
    }

    /**
     * @param time the time at which this message occurred
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * @return the message itself
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message itself
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the severity
     */
    public AlertType getType() {
        return type;
    }

    /**
     * @param type the severity
     */
    public void setType(AlertType type) {
        this.type = type;
    }

    /**
     * @return machine-readable additional information to this alert
     */
    @SuppressWarnings("WeakerAccess")
    public List<AlertProperty> getProperties() {
        return properties;
    }

    /**
     * @param properties machine-readable additional information to this alert
     */
    public void setProperties(List<AlertProperty> properties) {
        this.properties = properties;
    }

    /**
     * wraps {@link #getProperties()} to a map
     *
     * @return the machine-readable additional information to this alert (UNMODIFIABLE, to modify use {@link #getProperty(String)} and {@link #setProperty(String, String)})
     */
    public Map<String, String> getPropertiesMap() {
        final Map<String, String> result = new HashMap<>();
        for (AlertProperty p : getProperties()) {
            result.put(p.getKey(), p.getValue());
        }
        return Collections.unmodifiableMap(result);
    }

    /**
     * queries the properties and returns the one that has the given key
     *
     * @param key the {@link AlertProperty}'s key
     * @return the {@link AlertProperty} itself, or null if non-existent
     */
    @SuppressWarnings("WeakerAccess")
    public AlertProperty getProperty(final String key) {
        for (AlertProperty p : getProperties()) {
            if (p.getKey().equals(key)) {
                return p;
            }
        }
        return null;
    }


    /**
     * queries the properties and returns the value of the one that has the given key
     *
     * @param key the {@link AlertProperty}'s key
     * @return the {@link AlertProperty}'s value, or null if non-existent
     */
    public String getPropertyValue(final String key) {
        final AlertProperty p = getProperty(key);
        if (p != null) {
            return p.getValue();
        } else {
            return null;
        }
    }

    /**
     * sets the given property
     * @param key the key of the {@link AlertProperty} to set
     * @param value the value to set for the {@link AlertProperty}
     * @return the former value that was associated with that key (null if there was none)
     */
    @SuppressWarnings("WeakerAccess")
    public String setProperty(final String key, final String value) {
        AlertProperty p = getProperty(key);
        String oldValue = null;
        if (p == null) {
            p = new AlertProperty();
            p.setAlert(this);
            p.setKey(key);
        } else {
            oldValue = p.getValue();
        }
        p.setValue(value);
        return oldValue;
    }
}
