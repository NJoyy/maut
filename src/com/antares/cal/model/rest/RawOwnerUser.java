package com.antares.cal.model.rest;

import com.antares.cal.model.car.Car;
import com.antares.cal.model.user.Address;
import com.antares.cal.model.user.BillingPreference;
import com.antares.cal.model.user.PersonalData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 */

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawOwnerUser{

    // No-Argument Constructor for REST
    public RawOwnerUser() {
    }


    private BillingPreference billingPreference;

    private Address address;

    private PersonalData personalData;

    private List<Car> cars = new ArrayList<>();

    public BillingPreference getBillingPreference() {
        return billingPreference;
    }

    public void setBillingPreference(BillingPreference billingPreference) {
        this.billingPreference = billingPreference;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(final Car car) {
        if (car == null) {
            throw new IllegalArgumentException("Cannot add a null Car to an OwnerUser");
        }
        getCars().add(car);
    }
}
