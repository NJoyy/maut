package com.antares.cal.model.rest;

import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.measurement.MeasureStation;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;


/**
 * A class representing the parsed incoming measurement from a physical {@link MeasureStation}
 *
 * @author Tim Trense
 * @since 0.1
 */
// DO NOT MARK AS ENTITY: instances of this class are not ought to be persisted, but handled by a {@link MeasurementHandler}
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawMeasurement {

    private long time;
    private LicenseNumber licenseNumber; // this is no plain string, because a licence number can conveniently be acquired from a string
    private long interfaceId;
    private String password;

    public RawMeasurement() {
        this.time = 0;
        this.licenseNumber = null;
        this.interfaceId = 0;
    }


    /**
     * @param time          the timestamp that the {@link MeasureStation} captured this measurement (!! not the timestamp that this system received the measurement)
     * @param licenseNumber the licence number
     * @param interfaceId   the interface id of the MeasureStation to match {@link MeasureStation#getInterfaceId()}
     */
    @SuppressWarnings("WeakerAccess")
    public RawMeasurement(long time, LicenseNumber licenseNumber, long interfaceId, String interfacePassword) {
        this.time = time;
        this.licenseNumber = licenseNumber;
        this.interfaceId = interfaceId;
        this.password = interfacePassword;

        if (time < 1) {
            throw new IllegalArgumentException("cannot create a raw measurement upon a non-positive timestamp");
        }
        if (licenseNumber == null) {
            throw new IllegalArgumentException("cannet create a raw measurement upon a null licence number");
        }
    }

    /**
     * @return the timestamp that the {@link MeasureStation} captured this measurement (!! not the timestamp that this system received the measurement)
     */
    @SuppressWarnings("WeakerAccess")
    public Long getTime() {
        return time;
    }

    /**
     * calls {@link #getTime()} and wraps the result in a {@link Date}
     *
     * @return the timestamp that the {@link MeasureStation} captured this measurement (!! not the timestamp that this system received the measurement)
     */
    public Date getDate() {
        return new Date(getTime());
    }

    /**
     * @return the licence number
     */
    public LicenseNumber getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * @return the id of the {@link MeasureStation#getInterfaceId()}
     */
    public Long getInterfaceId() {
        return interfaceId;
    }

    /**
     *
     * @param time the timestamp that the {@link MeasureStation} captured this measurement (!! not the timestamp that this system received the measurement)
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     *
     * @param interfaceId the id of the {@link MeasureStation#getInterfaceId()}
     */
    public void setInterfaceId(long interfaceId) {
        this.interfaceId = interfaceId;
    }

    /**
     *
     * @param licenseNumber the licence number
     */
    public void setLicenseNumber(LicenseNumber licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     *
     * @param licenseNumber a string serialized form that can be parsed using {@link LicenseNumber#parse(String)}
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = LicenseNumber.parse(licenseNumber);
    }

    /**
     *
     * @return the password transmitted to authenticate the measure station
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password the password transmitted to authenticate the measure station
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
