package com.antares.cal.model.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Fabian Mittmann
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RawInvoice {

    //No Argument Constructor - REST
    public RawInvoice() {
    }

    private Long invoiceNumber;
    private Boolean paid;


    public RawInvoice(long invoiceNumber, boolean paid) {
        this.invoiceNumber = invoiceNumber;
        this.paid = paid;
    }

    public Long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

}
