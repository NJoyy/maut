package com.antares.cal.controllers.rest.controller;

import com.antares.cal.model.alert.Alert;
import com.antares.cal.service.logic.alert.AlertSystem;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * @author Paul Konietzny
 */

@Path("/alerts")
public class MeasureStationAlertRestController {


    private AlertSystem as;


    @POST
    @Path("/msping")
    @Consumes(MediaType.APPLICATION_JSON)
    public void MeasureStationOffline(Alert alert) {

        //gtfo!
        as = AlertSystem.getInstance();
        as.fireAlert(alert);
    }

}
