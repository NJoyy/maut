package com.antares.cal.controllers.rest.controller;

import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.dao.user.OwnerUserDao;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST interface to import owner users into database
 * @author Paul Konietzny
 * @author Fabian Mittmann
 */
@Path("/user")
public class OwnerUserRestController {

    @Inject
    private OwnerUserDao ownerUserDao;

    /**
     * consumes an object {@link OwnerUser} via REST interface as JSON data to persist in database
     * @param ownerUser an object {@link OwnerUser} to add via REST in database
     * @return a {@link Response} for the REST data producer
     */
    @POST
    @Path("/addOwnerUser")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOwnerUser(OwnerUser ownerUser) {
        if(ownerUserDao.findByLastNameAndFirstName(ownerUser) != null) {
            return Response.status(Response.Status.FORBIDDEN).entity("User already exists. DO not try to add 2 identical users.").build();
        } else {
            ownerUserDao.insertOwnerUser(ownerUser);
            return Response.status(Response.Status.CREATED).entity("User created successfully.").build();
        }
    }

}
