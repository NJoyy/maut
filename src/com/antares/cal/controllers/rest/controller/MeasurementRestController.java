package com.antares.cal.controllers.rest.controller;

import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.model.rest.RawMeasurement;
import com.antares.cal.service.dao.measurement.MeasureStationDao;
import com.antares.cal.service.logic.measurement.MeasurementHandler;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Rest interface to consume JSON form of a {@link RawMeasurement} from any measure station and call the {@link com.antares.cal.service.logic.measurement.MeasurementHandler}
 *
 * @author Tim Trense
 * @author Paul Konietzny
 */
@SuppressWarnings("CdiInjectionPointsInspection") // this will actually work
@Path("/measurements")
public class MeasurementRestController {

    @Inject
    private MeasurementHandler handler;

    @Inject
    private MeasureStationDao measureStationDao;

    /**
     * receive JSON data to persist measurement data into database
     *
     * @param measurement the raw measurement to process
     */
    @POST
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insertMeasurement(RawMeasurement measurement) {
        if (handler == null) {
            System.err.println("No global measurement handler present");
        }

        if (measurement == null) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity("REST Interface only accepts full set of Measurement").build();
        }

        MeasureStation ms = measureStationDao.findByInterfaceId(measurement.getInterfaceId());
        if (!ms.login(measurement.getPassword())) {
            return Response.status(Response.Status.FORBIDDEN).entity("Access denied").build();
        }

        handler.handle(measurement);
        return Response.status(Response.Status.CREATED).entity("measurement created successfully").build();
    }

}
