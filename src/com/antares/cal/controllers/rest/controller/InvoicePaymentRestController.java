package com.antares.cal.controllers.rest.controller;

import com.antares.cal.model.invoice.Invoice;
import com.antares.cal.model.rest.RawInvoice;
import com.antares.cal.service.dao.invoice.InvoiceDao;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Rest interface to set the paid_status from existing bill to paid
 * this demonstrate a interface WITHOUT security features - so a bank or whatever can confirm that the bill is paid
 * @author Paul Konietzny
 */
@Path("/invoice")
public class InvoicePaymentRestController {

    @Inject
    private InvoiceDao dao;

    @POST
    @Path("/setPaid")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response InvoicePaymentIsPaid(RawInvoice rawInvoice){

        if(!rawInvoice.isPaid()){
            return Response.status(Response.Status.FORBIDDEN).entity("REST Interface only accepts paid as true").build();
        }

       Invoice invoice = dao.findByInvoiceNumber(rawInvoice.getInvoiceNumber());
        invoice.setPaid(true);
       dao.updateInvoice(invoice);

       return Response.status(Response.Status.ACCEPTED).entity("OK").build();
    }
}
