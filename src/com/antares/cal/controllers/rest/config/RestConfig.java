package com.antares.cal.controllers.rest.config;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


// Application controller for REST-Services
// the path will be ~/maut/rest/* for all incoming new rest-requests

@ApplicationPath("/rest")
public class RestConfig extends Application {
}
