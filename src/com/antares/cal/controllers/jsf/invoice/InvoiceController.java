package com.antares.cal.controllers.jsf.invoice;

import com.antares.cal.model.invoice.Invoice;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.dao.invoice.InvoiceDao;
import com.antares.cal.utility.convert.DataFormatter;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * provides CRUD functionality of {@link Invoice}s for the web view
 *
 * @author Fabian Mittmann
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Named
@RequestScoped
public class InvoiceController {

    @Inject
    private InvoiceDao dao;

    // required for getAllInvoices()
    private List<Invoice> rawInvoices;
    private DataModel<Invoice> allInvoicesDataModel;

    /**
     * loads a list of all {@link Invoice}s from database for use in web view
     * @return a {@link DataModel} of {@link Invoice}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Invoice> getAllInvoices() {
        rawInvoices = new ArrayList<>();
        rawInvoices.addAll(dao.findAllInvoices());
        allInvoicesDataModel = new ListDataModel<>(rawInvoices);
        return allInvoicesDataModel;
    }

    // required for getAllUnpaidInvoices()
    private DataModel<Invoice> unpaidInvoicesDataModel;

    /**
     * loads a list of all unpaid {@link Invoice}s from database for use in web view
     * @return a {@link DataModel} of {@link Invoice}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Invoice> getAllUnpaidInvoices() {
        rawInvoices = new ArrayList<>();
        rawInvoices.addAll(dao.findAllUnpaidInvoices());
        unpaidInvoicesDataModel = new ListDataModel<>(rawInvoices);
        return unpaidInvoicesDataModel;
    }

    // required for getInvoicesByOwner() and getInvoicesByOwnerAndDate()
    private DataModel<Invoice> invoicesFromOwnerDataModel;

    /**
     * loads a list of all {@link Invoice}s of the owner from database for use in web view
     *
     * @param owner an {@link OwnerUser} to search for its {@link Invoice}s
     * @return a {@link DataModel} of {@link Invoice}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Invoice> getInvoicesByOwner(OwnerUser owner) {
        rawInvoices = new ArrayList<>();
        rawInvoices.addAll(dao.findByOwnerUser(owner));
        invoicesFromOwnerDataModel = new ListDataModel<>(rawInvoices);
        return invoicesFromOwnerDataModel;
    }

    public DataModel<Invoice> getInvoicesByOwnerAndDate(OwnerUser owner, long startDate, long endDate) {
        rawInvoices = new ArrayList<>();
        rawInvoices.addAll(dao.findByDateAndOwnerUser(owner, startDate, endDate));
        invoicesFromOwnerDataModel = new ListDataModel<>(rawInvoices);
        return invoicesFromOwnerDataModel;
    }

    public StreamedContent getFile(Invoice i) {
        ByteArrayInputStream bais = new ByteArrayInputStream(i.getInvoiceFile());
        return new DefaultStreamedContent(bais, "application/pdf", "rechnung.pdf");
    }

    public String formatTimeStamp(long timeStamp) {
        return DataFormatter.formatDateMonthAndYear(timeStamp);
    }

    public String formatPrice(BigDecimal bd) {
        return DataFormatter.formatBigDecimalPrice(bd);
    }

    public String formatIsPaid(boolean b) {
        return DataFormatter.formatBoolean(b);
    }

}
