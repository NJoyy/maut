package com.antares.cal.controllers.jsf.time;

import com.antares.cal.config.Time;
import com.antares.cal.config.TimeListener;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Tim Trense
 */
@Named
@SessionScoped
public class TimeController implements Serializable, TimeListener {

    private long time;

    @PostConstruct
    public void init() {
        Time.getInstance().addListener(this);
    }

    @PreDestroy
    public void uninit() {
        Time.getInstance().removeListener(this);
    }

    @Override
    public void onTimeChanged(long now) {
        setTime(now);
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void save() {
        Time.getInstance().setTime(time);
    }
}
