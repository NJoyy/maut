package com.antares.cal.controllers.jsf.statistic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.antares.cal.model.statistic.LongHaulDrives;
import com.antares.cal.service.dao.statistic.BuildStatisticDao;
/**
 * 
 * @author arved niedner
 *
 */
@Named
@RequestScoped
public class StatisticController implements Serializable {
	
	private String warning;
	private Date startDate;
	private Date endDate;
	
	@Inject
	private BuildStatisticDao dao;
	
	private BarChartModel barModelForeign;
	private BarChartModel barModelAll;
	
	private List<LongHaulDrives> longHaulDrivesRaw;
	private DataModel<LongHaulDrives> longHaulDrivesModel;
	
	/**
	 * removes last warning when page is reloaded
	 */
	public void init() {
		setWarning("");
	}
	
	/**
	 * loads a list of countries and their driven distance from the data base
	 * @param startDate begin of time span
	 * @param endDate end of time span
	 * @return values of long haul driver statistic
	 */
	public String BuildLongHaulDriverStatistic() {
		
		if (startDate == null || endDate == null) {
			setWarning("Es wurde kein Anfangs- oder Enddatum angegeben!");
			return null;
		}
		longHaulDrivesRaw = new ArrayList<>();
		longHaulDrivesRaw.addAll(dao.createLongHaulDriverStatisticForeign(startDate.getTime(), endDate.getTime()));
		if (longHaulDrivesRaw.isEmpty()) {
			setWarning("Es konnten keine Datens�tze f�r den angegebenen Zeitraum gefunden werden!");
			return null;
		}
		longHaulDrivesModel = new ListDataModel<LongHaulDrives>(longHaulDrivesRaw);
		barModelForeign = new BarChartModel();
		barModelForeign = BuildBarChartModel(barModelForeign);
		if (barModelForeign == null) {
			setWarning("Das Balkendiagramm f�r die ausl�ndischen PKWs konnte nicht erstellt werden!");
			return null;
		}
		longHaulDrivesRaw = new ArrayList<>();
		longHaulDrivesRaw.addAll(dao.createLongHaulDriverStatisticAll(startDate.getTime(), endDate.getTime()));
		if (longHaulDrivesRaw.isEmpty()) {
			setWarning("Es konnten keine Datens�tze f�r den angegebenen Zeitraum gefunden werden!");
			return null;
		}
		longHaulDrivesModel = new ListDataModel<LongHaulDrives>(longHaulDrivesRaw);
		barModelAll = new BarChartModel();
		barModelAll = BuildBarChartModel(barModelAll);
		if (barModelAll == null) {
			setWarning("Das Balkendiagramm f�r alle PKWs konnte nicht erstellt werden!");
			return null;
		}
		return "longhauldrivesstatistic";
	}
	
	/**
	 * builds the bar chart
	 * @return check if chart is created
	 */
	public BarChartModel BuildBarChartModel(BarChartModel barModel) {
		ChartSeries longHaulDrives = new ChartSeries();
		longHaulDrivesModel.setRowIndex(0);
		for (int i = 0; i < longHaulDrivesModel.getRowCount(); i++) {
			longHaulDrives.set(longHaulDrivesModel.getRowData().getCountry(), longHaulDrivesModel.getRowData().getDistance());
			longHaulDrivesModel.setRowIndex(i);
		}
		longHaulDrives.setLabel("Kilometeranzahl");
		barModel.addSeries(longHaulDrives);
		barModel.setTitle("Kilometerfresser-Statistik");
		barModel.setLegendPosition("ne");
		Axis xAxis = barModelForeign.getAxis(AxisType.X);
		xAxis.setLabel("Land");
		Axis yAxis = barModel.getAxis(AxisType.Y);
		yAxis.setLabel("Kilometer");
		return barModel;
	}
	
	public void setBarModelForeign(BarChartModel barModel) {
		this.barModelForeign = barModel;
	}
	
	public BarChartModel getBarModelForeign() {
		return barModelForeign;
	}
	
	public void setBarModelAll(BarChartModel barModel) {
		this.barModelAll = barModel;
	}
	
	public BarChartModel getBarModelAll() {
		return barModelAll;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getWarning() {
		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}
	
}
