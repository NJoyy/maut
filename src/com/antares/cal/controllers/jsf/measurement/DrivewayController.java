package com.antares.cal.controllers.jsf.measurement;

import com.antares.cal.model.measurement.Driveway;
import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.service.dao.measurement.DrivewayDao;
import com.antares.cal.service.dao.measurement.MeasureStationDao;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * this provides CRUD functionality of {@link Driveway}s for the web view
 * @author Fabian Mittmann
 */
@SuppressWarnings("CdiInjectionPointsInspection")
@Named
@SessionScoped
public class DrivewayController implements Serializable {

    @Inject
    private DrivewayDao drivewayDao;

    @Inject
    private MeasureStationDao measureStationDao;

    private boolean unsaveable = false;

    // initialized in createDriveway()
    private Driveway inEdit;

    // initialized in searchForStartPoint() and searchForEndPoint()
    private MeasureStation drivewayStartPoint;
    private MeasureStation drivewayEndPoint;

    /* search parameters for searchForStartPoint() and searchForEndPoint(),
       they should declare the short name of the measure station,
       both should be mapped on text fields,
       they need to be validated so that only measure station short names can be set as parameters*/
    private String startPointShortName;
    private String endPointShortName;

    // required for getAllDriveways()
    private List<Driveway> rawDriveways;
    private DataModel<Driveway> drivewayDataModel;

    /**
     * loads a list of all {@link Driveway}s from database for use in web view
     * @return a {@link DataModel} to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Driveway> getAllDriveways() {
        rawDriveways = new ArrayList<>();
        rawDriveways.addAll(drivewayDao.findAllDriveways());
        drivewayDataModel = new ListDataModel<>(rawDriveways);
        return drivewayDataModel;
    }

    /**
     * initializes a new {@link Driveway} for use in the web view to give it values
     * @return the name of the edit page of {@link Driveway}s
     */
    public String createDriveway() {
        inEdit = new Driveway();
        return "createdriveway";
    }

    /**
     * reads out the value of property {@link #startPointShortName} and sets the variable {@link #drivewayStartPoint},
     * sets the start point property of the edited {@link Driveway} variable {@link #inEdit},
     * should be called on a button that is searching for the start point of the driveway that should be created
     */
    public void searchForStartPoint() {
        drivewayStartPoint = measureStationDao.findByShortName(startPointShortName);
        inEdit.setStartPoint(drivewayStartPoint);
    }

    /**
     * reads out the value of property {@link #endPointShortName} and sets the variable {@link #drivewayEndPoint},
     * sets the end point property of the edited {@link Driveway} variable {@link #inEdit},
     * should be called on a button that is searching for the end point of the driveway that should be created
     */
    public void searchForEndpoint() {
        drivewayEndPoint = measureStationDao.findByShortName(endPointShortName);
        inEdit.setEndPoint(drivewayEndPoint);
    }

    /**
     * provides the functionality of saving the edited {@link Driveway} to the database
     * @return the name of the {@link Driveway}s management page where you can see the changes
     */
    public String saveDriveway() {
        if (inEdit.getStartPoint() == null || inEdit.getEndPoint() == null) {
            this.unsaveable = true;
            return "createdriveway";
        }
        drivewayDao.insertDriveway(inEdit);
        return "editdriveway";
    }

    /**
     * deletes the in the web view selected {@link Driveway} from the database
     * @param driveway a {@link Driveway} to delete from database
     * @return the name of the {@link Driveway}s management page where you can see the changes
     */
    public String deleteDriveway(Driveway driveway) {
        drivewayDao.removeDriveway(driveway);
        return "editdriveway";
    }

    public Driveway getInEdit() {
        return inEdit;
    }
    public void setInEdit(Driveway inEdit) {
        this.inEdit = inEdit;
    }

    public MeasureStation getDrivewayStartPoint() {
        return drivewayStartPoint;
    }

    public MeasureStation getDrivewayEndPoint() {
        return drivewayEndPoint;
    }

    public String getStartPointShortName() {
        return startPointShortName;
    }
    public void setStartPointShortName(String startPointShortName) {
        this.startPointShortName = startPointShortName;
    }

    public String getEndPointShortName() {
        return endPointShortName;
    }
    public void setEndPointShortName(String endPointShortName) {
        this.endPointShortName = endPointShortName;
    }

    public boolean isUnsaveable() {
        return unsaveable;
    }
}
