package com.antares.cal.controllers.jsf.measurement;

import com.antares.cal.model.measurement.MeasureStation;
import com.antares.cal.service.dao.measurement.MeasureStationDao;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * this provides CRUD functionality of {@link MeasureStation}s for the web view
 * @author Fabian Mittmann
 */
@SuppressWarnings("serial")
@Named
@SessionScoped
public class MeasureStationController implements Serializable {

    @Inject
    private MeasureStationDao dao;
    private String password;

    /**
     * initialized in {@link #createMeasureStation()}
     */
    private MeasureStation inEdit;

    // both used in method getAllMeasureStations()
    private List<MeasureStation> rawMeasureStations;
    private DataModel<MeasureStation> measureStationsDataModel;

    /**
     * loads a list of all {@link MeasureStation}s from database for use in web view
     * @return a {@link DataModel} to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<MeasureStation> getAllMeasureStations() {
        rawMeasureStations = new ArrayList<>();
        rawMeasureStations.addAll(dao.findAllMeasureStations());
        measureStationsDataModel =  new ListDataModel<MeasureStation>(rawMeasureStations);
        return measureStationsDataModel;
    }

    /**
     * initializes a new {@link MeasureStation} for use in the web view to give it values
     * @return the name of the edit page of {@link MeasureStation}s
     */
    public String createMeasureStation() {
        inEdit = new MeasureStation();
        return "createmeasurestation";
    }

    /**
     * provides the functionality of saving the edited {@link MeasureStation} to the database
     * @return the name of the {@link MeasureStation}s management page where you can see the changes
     */
    public String saveMeasureStation() {
    	inEdit.setPassword(getPassword());
        dao.insertMeasurestation(inEdit);
        return "editmeasurestation";
    }

    /**
     * deletes the in the web view selected {@link MeasureStation} from the database
     * @param station a {@link MeasureStation} to delete from database
     * @return the name of the {@link MeasureStation}s management page where you can see the changes
     */
    public String deleteMeasureStation(MeasureStation station) {
        dao.deleteMeasureStation(station);
        return "measurestationmanagement";
    }

    public MeasureStation getInEdit() {
        return inEdit;
    }
    public void setInEdit(MeasureStation inEdit) {
        this.inEdit = inEdit;
    }
    
    public SelectItem[] getMeasureStationName() {
    	final Collection<MeasureStation> collection = dao.findAllMeasureStations();
    	SelectItem[] items = new SelectItem[collection.size()];
    	int i = 0;
    	for (MeasureStation m : collection) {
			items[i] = new SelectItem();
			items[i].setValue(m);
			items[i].setLabel(m.getShortName().toString());
			i++;
		}
    	return items;
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
