package com.antares.cal.controllers.jsf.user;

import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.dao.user.OwnerUserDao;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * provides functionality to interact with owner data
 * @author Fabian Mittmann
 */
@Named
@SessionScoped
public class OwnerUserController implements Serializable {

    @Inject
    private OwnerUserDao dao;

    /**
     * updates the values of an {@link OwnerUser} in database
     * @param ownerUser a {@link OwnerUser} to update in database
     * @return the name of the main page of {@link OwnerUser}
     */
    public String updateOwner(OwnerUser ownerUser) {
        dao.updateOwnerUser(ownerUser);
        return "mainpage";
    }
    
    public OwnerUser getOwnerUserByID(Long id) {
    	 return dao.findById(id);
    }
}
