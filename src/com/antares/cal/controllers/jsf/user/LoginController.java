package com.antares.cal.controllers.jsf.user;

import com.antares.cal.model.user.AgencyType;
import com.antares.cal.model.user.AgencyUser;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.model.user.User;
import com.antares.cal.service.dao.user.LoginDao;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Named
@SessionScoped
public class LoginController implements Serializable {

    @Inject
    private LoginDao loginDao;

    private String loginName;
    private String password;

    private User loggedUser;

    /**
     * this tries to login a user with the login name and the password properties
     * if that is successful the user object gets put into the session map
     *
     * @return the main page of the user logged in or an error page if login has failed
     */
    @SuppressWarnings("ConstantConditions")
    public String login() {
        //noinspection UnusedAssignment : initialization to clarify
        User user = null;
        if (loginName == null || password == null) {
            System.err.println("login routine called without passing loginName and password");
            return "login-error";
        }

        System.out.println("finding user...");
        user = loginDao.findOwnerUserByLogin(loginName);
        if (user == null) {
            user = loginDao.findAgencyUserByLogin(loginName);
        }
        //... add other user types if necessary

        if (user == null) {
            // redirect to error page due to not finding the user
            System.out.println("user with name=" + loginName + " not found.");
            return "login-error";
        }

        if (!user.login(password)) {
            // redirect to error page due to login has failed
            System.out.println("password-authentication for user=" + loginName + " failed.");
            return "login-error";
        }

        System.out.println("login for user=" + loginName + " successful");

        // puts user into session so you can check login with a filter anytime
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("loggedUser", user);

        // redirect to proper mainpage according to user type
        if (user instanceof OwnerUser) {
            loggedUser = user;
            return "owner/mainpage?faces-redirect=true";
        } else if (user instanceof AgencyUser) {
            AgencyUser agencyUser = (AgencyUser) user;
            loggedUser = user;
            if (agencyUser.getAgencyType() == AgencyType.GOVERNMENT) {
                return "gov/mainpage?faces-redirect=true";
            } else if (agencyUser.getAgencyType() == AgencyType.POLICE) {
                return "police/mainpage?faces-redirect=true";
            } else if (agencyUser.getAgencyType() == AgencyType.ADMIN) {
                return "admin/mainpage?faces-redirect=true";
            } else {
                throw new RuntimeException("agency user has unsupported agency type");
            }
        } else {
            throw new RuntimeException("Unknown user type");
        }
    }

    public String getLoginName() {
        return loginName;
    }
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public OwnerUser getUserAsOwner() {
        return (OwnerUser) loggedUser;
    }

    public AgencyUser getUserAsAgency() {
        return (AgencyUser) loggedUser;
    }

    public void setUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }
    public User getUser() {
        return loggedUser;
    }


}
