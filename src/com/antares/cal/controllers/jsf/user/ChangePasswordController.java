package com.antares.cal.controllers.jsf.user;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.antares.cal.model.user.AgencyUser;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.model.user.User;
import com.antares.cal.service.dao.user.AgencyUserDao;
import com.antares.cal.service.dao.user.OwnerUserDao;

@SuppressWarnings("CdiInjectionPointsInspection")
@Named
@SessionScoped
public class ChangePasswordController implements Serializable {

    private String warning;
    private String oldPassword;
    private String password, passwordRepeat;

    @Inject
    private OwnerUserDao ownerUserDao;
    @Inject
    private AgencyUserDao agencyUserDao;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void changePassword(User user) {
        final String password = getPassword();
        final String passwordRepeat = getPasswordRepeat();
        final String oldPassword = getOldPassword();

        final boolean ok = user.login(oldPassword);

        setOldPassword("");

        if (!ok) {
            setWarning("Altes Passwort falsch");
            return;
        }

        if (!password.equals(passwordRepeat)) {
            setWarning("Neues Passwort stimmt nicht mit Wiederholung überein");
            return;
        }

        user.setPassword(password);
        if (user instanceof OwnerUser) {
            ownerUserDao.updateOwnerUser((OwnerUser) user);
            setWarning("Passwortünderung erfolgreich");
        } else if (user instanceof AgencyUser) {
            agencyUserDao.updateAgencyUser((AgencyUser) user);
            setWarning("Passwortünderung erfolgreich");
        } else {
            setWarning("Fehler beim Speichern");
        }

        setPassword("");
        setPasswordRepeat("");
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }
}
