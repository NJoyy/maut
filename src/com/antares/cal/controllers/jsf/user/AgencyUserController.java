package com.antares.cal.controllers.jsf.user;

import com.antares.cal.model.user.AgencyUser;
import com.antares.cal.service.dao.user.AgencyUserDao;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * provides functionality to interact with {@link AgencyUser} data
 * @author Fabian Mittmann
 */
@Named
@SessionScoped
public class AgencyUserController implements Serializable {

    @Inject
    private AgencyUserDao dao;

    /**
     * updates the values of an {@link AgencyUser} in database
     * @param agencyUser a {@link AgencyUser} to update in database
     * @return the name of the main page of {@link AgencyUser}
     */
    public String updateAgencyUser(AgencyUser agencyUser) {
        dao.updateAgencyUser(agencyUser);
        //TODO add return value, maybe navigation rules for different user roles
        return "";
    }

}
