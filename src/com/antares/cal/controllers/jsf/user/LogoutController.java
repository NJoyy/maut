package com.antares.cal.controllers.jsf.user;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

/**
 * this jsf bean provides logout functionality
 * @author Fabian Mittmann
 */
@Named
@RequestScoped
public class LogoutController implements Serializable {

    /**
     * this kills the session and redirects to logout page
     * @return name of the logout page
     */
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "logout";
    }

}
