package com.antares.cal.controllers.jsf.basic;

import com.antares.cal.model.measurement.MeasureStationType;
import com.antares.cal.model.user.BillingPreference;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import java.io.Serializable;

/**
 * provides access to constants values that should be available in the web view
 * @author Fabian Mittmann
 * @author Tim Trense
 */
@Named
@ApplicationScoped
public class ConstantController implements Serializable {

    /**
     *
     * @return a selectmenu with every billing preference
     */
    public SelectItem[] getBillingPreferences() {
        SelectItem[] options = new SelectItem[BillingPreference.values().length];
        SelectItem item = new SelectItem();
        item.setLabel("E-Mail");
        item.setValue(BillingPreference.EMAIL);
        options[0] = item;
        item = new SelectItem();
        item.setLabel("Brief");
        item.setValue(BillingPreference.LETTER);
        options[1] = item;
        item = new SelectItem();
        item.setLabel("Online-Ansicht");
        item.setValue(BillingPreference.ONLINE);
        options[2] = item;
        return options;
    }
    
    /**
    *
    * @return a selectmenu with every MeasureStationType
    */
    public SelectItem[] getMeasureStationType() {
        SelectItem[] options = new SelectItem[MeasureStationType.values().length];
        SelectItem item = new SelectItem();
        item.setLabel("Auffahrt");
        item.setValue(MeasureStationType.APPROACH);
        options[0] = item;
        item = new SelectItem();
        item.setLabel("Abfahrt");
        item.setValue(MeasureStationType.EXIT);
        options[1] = item;
        return options;
    }
    
}
