package com.antares.cal.controllers.jsf.alert;

import com.antares.cal.model.alert.Alert;
import com.antares.cal.model.alert.AlertRenderType;
import com.antares.cal.model.user.AgencyType;
import com.antares.cal.model.user.AgencyUser;
import com.antares.cal.service.logic.alert.AlertSystem;
import com.antares.cal.utility.convert.DataFormatter;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * this provides CRUD functionality of {@link Alert}s for the web view
 *
 * @author Fabian Mittmann
 */
@Named
@RequestScoped
public class AlertController implements Serializable {

    /**
     * loads a list of all {@link Alert}s for use in web view
     *
     * @return a {@link DataModel} of {@link Alert}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Alert> getAllAlerts() {
        return new ListDataModel<>(AlertSystem.getInstance().getUnhandled());
    }

    /**
     * loads a list of all {@link Alert}s the user has access for to use in web view
     *
     * @param user a object of {@link AgencyUser} to check access for alerts
     * @return a {@link DataModel} of {@link Alert}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<AlertRenderType> getAlertsForUserGroup(AgencyUser user) {
        if (user.getAgencyType() != AgencyType.ADMIN && user.getAgencyType() != AgencyType.POLICE
                && user.getAgencyType() != AgencyType.GOVERNMENT) {
            throw new UnsupportedOperationException("alerts can only be accessed by admin, gov or police user group.");

        }
        final List<Alert> rawAlerts = new ArrayList<>(AlertSystem.getInstance().getUnhandled());
        // create new list for user specific alerts
        List<AlertRenderType> userAlerts = new LinkedList<>();
        // add only the user specific alerts to list
        for (Alert alert : rawAlerts) {
            if (user.canSeeAlert(alert)) {
                userAlerts.add(convert(alert));
            }
        }
        // initialize data model for web view
        // required for getAlertsForUserGroup()
        return new ListDataModel<>(userAlerts);
    }

    /**
     * deletes the in the web view selected {@link Alert} from the database
     *
     * @param alert a {@link AlertRenderType} to set as handled
     * @return the name of the {@link Alert}s management page where you can see the changes
     */
    public String deleteAlert(AlertRenderType alert) {
        AlertSystem.getInstance().close(alert.getOrigin());
        return "infocenter";
    }

    /**
     * renders an {@link Alert}
     *
     * @param alert the alert to display
     * @return the render data of that alert
     */
    public AlertRenderType convert(final Alert alert) {
        String time, type, message;
        switch (alert.getType()) {
            case CAR_MISSING:
                type = "Auto vermisst";
                break;
            case INFO:
                type = "Information";
                break;
            case MEASURESTATION_OFFLINE:
                type = "Messstation offline";
                break;
            case MEASURESTATION_OFFLINE_TEST:
                type = "Messstation 7 offline!";
                break;
            case MEASURESTATION_OFFLINE_TEST2:
                type = "Messsation 8 offline!";
                break;
            case MALFUNCTION:
                type = "Systemfehler";
                break;
            case RESCUE_LANE_SPEEDER:
                type = "Rettungsgassenfahrer";
                break;
            case STOLEN_CAR:
                type = "Gestohlenes Fahrzeug";
                break;
            case TRAFFIC_JAM:
                type = "Stau";
                break;
            case UNKNOWN_CAR:
                type = "Unbekanntes Fahrzeug";
                break;
            case WRONG_WAY_DRIVER:
                type = "Geisterfahrer";
                break;
            default:
                type = "Was anderes";
        }
        time = DataFormatter.formatDateTime(alert.getTime());
        message = alert.getMessage();
        return new AlertRenderType(alert, time, type, message);
    }


}
