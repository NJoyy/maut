package com.antares.cal.controllers.jsf.car;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.antares.cal.model.car.Car;
import com.antares.cal.model.car.LicenseNumber;
import com.antares.cal.model.user.OwnerUser;
import com.antares.cal.service.dao.car.CarDao;

/**
 * this provides CRUD functionality of {@link Car}s for the web view
 * @author Fabian Mittmann
 */
@Named
@RequestScoped
public class CarController implements Serializable {

    @Inject
    private CarDao dao;

    private LicenseNumber licenseNumber = new LicenseNumber();
    private Car carSearchResult;

    // required for findCarsByOwner()
    private List<Car> rawCars;
    private DataModel<Car> carFromOwnerDataModel;

    /**
     * loads a list of all {@link Car}s of the owner from database for use in web view
     * @param owner an {@link OwnerUser} to search for its {@link Car}s
     * @return a {@link DataModel} of {@link Car}s to use in a jsf table (e.g. '<h:datatable/>')
     */
    public DataModel<Car> findCarsByOwner(OwnerUser owner) {
        rawCars = new ArrayList<>();
        rawCars.addAll(dao.findByOwnerUser(owner));
        carFromOwnerDataModel = new ListDataModel<>(rawCars);
        return carFromOwnerDataModel;
    }

    /**
     * searches for a {@link Car} and sets the variable with the values of the {@link LicenseNumber}
     * @return the name of the page where the result {@link Car} is shown
     */
    public String findCarByLicenseNumber() {
        carSearchResult = dao.findByLicenseNumber(licenseNumber);
        return "positiontracking";
    }

    public LicenseNumber getLicenseNumber() {
        return licenseNumber;
    }
    public void setLicenseNumber(LicenseNumber licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Car getCarSearchResult() {
        return carSearchResult;
    }
    public void setCarSearchResult(Car carSearchResult) {
        this.carSearchResult = carSearchResult;
    }

    public SelectItem[] getCarLicenses(OwnerUser user) {
    	final Collection<Car> collection = dao.findByOwnerUser(user);
    	SelectItem[] items = new SelectItem[collection.size()];
    	int i = 0;
    	for (Car c : collection) {
			items[i] = new SelectItem();
			items[i].setValue(c);
			items[i].setLabel(c.getLicenseNumber().toString());
			i++;
		}
    	return items;
    }
}
